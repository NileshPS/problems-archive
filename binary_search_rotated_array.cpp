#include <bits/stdc++.h>

using namespace std;

int binary_search(int *arr, int left, int right, int key) {
	if(left > right) return -1;

	int mid = (left + right)/2;

	if(arr[mid] == key) return mid;
	if(arr[left] <= arr[mid]) { //left array is sorted
		if(key >= arr[left] && key <= arr[mid])
				return binary_search(arr, left, mid-1, key);

		return binary_search(arr, mid+1, right, key);
	}
	// right sub-array is sorted
	else {
		if(key >= arr[mid+1] && key <= arr[right]) {
			return binary_search(arr, mid+1, right, key);
		}

		return binary_search(arr, left, mid-1, key);
	}
}

int main() {
	int n;
	cin>>n;
	int arr[n];
	for(int i=0; i<n; ++i) cin>>arr[i];
	int key;
	cin>>key;
	cout<<binary_search(arr, 0, n-1, key)<<endl;
	return 0;
}