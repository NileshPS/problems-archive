/**
 * Binary Indexed Tree  of Fenwick Tree
 * References :
 * 	1) www.geeksforgeeks.org/binary-indexed-tree-or-fenwick-tree-2/
 * 	2) https://www.topcoder.com/community/data-science/data-science-tutorials/binary-indexed-trees/
 * 	3) https://www.hackerearth.com/practice/notes/binary-indexed-tree-made-easy-2/
 */

#include <bits/stdc++.h>

using namespace std;

class BIT {
private:
	int *bit, size;

public:

	//copy constructor
	explicit BIT(BIT &arg) {
		size = arg.size;
		bit = new int[size];
		for(int i=0; i<size; ++i) bit[i] = arg.bit[i];
	}
	
	//other constructor
	BIT(int *arr, int n) {
		assert(arr != NULL && n > 0);
		bit = new int[n+1];
		size = n + 1;
		memset(bit, 0, sizeof(int) * (n+1));
		for(int i=0; i<n; ++i) {
			update(i, arr[i]);
		}
	}

	~BIT() {
		delete [] bit;
	}

	void update(int index, int diff) {
		index = index + 1;
		while(index < size) {
			bit[index] += diff;
			index += (index & -index);
		}
	}

	int prefixSum(int index) {
		if(index < 0) return 0;
		index++;
		int sum = 0;
		while(index > 0) {
			sum += bit[index];
			index -= ( index & -index);
		}

		return sum;
	}
};

#define TYPE_RANGE 1
#define TYPE_PREFIX 2
#define TYPE_EXIT 3

int main() {
	int  n;
	cin>>n;
	int arr[n];
	for(int i=0; i<n; ++i) cin>>arr[i];
	BIT bit(arr, n);
	while(true) {
		int t, a, b;
		cin>>t;
		switch(t) {
			case TYPE_RANGE:
				cin>>a>>b;
				cout<<bit.prefixSum(b)  - bit.prefixSum(a-1)<<endl;
				break;
			case TYPE_PREFIX:
				cin>>a;
				cout<<bit.prefixSum(a)<<endl;
				break;
			case TYPE_EXIT:
				return 0;

		}
	}
	return 0;
}
