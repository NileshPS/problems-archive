#include "m_includes.hpp"

/**
    Given a set of points. Find whats the maximum no of points that lie in the same line.
**/

using namespace std;

typedef vector<pii> vp;

int max_points_online(const vp &points) {
    map< pii, int > umap;
    int ans = 0, overlapped = 0, vertical = 0, curMax;
    for(int i=0; i < points.size(); ++i) {
        umap.clear();
        overlapped = vertical = curMax = 0;
        for(int j = i + 1; j < points.size(); ++j) {
            if(points[i] == points[j])
                overlapped++;
            else if(points[i].first == points[j].first) //vertical
                vertical++;
            else {
                int dx = points[j].first - points[i].first, dy = points[j].second - points[i].second;
                int gcd = __gcd(dx, dy);
                dx /= gcd;
                dy /= gcd;
                auto it = umap.find(make_pair(dx, dy));
                if(it == umap.end()) {
                    umap[{dx, dy}] = 1;
                } else {
                    umap[{dx, dy}]++;
                }
                curMax = max(curMax, umap[pii(dx, dy)]);
            }
            curMax = max(curMax, vertical);
        }

        ans = max(ans, curMax + overlapped + 1);    
    }
    return ans;
}

int main() {
    vp points { {-1, 1}, {1, 1}, {2, 2}, {3, 3}, {-1, -2}, {0, 0}, {3, 4}};
    cout << max_points_online(points) << endl;
    return 0;
}