/*input
2
7
100 180 260 310 40 535 695
10
23 13 25 29 33 19 34 45 65 67
*/
#include <bits/stdc++.h>

using namespace std;

typedef vector<int> vi;
typedef vector<vi> matrix;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef vector<pii>   vpii;
typedef vector<bool> vb;
typedef vector<vb> bmatrix;

#define matrix_init(mat, m, n, val)   mat.resize(m, vi(n, val))
#define read_arr(arr)                       for (auto &el : arr) cin >> el;
#define matrix_read(mat)       for (auto &row : mat) \
                                       for (auto &el : row) \
                                          cin >> el;



int max_profit(const vi &price, int  n) {
    int profit =0, i = 1, start = 0;
    while (i < n) {
    	if (price[i] >= price[i-1]) i++;
    	else {
    		if (start != i - 1) {
    			profit += price[i-1] - price[start];
    			printf("(%d %d) ", start, i - 1);
    		}
    		start = i++;
    	}
    }
    if (start < n - 1) {
    	profit += price[n-1] - price[start];
    	printf("(%d %d) ", start, n - 1);
    }
    if (profit == 0) cout << "No Profit";
    cout << endl;
    return profit;
}

int main() {
    int t;
    cin >> t;
    while (t--) {
        int n;
        cin >> n;
        vi price(n);
        read_arr(price);
        max_profit(price, n);
    }
    return 0;
}