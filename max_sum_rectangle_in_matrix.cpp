/*input
4 5
 1  2 -1 -4 -20
-8 -3  4  2   1
 3  8 10  1   3
-4 -1  1  7   -6
*/
#include "m_includes.hpp"

using namespace std;


int kadane(const vi &arr, int &start, int &end, int n) {
	int curSum = arr[0], maxSum = arr[0], cur_start = 0 , cur_end = 0;
	start = end = 0;
	for (int i=1; i < n; ++i) {
		if (curSum + arr[i] < arr[i]) {
			cur_start = cur_end = i;
			curSum = arr[i];
		} else {
			cur_end++;
			curSum += arr[i];
		}
		if (maxSum < curSum) {
			start = cur_start;
			end  = cur_end;
			maxSum = curSum;
		}
	}
	return maxSum;
}

int max_sum(vi &arr, int m) {
	int ans = arr[0], cur = arr[0];
	for (int i=1; i< m; ++i) {
		cur = max(cur + arr[i], arr[i]);
		ans = max(ans, cur);
	}
	return ans;
}

void print_max_sum_submatrix(matrix &mat, int m , int n) {
	assert (m > 0 && n > 0);
	int ans = INT_MIN, colStart, colEnd, rowStart, rowEnd;
	for (int left = 0; left < n; ++left) {
		vi rowSum(m, 0);
		for (int right = left; right < n; ++right) {
			for (int i=0; i < m; ++i)
				rowSum[i] += mat[i][right];
			int start = -1, end = -1;
			int maxSum = kadane(rowSum, start, end, m);
			if (maxSum > ans) {
				colStart = left;
				colEnd   = right;
				rowStart = start;
				rowEnd   = end;
				ans = maxSum;
			}
		}
	}
	cout << "Max sum rectangle = " << endl;
	for (int i=rowStart; i <= rowEnd; ++i) {
		for (int j = colStart; j <= colEnd; ++j) {
			cout << mat[i][j] << "  ";
		}
		cout << endl;
	}
	cout << "Max sum  = " << ans << endl;
}

int main() {
	int m, n;
	cin >> m >> n;
	matrix mat;
	matrix_init(mat, m, n, 0);
	matrix_read(mat);
	print_max_sum_submatrix(mat, m, n);
	// int m;
	// cin >> m;
	// vi arr(m);
	// READ_ARR(arr);
	// int start, end;
	// int sum = kadane(arr, start, end, m);
	// for (int i=start; i <= end; ++i)
	// 	cout << arr[i] << " ";
	// cout << endl;
	// cout << sum << endl;
	return 0;
}