#include "m_includes.hpp"

using namespace std;

void print_words(const vi &arr, int i, string str) {
    if(i == arr.size()) {
        cout << str << endl;
        return;
    }
    static string alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    print_words(arr, i+1, str + char(alphabets[arr[i] - 1]));
    if(i + 1 < arr.size()) {
        int num = arr[i]*10 + arr[i+1];
        if(num <= 26) {
            print_words(arr, i+2, str + char(alphabets[num - 1]));
        }
    }
    return;
}
int main() {
    vi arr = {1, 2, 2, 1};

    print_words(arr, 0, "");
    return 0;
}
