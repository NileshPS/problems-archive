#include <iostream>
#include <vector>

using namespace std;

int n;
vector<int> vec;

bool isSafe(int queen, int row) {
	for(int i=0; i<queen; ++i) {
		if(vec[i] == row) {
			return false;
		}
	}

	int r = row-1;
	int c = queen - 1;
	while(c >= 0 && r > 0) {
		if(vec[c] == r)
			return false;
		--r;
		--c;
	}

	r = row;
	c = queen;

	while(c>=0 && r<=n) {
		if(vec[c] == r) {
			return false;
		}
		c--;
		r++;
	}

	return true;

}


void print() {
	for(int i=0; i<n; ++i) {
		for(int j=0; j<n; ++j) {
			if(vec[j] == i+1) {
				 cout<<"1 ";
			}
			else {
				cout<<"0 ";
			}
		}
		cout<<endl;
	}
}


bool place(int q) {
	if(q>=n) {
		return true;
	}
	for(int i=0; i<n;++i) {
		if(isSafe(q, i+1)) {
			vec[q] = i+1;
			if(place(q+1)) {
				return true;
			}
			vec[q] = 0;
		}
	}

	return false;
}


int main() {
	cin>>n;
	vec = vector<int>(n, 0);

	if(place(0)) {
		cout<<"Success!"<<endl;
		print();
	}
	else {
		cout<<"No Solution!"<<endl;
	}
	cout<<endl;
	return 0;

}

/**
 * ALTERNATIVE
 *
 * #include <iostream>
#include <vector>

using namespace std;

int N = 8;
vector< vector<int> > vec(N, vector<int>(N, 0));


bool isSafe(int row, int col) {
	for(int i=0; i<col; ++i) {
		if(vec[row][i])
			return false;
	}

	for(int i=row, j=col; i>=0 && j>=0; --i, --j) {
		if(vec[i][j])
			return false;
	}

	for(int i=row, j=col; i<N && j>=0; ++i, --j) {
		if(vec[i][j])
			return false;
	}
	return true;
}

bool place(int col) {
	if(col >= N)
		return true;

	for(int row = 0 ; row<N; ++row) {
		if(isSafe(row, col)) {
			vec[row][col] = 1;
			if(place(col + 1)) {
				return true;
			}

			vec[row][col] = 0;
		}
	}

	return false;
}


int main() {
	cin>>N;
	vec = vector< vector<int> >(N, vector<int>(N, 0) );
	if(place(0)) {
		cout<<"Solution exists!"<<endl;
		for(int i=0; i<N; ++i) {
			for(int j=0; j<N; ++j) {
				cout<<vec[i][j]<<" ";
			}
			cout<<endl;
		}
	}
	else {
		cout<<"No solution!"<<endl;
	}
	return 0;
}
 * 
 */