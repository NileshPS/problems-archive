/*input
10
2
*/
#include "m_includes.hpp"

using namespace std;

int find_recur(int f, int e) {
	if (e == 1) return f;
	if (f == 1) return 1;
	if ( !f || !e)return 0;

	int ans = INT_MAX;
	for (int i = 1; i <= f; ++i) {
		ans  = min (ans, 
			        max(find_recur(i - 1, e - 1), find_recur(f - i, e)) + 1);
	}
	return ans;
}

int find_worst_tries(int f, int e) {
	matrix dp;
	matrix_init(dp, f + 1, e + 1, 0);
	for (int i=0; i  <= f; ++i) {
		dp[i][0] = 0;
		dp[i][1] = i;
	}
	for (int j =1; j <= e; ++j) {
		dp[1][j] = 1;
		dp[0][j] = 0;
	}
	for (int i=2; i<= f; ++i) {
		for (int j = 2; j  <= e; ++j) {
			dp[i][j] = INF;
			for (int k=1; k <= i; ++k ){
				dp[i][j] = min(dp[i][j], 
						max(
							dp[k-1][j-1], dp[i - k][j]
						) + 1);
			}
		}
	}
	return dp[f][e];
}

int main() {
	int floors ,eggs;
	cin >> floors >> eggs;
	cout << find_recur(floors, eggs) << endl;
	return 0;
}