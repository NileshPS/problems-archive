#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CHAR_SIZE 256

int max(int a, int b) {
	return a < b? b : a;
}

void search(const char *text, const char *pat) {
	int m = strlen(text), n = strlen(pat);
	int s = 0;
	// Preprocess
	int table[CHAR_SIZE];
	memset(table, -1, CHAR_SIZE * sizeof(int));
	for(int i=0; i<n; ++i) {
		table[(int)pat[i]] = i;
	}
	// Preprocessing complete.
	while( s <= m - n) {
		int i = n - 1;
		while(i >= 0 && pat[i] == text[s + i]) {
			i--;
		}
		if ( i < 0) {
			//Pattern found
			printf("Pattern found at %d\n", s);
			s += 1;
		} else {
			s += max(1, table[(int)text[i]] - i);
		}
	}
}

int main() {
	char text[] = "geeksforgeeksaregeeks", pat[] = "eeks";
	search(text, pat);
	return 0;
}