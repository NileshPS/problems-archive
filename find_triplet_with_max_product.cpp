/*input
1
6
0 -1 3 100 70 50
*/
#include <bits/stdc++.h>

using namespace std;

typedef vector<int> vi;
typedef vector<vi> matrix;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef vector<pii>   vpii;
typedef vector<bool> vb;
typedef vector<vb> bmatrix;

#define matrix_init(mat, m, n, val)   mat.resize(m, vi(n, val))
#define read_arr(arr)                       for (auto &el : arr) cin >> el;
#define matrix_read(mat)       for (auto &row : mat) \
                                       for (auto &el : row) \
                                          cin >> el;


int main() {
	int t;
	cin >> t;
	while (t--) {
		int n;
		cin >> n;
		assert (n >= 3);
		vi arr(n);
		read_arr(arr);
		cout << max(arr[0] * arr[1] * arr[2], arr[n-1] * arr[n-2] * arr[n-3]) << endl;
	}
    return 0;
}