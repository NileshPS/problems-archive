/*input
abbc
*/
#include "m_includes.hpp"

using namespace std;
/**
 * Given a string, count number of subsequences of the form aibjck, i.e., 
 * it consists of i ’a’ characters, followed by j ’b’ characters, followed 
 * by k ’c’ characters where i >= 1, j >=1 and k >= 1.
 */
int main() {
	string str;
	cin >> str;
	int ac = 0, bc =0, cc = 0;
	for (int i=0; i < str.size(); ++i ){
		if (str[i] == 'a') {
			ac = 1 + 2 * ac;
		}
		else if(str[i] == 'b') {
			bc = ac + 2 * bc;
		}
		else if(str[i] == 'c') {
			cc = bc + 2 * cc;
		}
		else 
			throw "Invalid Input!";
	}
	cout << cc << endl;
}