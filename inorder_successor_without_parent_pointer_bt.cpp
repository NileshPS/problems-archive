#include <bits/stdc++.h>

using namespace std;

struct node {
    int data;
    node *left, *right;
};

node *succ = nullptr;
bool flag = false;

void findInorderSuccessor(node *root, int k) {
    if(! root) return;
    if(! flag  && root->data == k) {
        flag = true;
        if(root->right) {
            succ = root->right;
            while(succ->left)
                succ = succ->left;
        }
        return;
    }
    findInorderSuccessor(root->left, k);
    if(flag && succ == nullptr)
        succ = root;
    findInorderSuccessor(root->right, k);
}

int main() {
    node *n[10];
    for(int i=0; i< sizeof(n)/sizeof(n[0]); ++i) {
        n[i] = new node;
        n[i]->left = n[i]->right = nullptr;
        n[i]->data = i;
    }
    /**
             1
            /  \
           2    6
          / \    \
         3   5    7
          \      /
           4    8
    **/

    n[1]->left = n[2], n[1]->right = n[6];
    n[2]->left = n[3], n[2]->right = n[5];
    n[3]->right = n[4];
    n[6]->right = n[7];
    n[7]->left = n[8];

    findInorderSuccessor(n[1], 1);
    cout << succ->data << endl;
    flag  = false, succ = nullptr;
    findInorderSuccessor(n[1], 5);
    cout << succ->data << endl;
    flag  = false, succ = nullptr;
    findInorderSuccessor(n[1], 8);
    if(succ) {
        cout <<succ->data << endl;
    } else {
        cout << "No succcessor !" << endl;
    }
    return 0;
}