/**
    Find the sum of all left-leaves of a binary tree.
**/
    
#include "m_includes.hpp"
#define is_leaf(n) ((n)->left == nullptr && (n)->right == nullptr)

using namespace std;

struct node {
    node *left, *right;
    int data;
};

void left_leaf_sum(node *root, int &left_sum, bool left_child) {
    if(!root) return;
    if(is_leaf(root) && left_child) {
        left_sum += root->data;
        return;
    }
    left_leaf_sum(root->left, left_sum, true);
    left_leaf_sum(root->right, left_sum, false);
}

int main() {
    node *n[11];
    for(int i=0; i<11; ++i) {
        n[i] = new node;
        n[i]->left = n[i]->right = nullptr;
        n[i]->data = i;
    }
    /**
                     1
                  /    \
                 2      3
                /  \   /  \ 
               4    5  6   7
                          /
                         8
                       /   \
                      10    9
    **/
    n[1]->left = n[2], n[1]->right = n[3];
    n[2]->left = n[4], n[2]->right = n[5];
    n[3]->left =n[6], n[3]->right = n[7];
    n[7]->left = n[8];
    n[8]->left =n[10];
    n[8]->right = n[9];
    int sum = 0;
    left_leaf_sum(n[1], sum, false);
    cout << "Sum of left leaves is : " << sum << endl;
    return 0;

}