/*
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                                             #
# Find all occurences of a word in a matrix. No cycles are allowed. You can   #
# move to all 8 directions.                                                   #
#                                                                             # 
#                                                                             #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #*/

#include "m_includes.hpp"

using namespace std;

inline bool is_valid(int i, int j, int m, int n, vector<vb> &visited) {
    return i >= 0 && i < m && j >= 0 && j < n && !visited[i][j];
}

void search(const vs &grid, int i, int j, const string &pat, int k, vector<vb> &visited, vector<pii> path) {
    int m = grid.size(), n = grid[0].size();
    if(! is_valid(i, j, m, n, visited))
        return;
    if(grid[i][j] != pat[k])
        return;
    int dx[] = { 0, 1, 1, 1, 0, -1, -1, -1};
    int dy[] = {-1,-1, 0, 1, 1,  1,  0, -1};
    path.push_back( { i, j});
    visited[i][j] = true;
    if(k == pat.size() - 1) {
        // Print path.
        for(auto &el : path)
            printf("( %d, %d)  ", el.first, el.second);
        cout << endl;
        return;
    }
    for(int r=0; r < 8; ++r) {
        search(grid, i+dx[r], j + dy[r], pat, k + 1, visited, path);
    }
    path.pop_back();
}

void find_all_occurences(const vs &mat, const string &pat) {
    int m = mat.size(), n = mat[0].size();
    vector< pii > path;
    vector< vb > visited(m, vb(n, false));
    for(int i=0; i<m; ++i) {
        for(int j=0; j<n; ++j) {
            path.clear();
            visited.resize(m, vb(n, false));
            search(mat, i, j, pat, 0, visited, path);
        }
    }
}

int main() {
    vs mat = {
        "DEMXB", 
        "AOEPE",
        "DDCOD",
        "EBEDS",
        "CPYEN"
    };
    string pattern = "CODE";
    find_all_occurences(mat, pattern);
    return 0;
}