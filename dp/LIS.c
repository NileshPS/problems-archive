#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <limits.h>

int LIS(const int *arr, size_t len) {
	int *L = (int *)malloc(sizeof(int) * len);
	L[0] = 1;
	for(int i=1; i<len; ++i) {
		int j = i-1, k = -1;
		while(j >= 0) {
			if(arr[j] < arr[i]) {
				if(k == -1)
					k = j;
				else if(L[k] < L[j])
					k = j;
			}
			j--;
		}
		L[i] = (k == -1)?1:L[k] + 1;
	}
	int ans = INT_MIN;
	for(int i=0; i<len; ++i)
		if(L[i] > ans)
			ans = L[i];
	free(L);
	return ans;
}

int main() {
    int arr[] = {10, 22, 9, 33, 21, 50, 41, 60};
    int len = sizeof(arr)/sizeof(arr[0]);
    printf("Size of largest increasing subsequence is %d.\n", LIS(arr, len));
    return 0;
}