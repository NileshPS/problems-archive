#include <bits/stdc++.h>

using namespace std;

#define uint unsigned int

uint m_min(uint a, uint b, uint c) {
	if(a < b) {
		return a < c?a:c;
	}

	return b<c?b:c;
};

unsigned int cost_recur(const string &str1, int m, const string &str2, int n) {
	if(m==0)
		return n;

	if(n == 0)
		return m;

	if(str1[m-1] == str2[n-1])
		return cost_recur(str1, m-1, str2, n-1);

	return 1 + m_min(
		cost_recur(str1, m, str2, n-1), //insert
		cost_recur(str1, m-1, str2, n), //delete
		cost_recur(str1, m-1, str2, n-1) //update
		);

}


unsigned int cost_dp(const string &str1, const string &str2) {
	int m = str1.length(), n = str2.length();
	unsigned int mem[m+1][n+1];
	memset(mem, 0, sizeof(int)*(m+1)*(n+1));
	for(int i=0; i<=m; ++i) {
		for(int j=0; j<=n; ++j) {
			if( i == 0)
				mem[i][j] = j;
			else if( j == 0)
				mem[i][j] = i;
			else {
				if(str1[i-1] == str2[j-1]) {
					mem[i][j] = mem[i-1][j-1];
				}
				else {
					mem[i][j]  = 1 + m_min(mem[i-1][j], mem[i][j-1] , mem[i-1][j-1]);
				}
			}
		}
	}

	return mem[m][n];
}

int main() {
	string str1, str2;
	cin>>str1>>str2;
	cout<<"Max cost of str1->str2 = "<<cost_recur(str1, str1.length(), str2, str2.length())<<endl;
	cout<<"Using dp = " <<cost_dp(str1, str2)<<endl;
	return 0;
}