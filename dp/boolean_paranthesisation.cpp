#include <bits/stdc++.h>

using namespace std;


int dp_solution(const string &str, const string &op) {
    int n = str.size();
    assert(n == op.size() + 1);
    int T[n][n], F[n][n];
    for(int i=0;i < n; ++i) {
        T[i][i] = str[i] == 'T'?1:0;
        F[i][i] = str[i] == 'F'?1:0;
    }
    for(int cl = 2; cl <= n; ++cl) {
        for(int i=0; i + cl <= n; ++i) {
            int j = i + cl - 1;
            T[i][j] = F[i][j] = 0;
            for(int k = i; k < j; ++k) {
                if(op[k] == '|') {
                    T[i][j] += (T[i][k] + F[i][k]) * (T[k+1][j] + F[k+1][j]) - F[i][k] * F[k+1][j];
                    F[i][j] += F[i][k] * F[k+1][j];
                }
                else if(op[k] == '&') {
                    T[i][j] += T[i][k] * T[k+1][j];
                    F[i][j] += (T[i][k] + F[i][k]) * (T[k+1][j] + F[k+1][j]) - T[i][k] * T[k+1][j];
                }
                else if(op[k] == '^') {
                    T[i][j] += T[i][k] * F[k+1][j] + F[i][k] * T[k+1][j];
                    F[i][j] += T[i][k] * T[k+1][j] + F[i][k] * F[k+1][j];
                }
            }
        }
    }
    return T[0][n-1];
}



int main() {
    string str = "TTFT";
    cout << dp_solution(str, "|&^") << endl;
    return 0;
}