/**
 * Find the index of character to remove,  upon which the given string will be a palindrome.
 * Find the index for which str[i] != str[len - i - 1];
 * Now check whether s[0:i-1] + s[i+1:len] is a palindrome.
 * If yes, ans is i
 * else    ans is len - i - 1
 */

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int t;
    cin >> t;
    while(t--) {
        string s;
        cin >> s;
        int len = s.size();
        int i;
        for(i=0; i<len/2; ++i) {
            if(s[i] != s[len - i - 1]) break;
        }
        if(i == len/2 && len %2 == 1) {
            cout << -1 << endl;
            continue;
        }
        // Check s[0:i-1] & s[i+1:len] is a palidrome
        s.erase(i, 1);
        int j = i;
        // check whether s is palindrome
        len--;
        for(i=0; i < len/2; ++i) {
              if(s[i] != s[len - i - 1]) break;
        }
        if(i >= len/2) {
            cout << j << endl;
        }
        else {
            cout << len - j << endl;
        }
    }
    return 0;
}
