/**
    Split the input string into minimum no of palidromic substrings.
**/
#include "../m_includes.hpp"

using namespace std;

bool is_palidrome(string str) {
    for(int i=0; i< (str.length() >> 1); ++i) {
        if(str[i] != str[str.size() - i - 1]) return false;
    }
    return true;
}

int split(string str) {
    int n = str.length();
    if(n <= 1 || is_palidrome(str)) return 0; // Requires no cutting.
    int cmin = INT_MAX;
    for(int i=0;i < n; ++i) {
        // Cut the string as [0..i],[i+1..n]
        cmin = min( cmin, split(str.substr(0, i)) + split(str.substr(i+1)) + 1);
    }
    return cmin;
}

int split_dp(const string &str, int left, int right, map<pii, int> &dp) {
    if(left == right || is_palidrome(str.substr(left, right - left + 1))) return 0;
    auto it = dp.find(make_pair(left ,right));
    if(it != dp.end()) {
        return it->second;
    }
    int cmin = INT_MAX;
    for(int i=left; i<right; ++i) {
        // SPlit string as [left ..i], [i+1..right]
        cmin = min(cmin, split_dp(str, left, i, dp) + split_dp(str, i + 1, right ,dp) + 1);
    }
    return dp[make_pair(left, right)] = cmin;

}

int main() {
    string str = "cdcdddcdadcdcdcd";
    map<pii, int> mem;
    cout << split_dp(str, 0, str.size() - 1, mem) << endl;
    // for(auto it = mem.begin(); it != mem.end(); ++it) {
    //     int left  =it->first.first, right = it->first.second;
    //     cout << str.substr(left, right - left + 1) << "\t-->\t" << it->second << endl;
    // }
    return 0;
}