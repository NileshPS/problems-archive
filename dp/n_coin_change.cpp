#include "m_includes.hpp"

using namespace std;


int coinChange(const vi &coins, int count, int amount) {
	if(amount <= 0)
		return amount == 0?1:0;
	if(count <= 0)
		return 0;
	return  coinChange(coins, count-1, amount) + coinChange(coins, count, amount-coins[count-1]);

}

int coinChangeDP(const vi &coins, int count, int amount) {
	if(amount <= 0) {
		return amount == 0?1:0;
	}
	if(count <= 0)
		return 0;
	Matrix(dp, count + 1, amount + 1);
	for(int i=0; i <= count; ++i) {
		for(int j=0; j <= amount; ++j) {
			if(j == 0) {
				dp[i][j] = 1;
			}
			else if(i == 0) {
				dp[i][j] = 0;
			}
			else {
				dp[i][j] += j - coins[i-1] >= 0 ? dp[i][j-coins[i-1]] : 0;
				dp[i][j] += dp[i-1][j];
			}
		}
	}
	return dp[count][amount];
}


int coinChangeDPOptimized(const vi &coins, int count, int amount) {
	if(amount <= 0) {
		return amount == 0?1:0;
	}
	if(count <= 0)
		return 0;
	vi dp(amount + 1, 0);
	dp[0] = 1;
	for(int i=0; i<count; ++i) {
		for(int j = coins[i]; j <= amount; ++j) {
			dp[j] += dp[j - coins[i]];
		}
	}
	return dp[amount];
}

int main(int argc, char **argv) {
	vi coins;
	int noOfCoins, amount;
	cout << "Enter no of coins : ";
	cin >> noOfCoins;
	cout << "Enter their denominations : ";
	coins.resize(noOfCoins);
	for(int i=0; i < noOfCoins; ++i)
		cin >> coins[i];
	cout << "Enter amount : ";
	cin >> amount;
	cout << "No of possible ways to make change is : " << coinChangeDP(coins, noOfCoins, amount) << endl;
	return 0;
}