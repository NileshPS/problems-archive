#include "m_includes.hpp"

using namespace std;

int maxSumIS(const int *arr, int len) {
	int *L = new int[len];
	memset(L, 0, sizeof(int) * len);
	L[0] = arr[0];
	for(int i=1; i<len; ++i) {
		int k = -1, j = i-1;
		while(j > -1) {
			if(arr[j] < arr[i]) {
				if(k == -1) 
					k = j;
				else
					k = (L[k] < L[j]) ? j : k;
			}
			j--;
		}
		if(k == -1) {
			// No such j found
			L[i] = arr[i];
		} 
		else {
			L[i] = arr[i] + L[k];
		}
	}

	int ans = INT_MIN;
	for(int i=0;i <len; ++i) {
		ans = max(ans, L[i]);
	}
	delete[] L;
	return ans;
}

int main() {
	int arr[] = {1, 101, 2, 3, 100, 4, 5};
    int n = sizeof(arr)/sizeof(arr[0]);
    printf("Sum of maximum sum increasing subsequence is %d\n",
           maxSumIS( arr, n ) );
    return 0;
}