#include <bits/stdc++.h>

/**
 * Find the maximum possible dot product of two sets of +ve integers.
 * Eg. A =  { 2, 3, 1, 7, 8 }	
 *     B =  { 3, 6, 7 }
 *     Ans := 107
 */
using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector< ii > vii;

int _findMaxDotImpl(const vi &a, int m, const vi &b, int n) {
	// if(n < 0 || m < n) return 0;
	// return max(
	// 	_findMaxDotImpl(a, m-1, b, n),
	// 	_findMaxDotImpl(a, m-1, b, n-1) + a[m] * b[n]
	// 	);

	/** Using Dynamic Programming. */
	int dp[a.size() + 1][b.size() + 1];
	for(int i=0; i<= a.size(); ++i) {
		for(int j=0; j<= b.size(); ++j) {
			if( i == 0 || j == 0 || i < j)
				dp[i][j] = 0;
			else {
				dp[i][j] = max(dp[i-1][j], dp[i-1][j-1] + a[i-1] * b[j-1]);
			}
		}
	}
	return dp[a.size()][b.size()];
}

int findMaxDot(const vi &a, const vi &b) {
	int m = a.size(), n = b.size();
	assert(m >= n);
	return _findMaxDotImpl(a, m-1,  b, n-1);

}

int main() {
	int m, n;
	cin>>m>>n;
	if(m < n) {
		cout<<"M < N not possible!"<<endl;
		return 0;
	}
	vi a(m), b(n);
	for(int i=0; i<m; ++i)
		cin>>a[i];
	for(int j=0; j<n; ++j)
		cin>>b[j];

	cout<<findMaxDot(a, b)<<endl;
	return 0;
}