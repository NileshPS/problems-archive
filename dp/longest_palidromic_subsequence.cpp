#include "m_includes.hpp"

using namespace std;

int findLen(const string &str) {
	int len = str.length();
	Matrix(dp, len, len );
	for(int i=0; i<len; ++i) {
		dp[i][i] = 1;
		if(i + 1 < len) {
			dp[i][i+1]  = (str[i] == str[i+1])? 2 : 1;
		}
	}

	for(int j=3; j<=len; ++j) {
		for(int i=0; i + j <= len; ++i) {
			// Fill L[i][j]
			if(str[i] == str[i + j - 1]) {
				dp[i][i+j-1] = dp[i+1][i+j-2] + 2;
			} 
			else {
				dp[i][i+j-1] = max(dp[i][i+j-2], dp[i+1][i+j-1]);
			}
		}
	}

	return dp[0][len-1];
}

int main() {
	cout << "Enter a string : ";
	string str;
	getline(cin, str);
	cout << "Larget Palidromic Subsequence length is : " << findLen(str);
	return 0;
}