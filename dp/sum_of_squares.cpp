#include "../m_includes.hpp"
// Find the mininum no of values whose squares will sum upto S.
using namespace std;


// TODO := Not correct.
int find(int n, int total) {
    if(total == 0) return 0;
    int amin = INT_MAX;
    for(int i=1; i<=n; ++i) {
        if(total >= i * i) {
            int t = find(n, total - i * i);
            if(t != INT_MAX && t + 1 < amin)
                amin = t + 1;
        }
    }
    return amin;
}

int findDP(int n, int total) {
    int dp[total + 1];
    dp[0] = 0;
    for(int i=1; i<= total; ++i) {
        dp[i] = INT_MAX;
        for(int j = 1; j<=n; ++j) {
            if(j * j <= i) {
                dp[i] = min(dp[i], dp[i-j*j] == INT_MAX ? 0 : dp[i-j*j] + 1);
            }
        }
    }
    return dp[total];
}

int main() {
    int S;
    cin >> S;
    cout << find((sqrt(S)), S) << endl << endl << findDP(sqrt(S), S) << endl;
    return 0;
}