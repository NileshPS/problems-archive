#include "m_includes.hpp"

using namespace std;

int minCost(const matrix &cost, int m, int n) {
	if(m < 0 || n < 0) 
		return INT_MAX;
	else if(m == 0 && n == 0)
		return cost[0][0];
	return min(
		min( minCost(cost, m-1, n), minCost(cost, m, n-1)), minCost(cost, m-1, n-1)) + cost[m][n];

}


int minCostDP(const matrix &cost, int m, int n) {
	Matrix(dp, m+1, n+1);

	for(int i=0; i<m; ++i) {
		for(int j=0; j<n; ++j) {
			if(i==0 && j==0) {
				dp[i][j] = cost[i][j];
			}
			else if(i == 0) {
				dp[i][j] = dp[i][j-1] + cost[i][j];
			}
			else if( j == 0) {
				dp[i][j] = dp[i-1][j] + cost[i][j];
			}
			else {
				dp[i][j] = min (
					min(dp[i-1][j-1], dp[i-1][j]), dp[i][j-1]) + cost[i][j];
			}
		}
	}
	return cost[m-1][n-1];
}


int main() {
	cout << "Enter N : ";
	int n;
	cin >> n;
	Matrix(mat, n, n);
	cout << "Enter costs : ";
	for(int i=0;i <n; ++i) {
		for(int j=0; j<n; ++j) {
			cin >> mat[i][j];
		}
	}
	// Find the mincost path
	cout << "Min cost path cost is : " << minCost(mat, n-1, n-1) << endl;
	return 0;
}