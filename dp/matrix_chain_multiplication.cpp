#include <bits/stdc++.h>

using namespace std;


int matrixChainOrder(const int *p, int l, int r) {
	if(l == r)
		return 0;
	int minCount = INT_MAX;
	for(int i=l; i<r; ++i) {
		// Divide the array into two disjoint parts. 
		// [l...i] and [i+1...r]
		minCount = min(
			minCount, matrixChainOrder(p, l, i) + matrixChainOrder(p, i+1, r) + p[l-1] * p[i] * p[r]);
	}
	return minCount;
}


int matrixChainOrderDP(const int *p, int n) {
	
	int dp[n][n];
	memset(dp, 0, sizeof(int) * n * n);
	for(int i=1; i<n; ++i) {
		dp[i][i] = 0; // Base case, nothing to multiply
		if(i + 1 < n) { // Cost to multiply two matrices
			dp[i][i+1] = p[i-1] * p[i] * p[i+1];
		}
	}

	for(int cl = 3; cl < n; ++cl) {
		for(int j = 1; j + cl - 1 < n; ++j) {
			int l = j, r = j + cl - 1;
			dp[l][r] = INT_MAX;
			for(int k = l; k < r; ++k) {
				dp[l][r] = min(dp[l][r],
								dp[l][k] + dp[k+1][r] + p[l-1] * p[k] * p[r]
							);
			}
		}
	}
	return dp[1][n-1];
}


int main() {
	int n;
	cout << "Enter  N : ";
	cin >> n;
	cout << "Enter orders : ";
	int *p = new int[n];
	for(int i=0; i<n; ++i)
		cin >> p[i];
	cout << "Minimum no of  multiplications is " << matrixChainOrderDP(p, n) << endl;
	delete[] p;
	return 0;
}