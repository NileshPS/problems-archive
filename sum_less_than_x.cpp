#include "../archives/m_includes.hpp"

using namespace std;

int find(const vi &a, int x) {
    int r = 0;
    for(int i=0; i<a.size(); ++i) {
        for(int j=i+1; j < a.size(); ++j) {
            if(a[i] + a[j] < x) 
                r++;
        }
    }
    return r;
}

int find_opt(const vi &a, int x) {
    int r, i , j;
    r = i = 0, j = a.size() - 1;
    while(i < j) {
        int c = a[i] + a[j];
        if(c >= x) {
            j--;
        }
        else {
          r += j - i;
          i++;
        }
    }
    return r;
}
int main(int argc, char **argv) {
    vi arr = {1, 2, 3, 6, 9, 10, 11, 15, 16 };
    if(argc < 2) {
        return 1;
    }
    int n = stoi(argv[1]);
    cout << "Native : " << find(arr, n) << endl << "Optimized : " << find_opt(arr, n) << endl << endl;
    return 0;

}

