#include <bits/stdc++.h>

using namespace std;

typedef vector<int> vi;


void build_heap(vi &a) {
	int n = a.size();
	for(int i=0; i<n; ++i) {
		int j = i;
		while ( j > 0) {
			if(a[j] > a[j/2]) swap(a[j], a[j/2]);
			j = j/2;
		}
	}
}


void rebuild_heap(vi &a, int limit) {
	int i = 0;
	bool flag = true;
	while(flag) {
		int leftChild = 2 * i + 1, rightChild = 2 * i + 2;
		if(rightChild < limit) {
			if(a[leftChild] > a[rightChild] && a[leftChild] > a[i]) {
				swap(a[i], a[leftChild]);
				i = leftChild;
			}
			else if(a[rightChild] > a[i] && a[rightChild] >= a[leftChild]) {
				swap(a[i], a[rightChild]);
				i = rightChild;
			}
			else {
				flag = false;
			}
		}
		else if(leftChild < limit) {
			if(a[leftChild] > a[i]) {
				swap(a[leftChild], a[i]);
				i = leftChild;
			}
			else 
				flag = false;
		}
		else {
			flag =false;
		}

	}
}

void heapSort(vi &a) {
	build_heap(a);
	for(int i=a.size() - 1; i > 0; --i) {
		swap(a[i], a[0]);
		rebuild_heap(a, i);
	}
}

void print(const vi &a) {
	for(int i=0; i<a.size(); ++i) cout<<a[i]<<" ";
	cout<<endl;
}

int main() {
	int n;
	cin>>n;
	vi a(n);
	for(int i=0; i<n; ++i) cin>>a[i];
	heapSort(a);
	print(a);

}