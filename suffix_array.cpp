#include <bits/stdc++.h>

using namespace std;

struct suffix {
	size_t start;
	int  rank[2];
};


bool comparator(const suffix &one, const suffix &two) {
	if(one.rank[0] == two.rank[0])
		return one.rank[1] < two.rank[1];

	return one.rank[0] < two.rank[0];
}

suffix* get_sorted_suffix_array(const string & str) {
	int len = str.length();
	suffix *sarr = new suffix[len];
	for(int i=0; i<len; ++i) {
		sarr[i].start = i;
		sarr[i].rank[0] = str[i] - 'a';
		sarr[i].rank[1] = ((i + 1) < len) ? str[i+1] - 'a' : -1;
	}

	sort(sarr, sarr + len, comparator);

	vector<int> ind(len, 0);
	for(int k = 4; k < ( len << 1) ; k <<= 1) {
		// Assign new rank
		int prevr = sarr[0].rank[0], rank = 0;
		sarr[0].rank[0] = 0;
		ind[sarr[0].start] = 0;
		for(int i=1; i<len; ++i) {
			if(sarr[i].rank[0] == prevr && sarr[i-1].rank[1] == sarr[i].rank[1]) {
				sarr[i].rank[0] = rank;
			}
			else {
				prevr = sarr[i].rank[0];
				sarr[i].rank[0] = ++rank;
			}

			ind[sarr[i].start] = i;
		}
		// Assign rank[1]
		for(int i=0; i<len; ++i) {
			int ni  = sarr[i].start + k/2;
			sarr[i].rank[1] = ( ni < len) ? sarr[ind[ni]].rank[0] : -1;
		}
		
		sort(sarr, sarr + len, comparator);
	}
	return sarr;
}


int main() {
	string str;
	cin >> str;
	suffix *sarr = get_sorted_suffix_array(str);
	for(int i=0; i<str.size(); ++i) {
		cout << str.substr(sarr[i].start) << endl;
	}
	delete[] sarr;
	return 0;
}
