/**
 * A segment tree implementation for Range sum query.s
 */

#include <bits/stdc++.h>

using namespace std;


int *arr, *st;
int arr_len, st_len;

void bst(int ss, int se, int index) {
	if(ss == se) {
		st[index] = arr[se];
		return;
	}

	int mid = (ss + se ) /2;
	bst(ss, mid, 2*index + 1);
	bst(mid+1, se, 2*index + 2);
	st[index] = st[2*index + 1] + st[2*index + 2];
}


int range_sum(int qs, int qe, int ss, int se, int index) {
	if(qe  < ss || se < qs)
		return 0;

	if(ss >= qs && se <= qe)
		return st[index];

	int mid = (ss + se)/2;
	return range_sum(qs, qe, ss, mid, 2*index + 1) +
			range_sum(qs, qe, mid+1,se, 2*index + 2);
}

void update_util(int ss, int se, int i, int diff, int index) {
	if(i < ss || i > se ) {
		return;
	}
	

	st[index] += diff;
	if ( ss != se) {
		int mid = (ss + se)/2;
		update_util(ss, mid, i, diff, 2*index + 1);
		update_util(mid+1, se, i, diff, 2*index + 2);
	}

}

void update(int pos, int newval) {
	assert(pos >=0 && pos < arr_len);
	int diff = newval - arr[pos];
	arr[pos] = newval;
	update_util(0, arr_len-1, pos, diff, 0);

}
int main() {
	cin>>arr_len;
	arr = new int[arr_len];
	for(int i=0; i<arr_len; ++i)
		cin>>arr[i];
	st_len = (int) 2 * pow(2, ceil(log10(arr_len)/log10(2))) - 1;
	st = new int[st_len];
	memset(st, 0, st_len);
	bst(0, arr_len - 1, 0);
	// for(int i=0; i<st_len; ++i)
	// 	cout<<st[i]<<" ";
	// cout<<endl;
	while(true) {
		char c; int i, j;
		cin>>c;
		if( c == 'E')
			exit(0);

		cin>>i>>j;
		if(c == 'R') 
			cout<<range_sum(i, j, 0, arr_len - 1, 0)<<endl;
		else if( c == 'U')  
			update(i, j);

	}

	delete[] arr;
	delete[] st;
	return 0;
}