#include "__includes.h"

struct __node {
	int data;
	struct __node *left, *right;
};

typedef struct __node node;

node *new_node(int data) {
	node *n = (node *)malloc(sizeof(node));
	n->data = data;
	n->left = n->right = NULL;
}

node *preorder_recursive(node *root) {
	if(root) {
		printf("%d  ", root->data);
		preorder_recursive(root->left);
		preorder_recursive(root->right);
	}
}


node *preorder(node *root) {
	node *cur = root, *tmp = NULL;
	while(cur) {
		if(cur->left == NULL) {
			printf("%d ", cur->data);
			cur = cur->right;
		}
		else {
			tmp = cur->left;
			while(tmp->right && tmp->right != cur) 
				tmp = tmp->right;
			if(tmp->right == NULL ){
				tmp->right = cur;
				printf("%d ", cur->data);
				cur = cur->left;
			}
			else {
				tmp->right = NULL;
				cur = cur->right;
			}
		}
	}
}


/**
  	5
      /   \
     4     3
   /  \     \
  6    7     1 
   \    \     \
    8	 9     2
**/

int main() {
	node *ptr[10];
	for(int i=0; i<10; ++i)
		ptr[i] = new_node(i+1);
	node *header = ptr[5];
	ptr[1]->right = ptr[2];
	ptr[3]->right = ptr[1];
	ptr[4]->left = ptr[6], ptr[4]->right = ptr[7];
	ptr[5]->left = ptr[4], ptr[5]->right = ptr[3];
	ptr[6]->right = ptr[8];
	ptr[7]->right = ptr[9];

	printf("Recursive traversal : ");
	preorder_recursive(ptr[5]);
	printf("\n\n Using morris traversal : ");
	preorder(ptr[5]);
	printf("\n\n\n");
	return 0;
}
