/**
 * Modified level order traversal of a binary tree.
 * If level i is printed from left to right, then level i + 1 must be printed
 *                          from right to left.
 */
#include <bits/stdc++.h>

using namespace std;

struct node {
    int data;
    node *left, *right;
};

typedef pair<node *, int> pni;

void reverse(node *root) {
    if(!root) return;
    queue< pni > q;
    q.push({root, 0});
    stack<int> st;
    bool reverseLevel = false;
    while(!q.empty()) {
        int n = q.size();
        while(n--) {
            pni cur = q.front();
            q.pop();
            if(cur.first->left) {
                q.push({cur.first->left, cur.second + 1});
                if(!reverseLevel) st.push(cur.first->left->data);
            }
            if(cur.first->right){
                q.push({cur.first->right, cur.second + 1});
                if(!reverseLevel) st.push(cur.first->right->data);
            }
            if(reverseLevel) {
                cur.first->data = st.top();
                st.pop();
            }
        }
        reverseLevel = !reverseLevel;
    }
}


void levelOrder(node *root) {
    queue<node *> q;
    q.push(root);
    while(!q.empty()) {
        cout  << q.front()->data << "  ";
        node *top = q.front();
        q.pop();
        if(top->left)
            q.push(top->left);
        if(top->right)
            q.push(top->right);
    }
    cout << endl;
}

int main() {

    /**
            1
          /   \
        2       3
      /   \   /   \
     4    5   6    7 
    /      \        \
   8        9       10
   **/
   node *n[11];
   for(int i=0;i <11; ++i) {
    n[i] = new node;
    n[i]->left = n[i]->right = nullptr;
    n[i]->data = i;
   }   
   n[1]->left= n[2], n[1]->right =n[3];
   n[2]->left = n[4], n[2]->right = n[5];
   n[3]->left = n[6], n[3]->right = n[7];
   n[4]->left = n[8];
   n[5]->right = n[9];
   n[7]->right = n[10];
   reverse(n[1]);
   levelOrder(n[1]);
   return 0;
}