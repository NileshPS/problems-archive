/*input
200000
*/
#include "m_includes.hpp"

using namespace std;

int findMaxLen(int n) {
	if (n < 7) return n;
	int ans = INT_MIN;
	for (int i = n - 3; i>= 1; --i) {
		// say the spilt occured at index i
		int cur_count = findMaxLen(i) * (n - i - 1);
		ans = max(ans, cur_count);
	}
	return ans;
}

int findMaxLenDP(int n) {
	int dp[n + 1];
	memset(dp, 0, sizeof(int) * (n + 1));
	for (int i=1; i <= n; ++i) {
		if (i < 7)
			dp[i] = i;
		else {
			for (int j=i-3; j >= 1; --j) {
				dp[i] = max(dp[i], dp[j] * (i - j - 1));
			}
		}
	}
	return dp[n];
}

int main() {
	int n;
	cin >> n;
	int i = 0;
	while (i++ < n) {
		cout << "When N = " << i << ", length = " << findMaxLen(i) << endl;
	}
	return 0;
}