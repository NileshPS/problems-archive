#include <bits/stdc++.h>

using namespace std;


class UnionFind {
private:
	vector<int> ds, rank; int size;
public:
	UnionFind(int size = 0) {
		ds.resize(size, 0);
		rank.resize(size, 0);
		this->size = size;
		for(int i=0;i <size; ++i) {
			ds[i] = i;
			rank[i] = 1;
		}
	}

	int root(int a) {
		return ds[a] == a?a:ds[a] = root(ds[a]); // Uses path compression.
	}

	void connect(int a, int b) {
		int ra = root(a), rb = root(b);
		if(rank[ra] < rank[rb]) {
			ds[ra] = rb;
		}
		else if(rank[ra] > rank[rb]) {
			ds[rb] = ra;
		}
		else {
			ds[ra] = rb;
			rank[rb]++;
		}
	}

	bool isConnected(int a, int b) { return root(a) == root(b); }

};

int main() {


	return 0;
}
