#include <bits/stdc++.h>

using namespace std;


vector< vector<int> > prices;


int shop_util(int g, int b) {
	if( g >= prices.size())
		return b;

	if( b <= 0)
		return -1;

	for(int i=0; i<prices[g].size(); ++i) {
		if(prices[g][i] <= b) {
			int t = shop_util(g+1, b - prices[g][i]);
			if(t != -1) // yay
				return t;
		}
	}

	return -1;
}

struct comparator {
	bool operator()(const int &a, const int &b) {
		return a > b;
	}
};

int shop(int budget) {
	//sort the individual prices
	for(int i=0; i<prices.size(); ++i) {
		sort(prices[i].begin(), prices[i].end(), comparator());
	}

	int r = shop_util(0, budget);

	return r >= 0?budget - r : -1;
	

}

int main() {
	int b, n, t, z;
	cin>>b;
	cin>>n;
	prices.resize(n);
	for(int i=0; i<n; ++i) {
		cin>>t;
		while(t--) {
			cin>>z;
			prices[i].push_back(z);
		}
	}

	// for(int i=0;i<prices.size(); ++i) {
	// 	for(int j=0; j<prices[i].size(); ++j) {
	// 		cout<<prices[i][j]<<" ";
	// 	}
	// 	cout<<endl;
	//}
	
	int min = shop(b);
	if(min == -1) {
		cout<<"No solution!"<<endl;
	}
	else {
		cout<<min<<endl;
	}
	
	return 0;
}