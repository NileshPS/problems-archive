/*input
2 3 4 5
*/

/**
 * Calculate ((a ^ b) / c ) % m.
 */
#include <iostream>
using namespace std;

typedef long long ull;

ull x, y;

ull modularExp(int a, int b, int m) {
    if (b == 0) return 1;
    ull tmp = modularExp(a, b / 2, m) % m;
    if (b % 2) 
        return ((tmp * tmp) % m * (a % m)) % m;
    return (tmp * tmp) % m;
}

// We dont need GCD for real.
void extendedGCD(int a, int b) {
    if (b == 0) {
        x = 1;
        y = 0;
        return;
    }
    extendedGCD(b, a % b);
    int newx = y, newy = x - (a / b) * y;
    x = newx;
    y = newy;
}

ull modInverse(int a, int m) {
    extendedGCD(a, m);
    return (x % m + m) % m;
}

int main() {
    int a, b, c, m;
    cin >> a >> b >> c >> m;
    ull numr = modularExp(a, b, m);
    ull inv = modInverse(c, m);
    cout << "x = " << x << ", y = " << y << endl;
    cout << (numr * inv) % m << endl;
}