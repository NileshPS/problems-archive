/*input
4
6 8 7 3
*/
#include "../m_includes.hpp"

using namespace std;

pff sjf_stat(int n, vi &bt) {
	sort(bt.begin(), bt.end());
	float wt  = 0, cur_time = 0, tt = 0;
	for (int i=0; i  < n; ++i) {
		wt += cur_time;
		tt += bt[i] + cur_time;
		cur_time += bt[i];
	}
	return make_pair(wt / n, tt/n);
}

int main() {
	int n;
	cin >> n;
	vi burst_time(n);
	read_arr(burst_time);
	pff ret = sjf_stat(n, burst_time);
	cout << "Average waiting time = " << ret.first << endl;
	cout << "Average turn around time = " << ret.second << endl;
}