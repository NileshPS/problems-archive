/*input
3
3 3 6
5 9 6
*/
#include "../m_includes.hpp"

using namespace std;


pff fcfs_avg_stat(int n, vi &arr_time,vi &burst) {
	int cur_time = arr_time[0];
	vi completion_time(n);
	for (int i=0; i < n; ++i) {
		cur_time = max(cur_time, arr_time[i]) + burst[i];
		completion_time[i] = cur_time;
	}
	float total_wt = 0, total_tt = 0;
	printf("ID\tTurn\tWait\n");
	for (int i=0; i < n; ++i) {
		int turn_around_time = completion_time[i] - arr_time[i],
		    waiting_time     = turn_around_time - burst[i]	;
		total_wt += waiting_time;
		total_tt += turn_around_time;
		printf("%d\t%d\t\t%d\n", i, turn_around_time, waiting_time);
	}
	return make_pair(total_wt / n, total_tt / n);
}

int main() {
	int n;
	cin >> n;
	vi arrival_time(n), burst_time(n);
	read_arr(arrival_time);
	read_arr(burst_time);
	pff avg_time = fcfs_avg_stat(n, arrival_time, burst_time);
	cout << "Average waiting time = " << avg_time.first << endl;
	cout << "Average turn around time = " << avg_time.second << endl;
	return 0;
}