#ifndef __NODE_H
#define __NODE_H

template <class T>
struct __node {
    T data;
    __node<T> *left, *right;
};

typedef __node<int> node;

template<class T>
__node<T> *get_node(T data = 0) {
    __node<T> *n = new __node<T>;
    n->data = data;
    n->left = n->right = NULL;
    return n;
}


#endif
