/*input
6
1 3 5 7 9 11
*/  
#include <bits/stdc++.h>

using namespace std;

typedef vector<int> vi;
typedef vector<vi> matrix;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef vector<pii>   vpii;
typedef vector<bool> vb;
typedef vector<vb> bmatrix;

#define matrix_init(mat, m, n, val)   mat.resize(m, vi(n, val))
#define read_arr(arr)                       for (auto &el : arr) cin >> el;
#define matrix_read(mat)       for (auto &row : mat) \
                                       for (auto &el : row) \

#define print_arr(arr) for(auto &el : arr) { cout << el << " "; } cout << endl;
     


vi lazy;


void construct_segment_tree_util(const vi &arr, int l, int r, int index, vi &tree) {
    if (l == r) {
        tree[index] = arr[l];
        return;
    }
    int left = 2 * index + 1, right = 2 * index + 2, mid = (l + r ) / 2;
    construct_segment_tree_util(arr, l, mid, left, tree);
    construct_segment_tree_util(arr, mid + 1, r, right, tree);
    tree[index] = tree[left] + tree[right];
}

vi construct_segment_tree(const vi &arr) {
    int n = arr.size();
    vi tree(2 * pow(2, ceil(log2(n))) - 1, 0);
    lazy = tree;
    construct_segment_tree_util(arr, 0, n - 1, 0, tree);
    return tree;
}


int segment_tree_range_sum(vi &tree, int cur_l, int cur_r, int req_l, int req_r,int index = 0) {
    if (lazy[index] != 0) {
        tree[index] += (cur_r - cur_l + 1) * lazy[index];
        if (cur_l != cur_r) {
            lazy [2 * index + 1] += lazy[index];
            lazy [2 * index + 2] += lazy[index];
        }
        lazy[index] = 0;
    }
    if (cur_r < req_l || req_r < cur_l  ) return 0;
    if ( cur_l >= req_l && cur_r   <= req_r) return tree[index];
    int mid = (cur_l  + cur_r)  / 2;
    return segment_tree_range_sum(tree, cur_l, mid, req_l, req_r, 2 * index + 1) + 
            segment_tree_range_sum(tree, mid + 1, cur_r, req_l, req_r, 2 * index + 2);
}

void segment_tree_point_update(vi &tree, int l, int r, int index, int new_val, int si = 0) {
    if (l == r) {
        if (l == index) tree[si] = new_val;
        return;
    }
    int mid = (l + r ) / 2;
    segment_tree_point_update(tree, l, mid, index, new_val, 2 * si + 1);
    segment_tree_point_update(tree, mid + 1, r, index, new_val, 2 * si + 2);
    tree[si ] = tree[2 * si + 1] + tree[2 * si + 2];
}

void segment_tree_range_update(vi &tree, int l, int r, int rs, int re, int diff, int si = 0) {
// Using lazy propogation
    if (lazy[si] != 0) {
        tree[si] += (r - l + 1) * diff;
        if (l != r) { // Not a leaf node
            lazy [2 * si + 1] += lazy[si]; 
            lazy [2 * si + 2] += lazy[si];
        }
        lazy[si] = 0;
    }
    if (r < rs || re < l) return;
    if (l >= rs && r <= re) {
        tree[si] += (r - l + 1) * diff;
        if (l != r) {
            lazy[2 * si + 1] += diff;
            lazy[2 * si + 2] += diff;
        }
        lazy[si] = 0;
        return;
    }
    int mid = (l + r ) / 2;
    segment_tree_range_update(tree, l, mid, rs, re, diff, 2 * si + 1);
    segment_tree_range_update(tree, mid + 1, r, rs, re ,diff, 2 * si + 2);
    tree[si ] = tree[2 * si + 1] + tree[2 * si + 2];
}

int main() {
    int n;
    cin >> n;
    vi arr(n);
    read_arr(arr);
    vi seg_tree = construct_segment_tree(arr);
    cout << segment_tree_range_sum(seg_tree, 0, n  - 1, 1, 3) << endl;
    segment_tree_range_update(seg_tree, 0, n - 1, 1, 5, 10);
    cout << segment_tree_range_sum(seg_tree, 0, n - 1, 1, 3) << endl;
    return 0;
}