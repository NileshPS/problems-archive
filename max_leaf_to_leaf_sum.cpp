#include <bits/stdc++.h>

using namespace std;

struct node {
    int data;
    node *left, *right;
};

int findMaxLLSum(node *root, int &cur_max) {
    if(!root) return 0;
    if(!root->left && ! root->right) return root->data;
    int ls = findMaxLLSum(root->left, cur_max), rs = findMaxLLSum(root->right, cur_max);
    if(root->left && root->right) {
        cur_max = max(cur_max, ls + rs + root->data);
        return max(ls, rs) + root->data;
    }
    return root->left ? ls + root->data : rs + root->data;
}

int main() {
    node *n[11];
    for(int i=0;i < 11; ++i) {
        n[i] = new node;
        n[i]->left = n[i]->right = nullptr;
        n[i]->data = i;
    }
    /**
         1
        / \
       2   -4
      / \   \
     -8  3   5
            / \
           6   7
    **/
    n[8]->data = -8;
    n[4]->data = -4;
    n[1]->left = n[2], n[1]->right = n[4];
    n[2]->left = n[8], n[2]->right = n[3];
    n[4]->left = nullptr, n[4]->right = n[5];
    n[5]->left = n[6], n[5]->right = n[7]; 

    int smax = INT_MIN;
    findMaxLLSum(n[1], smax);
    cout << smax << endl;
    return 0;
}