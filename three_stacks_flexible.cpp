#include <bits/stdc++.h>

using namespace std;

class ThreeStacks {
	#define ASSERT_STACK_NO(s) assert(s>=0 && s < size)
	private:
		struct StackNode {
			int data;
			int prev_index;
		};
		StackNode *arr;
		int *free_nodes;
		int size, free_ptr, tops[3];

	public:
		ThreeStacks(int size) {
			assert(size >= 3);
			arr = new StackNode[size];
			free_nodes = new int[size];
			free_ptr = 0;
			for(int i=0; i<size; ++i) {
				free_nodes[i] = i;
			}
			this->size = size;
			tops[0] = tops[1] = tops[2] = -1;
		}

		~ThreeStacks() {
			delete[] arr;
			delete[] free_nodes;
		}

		bool push(int stack_no, int value) {
			ASSERT_STACK_NO(stack_no);
			if(isFull()) return false;
			int index = free_nodes[free_ptr];
			free_ptr++;
			arr[index].data = value;
			arr[index].prev_index = empty(stack_no)?-1:tops[stack_no];
			tops[stack_no] = index;
			return true;
		}
		void pop(int stack_no) {
			ASSERT_STACK_NO(stack_no);
			if(empty(stack_no)) return;
			int curIndex = tops[stack_no];
			tops[stack_no] = arr[curIndex].prev_index;
			--free_ptr;
			free_nodes[free_ptr] = curIndex;
		}
		int top(int stack_no) {
			ASSERT_STACK_NO(stack_no);
			if(empty(stack_no)) return -1;
			return arr[tops[stack_no]].data;
		}

		bool empty(int st_no) {
			ASSERT_STACK_NO(st_no);
			return tops[st_no] <= -1;
		}

		bool isFull() {
			return free_ptr >= size;
		}

		void print(int stack_no = -1) {
			if(stack_no == -1) {
				cout<<endl;
				print(0);
				print(1);
				print(2);
				return;
			}

			ASSERT_STACK_NO(stack_no);
			cout<<endl;
			if(empty(stack_no)) {
				cout<<"Stack "<<stack_no<<" (EMPTY)!"<<endl;
				return;
			}
			cout<<"Stack "<<stack_no<<" : ";
			int top = tops[stack_no];
			while(top != -1) {
				cout<<arr[top].data<<" ";
				top = arr[top].prev_index;
			}
			cout<<endl;
		}

};

int main() {
	int ch, data, st_no, size;
	cin>>size;
	ThreeStacks st(size);
	do {
		cout<<"1. Push\n2. Pop\n3. Display\n4. Display All\n5. Exit\nEnter your choice : ";
		cin>>ch;
		switch(ch) {
			case 1:
				if(st.isFull()) {
					cout<<"Full!"<<endl;
					continue;
				}
				cout<<"Enter stack_no : ";
				cin>>st_no;
				cout<<"Enter data : ";
				cin>>data;
				st.push(st_no, data);
				cout<<"Success!"<<endl;
				break;
			case 2:
				cout<<"Enter stack_no : ";
				cin>>st_no;
				if(st.empty(st_no)) {
					cout<<"Empty!"<<endl;
				}
				else {
					cout<<st.top(st_no)<<endl;
					st.pop(st_no);
					cout<<"Success!"<<endl;
				}
				break;
			case 3:
				cout<<"Enter stack_no : ";
				cin>>st_no;
				if(st.empty(st_no)) {
					cout<<"Empty!"<<endl;
				}
				else {
					st.print(st_no);
				}
				break;
			case 4:
				st.print();
				break;
			case 5:
				cout<<"Goodbye!"<<endl;
				break;
			default:
				cout<<"Invalid option!"<<endl;

		}
	} while(ch != 5);
}