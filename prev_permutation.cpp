#include <bits/stdc++.h>

using namespace std;

bool compare(const char &a, const char &b) {
        return a > b;
}

string prev(string str) {
        int len = str.size();
        int i=len - 2;
        while(i >= 0 && str[i] <= str[i+1])
                i--;
        if(i < 0 ) {
                // Reverse
                reverse(str.begin(), str.end());
                return str;
        }
        
        //else, Find least smallest element to the right
        int j , pos;
        j = pos = i + 1;
        while(j < len) {
                if(str[j] < str[i] && str[j] > str[pos]) {
                        pos = j;
                }
                j++;
        }
        swap(str[i], str[pos]);
        sort(str.begin() + i + 1, str.end(), compare);
        return str;
}

int main() {
        string str;
        cin >> str;
        cout << "My ans : " << prev(str) << endl;
        prev_permutation(str.begin(), str.end());
        cout << "Stdlib ans : " << str << endl;
        return 0;
}
