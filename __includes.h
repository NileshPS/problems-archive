#ifndef __INCLUDES_H
#define __INCLUDES_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <malloc.h>
#include <stdbool.h>
#include <regex.h>
#include <ctype.h>
#include <inttypes.h>


#define min(a, b) ((a) < (b))? (a): (b)
#define max(a, b) ((a) > (b))? (a): (b)
#define swap(a, b) {                        \
				typeof(a) _temp   = (a);    \
				              (a) = (b);    \
				              (b) = _temp;  \
                 }                          \

#define FOR(var, limit) for(int var = 0; var < limit; ++ var )
#define FORI(init, var, limit)  for(int var = init; var < limit; ++ var)



#endif