
#ifndef M_INCLUDES_HPP
#define M_INCLUDES_HPP


#include <bits/stdc++.h>

typedef std::vector<int> vi;
typedef std::vector<std::string> vs;

typedef std::pair<int, int> pii;
typedef std::vector<pii>    vpii;
typedef std::vector<vi>     matrix;
typedef std::vector<bool>   vb;
typedef std::vector<char>   vc;
typedef std::pair<float, float> pff;

#define matrix_init(mat, m, n, val)  mat.resize(m, vi(n, val))
#define matrix_read(mat)  for(auto &row : mat) for(auto &el : row) cin >> el
#define matrix_print(mat)  for(auto &row : mat)  {\
                                 for (auto &el : row) \
                                 	  cout << el <<  "  ";\
                                 cout << endl; \
                           }


typedef unsigned int uint;
typedef unsigned long long ull;

#define read_arr(arr)  for(auto &el : arr) cin  >> el
#define INF   INT_MAX
#define print_arr(arr) for(auto el : arr) { cout << el << " ";} cout << endl;
#endif
