/*input
2
5
*/
#include "m_includes.hpp"

using namespace std;

ull find_recur(int n, int sum) {
	if (sum < 0) return 0;
	if ( n == 0)
		return sum ? 0 : 1;
	ull ans = 0;
	for (int i=0; i <10; ++i) {
		ans += find_recur(n - 1, sum - i);
	}
	return ans;
}

ull find_count(int n, int sum) {
	ull ans = 0;
	for (int i=1; i< 10; ++i) {
		ans += find_recur(n - 1, sum - i);
	}
	return ans;
}


ull find_count_dp(int n, int sum) {
	vector< vector<ull> > dp(n + 1, vector<ull>(sum + 1, 0));
	dp[0][0] = 1;
	for (int i=1; i <= n; ++i){
		for (int j = 1; j <= sum; ++j) {
			dp[i][j] = 0;
			for (int k = 0; k < 10 && k <= j; ++k) {
				dp[i][j] += dp[i-1][j-k];
			}
		}
	}
	return dp[n][sum];
}

int main() {
	int n, sum;
	cin >> n >> sum;
	cout << find_count_dp(n, sum) << endl;
	return 0;
}