#include <bits/stdc++.h>

using namespace std;

typedef vector<int> vi;
typedef vector<vi> matrix;
typedef vector<string> vs;
typedef pair<int, int> pii;

#define matrix_init(mat, m, n, val)   mat.resize(m, vi(n, val))
#define read_arr(arr)                       for (auto &el : arr) cin >> el;
#define matrix_read(mat, m, n)       for (auto &row : mat) \
                                       for (auto &el : row) \
                                          cin >> el;


// O(n^3) solution
int has_quad(vi &a, int n, int s) {
    sort (a.begin(), a.end());
    for (int i=0; i < n; ++i) {
        for (int j= i + 1; j < n; ++j) {
            int l = j + 1, r = n - 1;
            while (l < r) {
                int curSum = a[i] + a[j] + a[l] + a[r];
                if (curSum < s) l++;
                else if ( curSum > s) r--;
                else return 1;
            }
        }
    }
    return 0;
}


struct sum_t {
    int sum, first, second;
};

int has_quad_opt(const vi &a, int n, int s) {
    int size = n * ( n - 1) / 2;
    vector<sum_t> psum(size);
    int k = 0;
    for (int i=0; i < n; ++i) {
        for (int j =i + 1; j < n; ++j) {
            psum[k].sum = a[i] + a[j];
            psum[k].first = i;
            psum[k].second = j;
        }
    }

    sort(psum.begin(), psum.end(), [](const sum_t &a, const sum_t &b) {
        return a.sum < b.sum;
    });

    int l = 0, r = size - 1;
    while (l < r) {
        int t = psum[l].sum + psum[r].sum;
        if ( t < s) l++;
        else if (t > s) r--;
        else if( psum[l].first != psum[r].first && psum[l].first != psum[r].second &&
                 psum[l].second != psum[r].first && psum[l].second != psum[r].second)
                 return 1;
    }
    return 0;
}

int main() {
    int t;
    cin >> t;
    while (t--) {
        int n, x;
        cin >> n;
        vi arr(n);
        read_arr(arr);
        cin >> x;
        cout << has_quad(arr, n, x) << endl;
    }
    return 0;
}