#include <bits/stdc++.h>

using namespace std;

typedef unsigned long long ull;
typedef pair<ull, pair<int, int> > graphNode;

vector< graphNode > graph;
int edges, nodes;


class UnionFind {
private:
	vector<int> ds;

public:
	UnionFind(int n = 0) {
		ds.resize(n, 0);
		for(int i=0; i<n; ++i) ds[i] = i;
	}

	void connect(int a, int b) {
		int ra = root(a), rb = root(b);
		ds[ra] = rb;
	}

	int root(int a) {
		return a == ds[a]?a:root(ds[a]);
	}

	bool isConnected(int a, int b) { return root(a) == root(b); }
};


struct edge_comparator {
	bool operator()(const graphNode &n1, const graphNode &n2) {
		return n1.first < n2.first; //compare the edges
	}
};




ull krusal() {
	sort(graph.begin(), graph.end(), edge_comparator());
	vector<int> ufds(nodes, 0);
	for(int i=0;i < nodes; ++i) ufds[i] = i;
	UnionFind uf(nodes);	
	ull minCost = 0l;
	for(int i=0; i<graph.size(); ++i) {
		if(!uf.isConnected(graph[i].second.first, graph[i].second.second)) {
			minCost += graph[i].first;
			uf.connect(graph[i].second.first, graph[i].second.second);
		}
	}
	return minCost;
}


int main() {
	cin>>nodes>>edges;
	graph.resize(edges);
	for(int i=0; i<edges; ++i) {
		ull edge; int a, b;
		cin>>edge>>a>>b;
		graph[i]  = make_pair(edge, make_pair(a, b));
	}

	// for(auto node : graph) {
	// 	cout<<"("<<node.second.first<<", "<<node.second.second<<")\t"<<node.first<<endl;
	// }

	vector<graphNode> mst;
	int minCost = krusal();
	cout<<minCost<<endl;

	return 0;
}