/*input
500
*/ 
#include "m_includes.hpp"

#define get_bit(n, k) ( ( (n) & (1 << (k)) ) >> k)
#define set_bit(n, k)  n = (n) | (1 << k)

#define ISIZE (sizeof(int) * 8 )

using namespace std;

struct trie_node {
	trie_node *link[2];
	trie_node() {
		link[0] = link[1] = NULL;
	}
};

typedef trie_node node;

node *trie_insert(node *root, int val) {
	if (!root) {
		root = new node;
	}
	node *ptr = root;
	for (int i = ISIZE - 1; i >= 0; --i) {
		int bit = get_bit(val, i);
		if (ptr->link[bit])
			ptr = ptr->link[bit];
		else {
			ptr->link[bit] = new node;
			ptr = ptr->link[bit];
		}
	}
	return root;
}




int trie_max_xor(node *root, int num) {
	if (!root) return 0;
	int ans = 0;
	node *ptr = root;
	for  (int i = ISIZE - 1; i >= 0; --i) {
		int bit = get_bit(num, i);
		if (ptr->link[1-bit]) {
			set_bit(ans, i);
			ptr = ptr->link[1-bit];
		} else {
			ptr = ptr->link[bit];
		}
	}
	return ans;
}

/**
 * Given an array A, find two numbers  A[i] & A[j] (i != j)
 * such that A[i] ^ A[j] is maximum.
 */

int max_xor_pair_brute(const vi &arr, int n) {
	int ans = INT_MIN;
	for (int i=0; i < n; ++i) {
		for (int j =i + 1; j < n; ++j) {
			ans = max(ans, arr[i] ^ arr[j]);
		}
	}
	return ans;
}


int max_xor_pair_opt(const vi &arr, int n) {
	int ans = INT_MIN;
	assert (n >= 2);
	node *root = trie_insert(NULL, arr[0]);
	for (int i=1; i < n; ++i) {
		ans = max(ans, trie_max_xor(root, arr[i]));
		root = trie_insert(root, arr[i]);
	}
	return ans;
}

/**
 * Find the subarray with maximum xor value
 */
int max_xor_subarray_brute(const vi &arr, int n) {
	int ans = INT_MIN;
	for (int i=0; i< n; ++i) {
		int cur = 0;
		for (int j =i; j < n; ++j) {
			cur ^= arr[j];
			ans = max(ans, cur);
		}
	}
	return ans;
}


int max_xor_subarray_trie(const vi &arr, int n) {
	node *root = trie_insert(NULL, 0);
	int pre = 0, ans = INT_MIN;
	for (int i=0; i < n; ++i) {
		pre ^= arr[i];
		ans = max(ans, trie_max_xor(root, pre));
		root = trie_insert(root, pre);
	}
	return  ans;
}

int main() {
	int n;
	cin >> n;
	vi arr(n);
	srand(time(NULL));
	for (int &num : arr)
		num = rand() % ( 3 * n);
	cout << max_xor_subarray_brute(arr, n) << endl 
		 << max_xor_subarray_trie(arr, n)   << endl;
	return 0;
}