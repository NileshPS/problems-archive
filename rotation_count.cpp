#include <bits/stdc++.h>

using namespace std;

int findRotations(const vector<int> &arr, int size) {
        int left = 0, right = size - 1;
        while(left <= right) {
                int mid = (left + right)/2;
                int next = (mid + 1)  % size, prev = (mid + size - 1) % size;
                if(arr[next] > arr[mid] && arr[prev] > arr[mid]) {
                        return mid;
                }
                
                // If the left subarry is sorted, continue with the right subarray and vice versa.
                if(arr[left] <= arr[mid]) {
                        left = mid + 1;
                }
                else if(arr[mid+1] <= arr[right]) {
                        right = mid - 1;
                }
                else {
                        return -1; // Perhaps, array is not sorted.
                }
        }
        return -1;
}

int main() {
        int n;
        cin >> n;
        vector<int> arr(n, 0);
        for(int i=0; i<n; ++i)
                cin >> arr[i];
        cout << "No. of rotations  = " << findRotations(arr, n) << endl;
        return 0;
}
