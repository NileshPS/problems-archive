#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

//Binary tree to circular double linked list conversion.
// Order of the new list should be same as the inorder traversal of the tree.

/*
	Explanation
<------------------>
	1) Convert the left and right subtree into a DCLL.
	2) Concatenate leftDCLL and root . Let it's header be temp.
	3) Concatenate temp and rightDCLL.
 */

typedef struct node {
	int data;
	struct node *lc /* prev */ , *rc /* next */;
} node;

node *get_node() {
	node *n = (node *)malloc(sizeof(node));
	n->lc = n->rc = NULL;
	return n;
}


/**
 *          10
 *         /  \
 *        11    9
 *       /  \  / \ 
 *      3   2 1   6
 *         /       \
 *        7         5
 *       /  \       / 
 *      0    8     4
 */


node *convert(node *root);
void inorder(node *);

int main() {
	node *root, *ptr;
	node *n[12];
	for(int i=0; i<12; ++i) {
		n[i]= get_node();
		n[i]->data = i;
	}

	n[2]->lc = n[7];
	n[5]->lc = n[4];
	n[6]->rc = n[5];
	n[7]->lc = n[0]; n[7] -> rc = n[8];
	n[9]->lc = n[1]; n[9] -> rc = n[6];
	n[10]->lc = n[11]; n[10]->rc = n[9];
	n[11]->lc = n[3]; n[11]->rc = n[2];

	root = n[10];
	inorder(root);
	printf("\n");

	root = convert(root);
	ptr = root;
	//print forward
	printf("Forward : ");
	do {
		printf("%d ", ptr->data);
		ptr = ptr->rc;
	}
	while(ptr != root);

	//print reverse
	printf("\nReverse : ");
	ptr = root;
	do {
		printf("%d ", ptr->data);
		ptr = ptr->lc;
	}
	while(ptr != root);
}

void inorder(node *root) {
	if(root) {
		inorder(root->lc);
		printf("%d ", root->data);
		inorder(root->rc);
	}
}

int isLeaf(node *n) {
	if(!n) return 0;
	return n->lc == NULL && n->rc == NULL;
}


node *concat(node *h1, node *h2) {
	//h1 and h2 are both circular double linked list.
	if( ! h1) return h2;
	if( !h2) return h1;
	node *second_list_last_node = h2->lc;
	h1->lc->rc = h2;
	h2->lc = h1->lc;
	h1->lc = second_list_last_node;
	second_list_last_node->rc = h1;
	return h1;
	
}

node *convert(node *n) {
	node *left, *right;
	if(!n) return NULL;
	if(isLeaf(n)) {
		n->lc = n->rc = n;
		return n;
	}
	//else
	left = convert(n->lc);
	right = convert(n->rc);
	n->lc = n->rc = n;
	concat(left, n);
	concat(left, right);
	return left;
}