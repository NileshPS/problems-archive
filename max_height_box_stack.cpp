#include "m_includes.hpp"

using namespace std;


struct box {
	float l, b, h;
	float base_area() const {
		return l * b;
	}
};

float max_stack_height(const vector<box> arg, int n) {
	vector<box> boxes(3 * n);
	for (int i=0; i < n; ++i ) {
		boxes[3 * i] = arg[i];
		boxes[3 * i + 1] = {
			.l = arg[i].l,
			.b = arg[i].h,
			.h = arg[i].b
		};
		boxes[3 * i + 2] = {
			.l = arg[i].b,
			.b = arg[i].h,
			.h = arg[i].l
		};
	}
	sort (boxes.begin(), boxes.end(), [](const box &a, const box &b){
		return a.base_area() > b.base_area();
	});
	cout << "All combinations : " << endl;
	for (int i=0; i < 3 * n ; ++i) {
		cout << boxes[i].l << " " << boxes[i].b << " " << boxes[i].h << endl;
	}
	vector<float> lis(3 *n, 0);
	lis[0] = boxes[0].h;
	for (int i=1; i < 3 * n; ++i) {
		lis[i] = boxes[i].h;
		for (int j =0 ; j < i; ++j) {
			if (boxes[i].l < boxes[j].l && boxes[i].b < boxes[j].b) {
				lis[i] = max(lis[i], lis[j] + boxes[i].h);
			}
		}
	}
	return *max_element(lis.begin(), lis.end());
}


/**
 * Refer this link for problem statement.
 * http://www.geeksforgeeks.org/dynamic-programming-set-21-box-stacking-problem
 */

int main() {
	int n;
	vector<box> boxes{ {4, 7, 9},
                       {5, 8, 9},
                       {11, 20, 40},
                       {1, 2, 3} };
	n = boxes.size();
	// for (auto &el : boxes) {
	// 	cin >> el.l >> el.b >> el.h;
	// }
	cout << max_stack_height(boxes, n ) << endl;
	return 0;
}