#include <bits/stdc++.h>

using namespace std;

struct node {
	node *left, *right;
	int data;
};

node *new_node(int data = 0) {
	node *n = new node;
	n->data =data;
	n->left = n->right = NULL;
	return n;
}

void zigzag(node *root) {
	if(!root) return;
	stack<node *> st1, st2;
	st1.push(root);
	while(!st1.empty() || !st2.empty()) {
		if(!st1.empty()) {
			while(!st1.empty()) {
				node *ptr = st1.top();
				cout<<ptr->data<<endl;
				st1.pop();
				if(ptr->right) st2.push(ptr->right);
				if(ptr->left) st2.push(ptr->left);
			}
		}
		else {
			while(!st2.empty()) {
				node *ptr = st2.top();
				cout<<ptr->data<<endl;
				st2.pop();
				if(ptr->left) st1.push(ptr->left);
				if(ptr->right) st1.push(ptr->right);
			}
		}
	}

}
int main() {
	node *n[8];
	for(int i=0; i<8; ++i)
		n[i] = new_node(i);

	n[1]->left = n[2]; n[1]->right = n[3];
	n[2]->left = n[4]; n[2]->right = n[5];
	n[3]->left = n[6]; n[3]->right = n[7];

	zigzag(n[1]);

	cout<<"Done!"<<endl;
	return 0;
}

