#include <bits/stdc++.h>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> pii;

pair<pii, int> find_max_cross(const vi &arr, int l, int m, int r) {
    int t = 0, leftIndex = m, rightIndex = m, leftMaxSum = INT_MIN, rightMaxSum = INT_MIN, i = m;
    while(i >= 0) {
        t += arr[i];
        if(t > leftMaxSum) {
            leftMaxSum = t;
            leftIndex = i;
        }
        i--;
    }
    i = m + 1;
    t = 0;
    while(i <= r ) {
        t += arr[i];
        if(t > rightMaxSum) {
            rightMaxSum =t;
            rightIndex = i;
        }
        i++;
    }
    return make_pair(make_pair(leftIndex, rightIndex), leftMaxSum + rightMaxSum);
}

pair<pii, int> find_max_sum_subarray(const vi &arr, int l, int r) {
    if(l == r) {
        return make_pair(make_pair(l, r), arr[l]);
    }
    int mid = (l + r) >> 1;
    pair<pii, int> left = find_max_sum_subarray(arr, l, mid),
                    right = find_max_sum_subarray(arr, mid + 1, r),
                    cross = find_max_cross(arr, l, mid, r);
    if(left.second >= right.second && left.second >= cross.second)
        return left;
    else if(right.second >= left.second && right.second >= cross.second)
        return right;
    else
        return cross;
}

int main() {
    vi arr { 1, 2, -5, 6, 7, -8, 9, 11, 2, -1, 0, 5, 6};
    pair<pii, int> ret = find_max_sum_subarray(arr, 0, arr.size() - 1);
    cout << "[" << ret.first.first << ", " << ret.first.second <<"]  =  " << ret.second << endl;
    return 0;

}