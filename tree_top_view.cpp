#include "m_includes.hpp"

using namespace std;

void generate_top_view(node *root, map<int, pii> &view, int cur_level, int cur_dist, int &maxLeft, int &maxRight) {
    if(! root)
        return;
    auto it = view.find(cur_dist);
    if(it == view.end() || it->second.first > cur_level) {
        view[cur_dist] = make_pair(cur_level, root->data);
    }
    maxLeft = min(cur_dist, maxLeft);
    maxRight = max(cur_dist, maxRight);
    generate_top_view(root->left, view, cur_level + 1, cur_dist - 1, maxLeft, maxRight);
    generate_top_view(root->right, view, cur_level + 1, cur_dist + 1, maxLeft, maxRight);
}

void generate_bottom_view(node *root, map<int, pii> &view, int cur_level, int cur_dist, int &maxLeft, int &maxRight) {
    if(! root)
        return;
    auto it = view.find(cur_dist);
    if(it == view.end() || it->second.first < cur_level) {
        view[cur_dist] = make_pair(cur_level, root->data);
    }
    maxLeft = min(cur_dist, maxLeft);
    maxRight = max(cur_dist, maxRight);
    generate_bottom_view(root->left, view, cur_level + 1, cur_dist - 1, maxLeft, maxRight);
    generate_bottom_view(root->right, view, cur_level + 1, cur_dist + 1, maxLeft, maxRight);
}


int main() {
    /**
     *       0
     *      / \
     *     1   2
     *    /   / \
     *   6   4   5
     *  /  \       \
     * 9    3       7
     *            /
     *           8
     */

    node *n[10];
    for(int i=0;i  < 10; ++i)
        n[i] = get_node(i);

    n[0]->left = n[1], n[0]->right = n[2];
    n[1]->left = n[6];
    n[2]->right = n[5], n[2]->left = n[4];
    n[6]->left = n[9], n[6]->right = n[3];
    n[5]->right = n[7];
    n[7]->left = n[8];

    map<int, pii> view;
    int maxLeft = 0, maxRight = 0;
    generate_top_view(n[0], view, 0, 0, maxLeft, maxRight);
    cout << "Top View : ";
    for(int i=maxLeft; i <= maxRight; ++i) {
        cout << view[i].second << "  ";
    }
    cout << endl << endl;
    view.clear();
    generate_bottom_view(n[0], view, 0, 0, maxLeft, maxRight);
    cout << "Bottom View : ";
    for(int i=maxLeft; i <= maxRight; ++i) {
        cout << view[i].second << "  ";
    }
    cout << endl << endl;

    return 0;
}

