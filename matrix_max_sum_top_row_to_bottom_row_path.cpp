/*input
4 4
1 2 3 4
1 3 2 1
4 2 1 1
9 8 1 2
*/

// Find max sum path from first row to bottom row. 
#include <bits/stdc++.h>

using namespace std;

typedef vector<int> vi;
typedef vector<vi> matrix;

#define matrix_init(mat, m, n, val) mat.resize(m, vi(n, val))


int m, n;

inline bool is_valid(int i, int j) {
	return i >= 0 && i < m && j >= 0 &&  j < n;
}

int strobe(const matrix &mat, int i, int j, matrix &dp) {
	if (! is_valid(i, j))
		return INT_MIN;
	if (dp[i][j] != -1) return dp[i][j];
	if (i == m - 1) {
		return dp[i][j] = mat[i][j];
	}

	return dp[i][j] = max( strobe(mat, i + 1, j, dp), 
		 					max( strobe(mat, i + 1, j - 1, dp), strobe(mat, i +1, j + 1, dp))) + mat[i][j];
}

int max_path_weight(const matrix &mat) {
	matrix dp;
	matrix_init(dp, m, n, -1);
	int ans = INT_MIN;
	for (int j=0; j <n; ++j) {
		ans = max(ans, strobe(mat, 0, j, dp));
	}
	return ans;
}

int bottom_up(const matrix &mat) {
	matrix dp;
	matrix_init(dp, m, n, 0);
	for (int j=0; j < n; ++j)
		dp[m-1][j] = mat[m-1][j];
	for(int i = m-2; i >= 0; --i) {
		for(int j=0; j < n; ++j) {
			dp[i][j] = INT_MIN;
			if (is_valid(i + 1, j))
				dp[i][j] = max(dp[i][j], mat[i][j] + dp[i+1][j]);
			if (is_valid(i + 1,j - 1))
				dp[i][j] = max(dp[i][j], mat[i][j] + dp[i+1][j - 1]);
			if (is_valid(i + 1, j + 1))
				dp[i][j] = max(dp[i][j], mat[i][j] + dp[i+1][j+1]);
		}
	}
	return *max_element(dp[0].begin(), dp[0].end());
}

int main() {
	cin >> m >> n;
	matrix mat;
	matrix_init(mat, m, n, 0);
	for (auto &row : mat)
		for (auto &el :row)
			cin >> el;
	cout << "Maximum path weight = " << max_path_weight(mat)<< endl << "Bottom up := " << bottom_up(mat) << endl;
	return 0;
}