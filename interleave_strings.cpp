/*input
AB
C
*/

/**
 * Given two strings str1 and str2, write a function that prints all 
 * interleavings of the given two strings. You may assume that all characters 
 * in both strings are different.
 */
#include "m_includes.hpp"

using namespace std;

void print_interleave(const string &a, int ai, const string &b, 
									int bi, string &result, int ri) {
	int lena = a.size(), lenb = b.size();
	if (ai >= lena && bi >= lenb) {
		cout << result << endl;
		return;
	}
	if (ai < lena) {
		result[ri] = a[ai];
		print_interleave(a, ai + 1, b, bi, result, ri + 1);
	}
	if (bi < lenb) {
		result[ri] = b[bi];
		print_interleave(a, ai, b, bi + 1, result, ri + 1);
	}
}

int main() {
	string a, b;
	cin >> a >> b;
	string result = a + b;

	print_interleave(a, 0 , b, 0, result, 0);
	return 0;
}