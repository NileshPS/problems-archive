/**

  Program to obtain a binary tree from its inorder and preorder traversal.
  Implementation is recursive. 
  For eg. First element of the preorder traversal is obviously the root of BT.
  This element will partition the inorder list into two parts, one for the left subtree and one for the right.
  Apply this procedure recursively.
 **/

#include <bits/stdc++.h>

using namespace std;

struct node {
	int data;
	node *left, *right;
};


void inorder(node *root) {
	if(root) {
		inorder(root->left);
		cout<<root->data<<" ";
		inorder(root->right);
	}
}

void preorder(node *root) {
	if(root) {
		cout<<root->data<<" ";
		preorder(root->left);
		preorder(root->right);
	}
}


int n;
int pi = 0;


int find_index(vector<int> &in, int key) {
	for(int i=0; i<in.size(); ++i)
		if(key == in[i])
				return i;

	return -1;
}

node *make_binary_tree(vector<int> &in, int is, int ie, vector<int> &pre) {
	if(is > ie) return NULL;
	node *m_node = new node;
	m_node->data = pre[pi];
	++pi;
	int index = find_index(in , pre[pi-1]);
	if(pi >= n) {
		m_node->left = m_node->right = NULL;
		return m_node; 
	}
	m_node->left = make_binary_tree(in, is, index-1, pre);
	m_node->right = make_binary_tree(in, index + 1, ie, pre);
	return m_node;
}

int main() {
	cin>>n;
	cout<<"Enter inorder : ";
	vector<int> in(n), pre(n);
	for(int i=0; i<n; ++i)
		cin>>in[i];

	cout<<"Enter preorder traversal : ";
	for(int i=0; i<n; ++i)
		cin>>pre[i];

	node *header = make_binary_tree(in, 0, n-1, pre);

	cout<<"Inorder traversal : ";
	inorder(header);
	cout<<endl;
	cout<<"PReorder traversal : ";
	preorder(header);
	cout<<endl;
	return 0;
}
