#include <bits/stdc++.h>

using namespace std;

typedef vector<int> vi;


int main() {
	int mat[][4] = {
		{10, 20, 30, 40 },
		{12, 22, 33, 44 },
		{15, 23, 25, 48 },
		{17, 23, 32, 79 }
	};

	int key = 23;
	int row = 0, col = 3; //top right
	while(row < 4 && col > -1) {
		if(mat[row][col] > key) {
			col--;
		}
		else if(mat[row][col] < key)
			row++;
		else {
			cout<<"Found at index "<<row<<", "<<col<<endl;
			return 0;
		}
	}
	cout<<"Not found!"<<endl;
	return 0;
}

	
