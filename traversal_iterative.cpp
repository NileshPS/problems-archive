#include <bits/stdc++.h>

using namespace std;

struct node {
	int data;
	struct node *left, *right;
};

node *new_node(int data = 0) {
	node *n = new node;
	n->left = n->right = NULL;
	n->data = data;
	return n;
}

void inorder(node *root) {
	if(! root)
		return;
	stack<node *> st;
	node *cur = root;
	st.push(cur);
	cur = cur->left;
	while(! st.empty()) {
		if(! cur) {
			node *top = st.top();
			st.pop();
			cout << top->data << "   ";
			cur = top->right;
		}
		if(cur) {
			st.push(cur);
			cur = cur->left;
		}

	}
	return;
}

void preorder(node *root) {
	if(! root)
		return;
	stack<node *> st;
	st.push(root);
	while(! st.empty()) {
		node *top = st.top();
		st.pop();
		cout << top->data << " ";
		if(top->right)
			st.push(top->right);
		if(top->left)
			st.push(top->left);
	}
}

void postorder(node *root) {
	if(! root)
		return;
	stack<node *> st1, st2;
	st1.push(root);
	while(! st1.empty()) {
		node *top = st1.top();
		st1.pop();
		st2.push(top);
		if(top->left)
			st1.push(top->left);
		if(top->right)
			st1.push(top->right);
	}

	while(! st2.empty()) {
		cout << st2.top()->data <<"  ";
		st2.pop();
	}
	return;
}

/**
      1
    /   \
   2     7
  /    /   \
 3    8     9
  \
   4
  / \
 5   6
**/

int main() {
	node *nodes[10];
	for(int i=0; i<10; ++i)
		nodes[i] = new_node(i);
	
	nodes[1]->left = nodes[2], nodes[1]->right = nodes[7];
	nodes[2]->left = nodes[3];
	nodes[3]->right = nodes[4];
	nodes[4]->left = nodes[5], nodes[4]->right = nodes[6];
	nodes[7]->left = nodes[8], nodes[7]->right = nodes[9];

	postorder(nodes[1]);
	cout << endl << endl;
	return 0;
}
