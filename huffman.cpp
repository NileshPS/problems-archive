#include <iostream>
#include <string>
#include <queue>

using namespace std;

struct node {
	char data;
	int count;
	node *left, *right;

	node(char d, int c) : data(d), count(c) {
	}

};

struct compare {
	bool operator()(node *one, node *two) {
		return one->count > two->count;
	}
};

void printHuffmanCodes(node *n, string str) {
	if(!n)
		return;
	//cout<<n->data<<" "<<n->count<<endl;
	if(n->data != '$') {
		cout<<n->data<<": "<<str<<endl;
	}

	printHuffmanCodes(n->left, str+"0");
	printHuffmanCodes(n->right, str+"1");
}

void findAndPrintHCodes(char *ch, int *ct, int size) {
	priority_queue<node *, vector<node *>, compare> pq;
/*	pq.push(new node('a', 10));
	pq.push(new node('b', 12));
	pq.push(new node('c', 5));
	pq.push(new node('d', 3));
	pq.push(new node('e', 6));

	while(!pq.empty()) {
		node *nod = pq.top();
		pq.pop();
		cout<<nod->data<< " "<<nod->count<<endl;
	}
	cout<<"Done!"<<endl;
*/
	for(int i=0; i<size; ++i) {
		pq.push(new node(ch[i], ct[i]));
	}

	node *one, *two;
	while(pq.size() != 1) {
		one = pq.top();
		pq.pop();
		two = pq.top();
		pq.pop();

		node *new_node  = new node('$', one->count + two->count);
		new_node->left = one;
		new_node->right = two;
		pq.push(new_node);
	}

	printHuffmanCodes(pq.top(), "");
}


int main() {
	int size;
	cin>>size;
	char *c = new char[size];
	int *count = new int[size];
	for(int i=0; i<size; ++i) {
		cin>>c[i]>>count[i];
	}
	findAndPrintHCodes(c, count, size);

	delete[] c;
	delete[] count;
	return 0;

}