/**
 * http://www.techiedelight.com/print-nodes-binary-tree-specific-order/
 */

#include "m_includes.hpp"

using namespace std;

struct node {
    int data;
    node *left, *right;
};


node *get_node(int data = 0) {
    node *ret = new node;
    ret->data = data;
    return ret;
}

void alternate_order_top_down(node *root) {
    queue<node *> q1, q2;
    if(!root)
        return;
    cout << root->data << " ";
    q1.push(root->left);
    q2.push(root->right);

    while(! q1.empty()) {
        node *top = q1.front();
        q1.pop();
        cout << top->data << "  ";
        if(top->left)
            q1.push(top->left);
        if(top->right)
            q1.push(top->right);

        top = q2.front();
        q2.pop();
        cout << top->data<< "  ";
        if(top->right)
            q2.push(top->right);
        if(top->left)
            q2.push(top->left);

    }
    cout << endl << endl;
}

stack<int> level_order(node *root, bool reverse = true) {
    stack<int> st;
    if(! root)
        return st;
    queue<node *> q;
    q.push(root);
    while(! q.empty()) {
        node *n = q.front();
        q.pop();
        st.push(n->data);
        if(reverse) {
            if(n->right)
                q.push(n->right);
            if(n->left)
                q.push(n->left);
        }
        else {
            if(n->left)
                q.push(n->left);
            if(n->right)
                q.push(n->right);
        }
    }   
    return st;
}

void alternate_order_bottom_up(node *root) {
    if(! root)
        return;
    stack<int> sleft = level_order(root->left, true), sright = level_order(root->right, false);
    assert(sleft.size() == sright.size());
    while(! sleft.empty()) {
        cout << "  " << sleft.top() <<"  " << sright.top();
        sleft.pop();
        sright.pop();
    }
    cout << root->data << "  " << endl << endl;
}

int main() {
    node *n[16];
    for(int i=0;i< 16; ++i) 
        n[i] = get_node(i);
    n[1]->left = n[2], n[1]->right = n[3];
    n[2]->left = n[4], n[2]->right = n[5];
    n[3]->left = n[6], n[3]->right = n[7];
    n[4]->left = n[8], n[4]->right = n[9];
    n[5]->left = n[10], n[5]->right = n[11];
    n[6]->left = n[12], n[6]->right = n[13];
    n[7]->left = n[14], n[7]->right = n[15];

    alternate_order_top_down(n[1]);
    cout << endl;
    alternate_order_bottom_up(n[1]);
    cout << endl;

    return 0;
}
