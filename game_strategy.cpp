/*input
4
8 15 3 7
*/
#include "m_includes.hpp"

using namespace std;

ull find_max_dp(const vi &arr) {
	int n = arr.size();
	matrix dp;
	matrix_init(dp, n, n, 0);
	for (int i=0; i < n; ++i) {
		dp[i][i] = arr[i];
		if (i + 1 < n)
			dp[i][i + 1] = max(arr[i], arr[i + 1]);
	}
	for (int cl = 3; cl <= n; ++cl) {
		for (int i=0; i + cl - 1 < n; ++i) {
			int j = i + cl - 1;
			dp[i][j] = max(
				arr[i] + min(dp[i+2][j], dp[i+1][j-1]),
				arr[j] + min(dp[i][j-2], dp[i+1][j-1])
			);
		}
	}
	return dp[0][n-1];
}

int main() {
	int n;
	cin >> n;
	vi arr(n);
	READ_ARR(arr);
	cout << find_max_dp(arr) << endl;
	return 0;
}