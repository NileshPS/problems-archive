#include <bits/stdc++.h>

using namespace std;

struct node {
	char c;
	int count;
};

struct compare {
	bool operator()(const node &a, const node &b) {
		return a.count < b.count;
	}
};

int main() {
	string str;
	cout << "Enter a string : ";
	cin >> str;
	int len = str.length();
	priority_queue< node, vector<node>, compare> pq;
	int count[256];
	memset(count, 0, 256);
	for(int i=0; i<len; ++i) {
		count[str[i] - 'a']++;
	}
	for(char c = 'a'; c <= 'z'; ++c) {
		if(count[c-'a']) {
			node newNode;
			newNode.c = c;
			newNode.count = count[c-'a'];
			pq.push(newNode);
		}
	}

	node prevNode;
	prevNode.c = '#';
	prevNode.count = -1;
	string result = "";
	while(!pq.empty()) {
		node curNode = pq.top();
		pq.pop();

		if(prevNode.count > 0) {
			pq.push(prevNode);
		}
		curNode.count--;
		result += curNode.c;
		prevNode = curNode;
	}
	if(len == result.length()) {
		cout << "Answer is :  " << result << endl;
	} else {
		cout << "Not possible!" << endl;
	}
	return 0;
}