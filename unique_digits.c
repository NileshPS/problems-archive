#include <stdio.h>
#include <stdlib.h>

int hasUniqueDigits(int n) {
	int mask = 0, rem;
	while( n) {
		rem = n % 10;
		n/=10;
		if( (mask & 1 << rem) )
			return 0;
		mask |= ( 1 << rem);
	}

	return 1;
}


int main() {
	int m, n;
	scanf("%d %d", &m, &n);
	for(int i=m; i<=n; ++i) {
		if(hasUniqueDigits(i))
			printf("%d ", i);
	}
	return 0;
}