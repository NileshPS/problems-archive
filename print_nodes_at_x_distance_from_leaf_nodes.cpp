#include <bits/stdc++.h>

using namespace std;

struct node {
    int data;
    node *left, *right;
};

void print(node *root, vector<int> &path, set<int> &visited, int distance) {
    if(!root) return;
    if(!root->left && !root->right) {
        int len = path.size();
        auto it = visited.find(path[len - distance]);
        if(len - distance >= 0 && it == visited.end()) {
            cout << path[len - distance] << "  ";
            visited.insert(path[len-distance]);
            return;
        }
    }
    path.push_back(root->data);
    print(root->left, path, visited, distance);
    print(root->right, path, visited, distance);
    path.pop_back();
}


int main() {
    /**
            1
          /   \
         2      3
        / \    / \
       4   5  6   7
          /         \ 
         9           8
    **/
    node *n[10];
    for(int i=0; i < 10; ++i) {
        n[i] = new node;
        n[i]->data = i;
        n[i]->left = n[i]->right= nullptr;
    }
    n[1]->left = n[2], n[1]->right = n[3];
    n[2]->left = n[4], n[2]->right  = n[5];
    n[3]->left = n[6], n[3]->right = n[7];
    n[5]->left = n[9];
    n[7]->right = n[8];
    vector<int> path;
    set<int> visited;
    print(n[1], path, visited, 1);
    return 0;
}