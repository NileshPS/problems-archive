#include <bits/stdc++.h>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector< ii > vii;


/**
 * A Sample heap implementation.
 * Use it as a max-heap. If min heap is required, push the data after multiplying with -1.
 */

class Heap {
private:
	vi arr;
	int size;
public:
	Heap() { size = 0; }
	void push(int n) {
		if(arr.size() > size) {
			arr[size] = n;
		}
		else {
			arr.push_back(n);
		}

		++size;
		int j = size - 1;
		while(j > 0) {
			int p = parent(j);
			if(arr[j] > arr[p]) {
				swap(arr[j], arr[p]);
				j = p;
			}
			else
				break;
		}
	}


	int extractTop() {
		if(size == 0) return INT_MIN;
		int temp = arr[0];
		arr[0] = arr[size - 1];
		--size;
		bool flag = true;
		int i  = 0;
		while(flag) {
			int leftChild = left(i), rightChild = right(i);
			if(rightChild < size) {
				if(arr[i] < arr[rightChild] && arr[rightChild] >= arr[leftChild]) {
					swap(arr[i], arr[rightChild]);
					i = rightChild;
				}
				else if(arr[i] < arr[leftChild] && arr[leftChild] > arr[rightChild]) {
					swap(arr[i], arr[leftChild]);
					i = leftChild;
				}
				else
					flag = false;
			}
			else if(leftChild < size) {
				if(arr[i] < arr[leftChild]) {
					swap(arr[i], arr[leftChild]);
					i = leftChild;
				}
				else {
					flag = false;
				}
			}
			else {
				flag = false;
			}
		}

		return temp;
	}

	inline int parent(int i) {
		return  i == 0?-1:(i - 1)/2;
	}

	inline int left(int i) {
		return 2*i + 1;
	}

	inline int  right(int i) {
		return 2*i + 2;
	}

	bool empty() { return size <= 0;}
	void print() {
		if(size <= 0) {
			cout<<"EMpty!"<<endl;
		}
		else {
			for(int i=0; i<size; ++i) {
				cout<<arr[i]<<" ";
			}
		}
		cout<<endl;
	}
};


/**
 * Driver function.
 * @return [description]
 */
int main() {
	
	Heap mHeap;
	int ch, temp;
	do {
		cout<<"1. Insert\n2. Delete\n3. Print\n4. Exit\nEnter your choice : ";
		cin>>ch;
		switch(ch) {
			case 1:
				cout<<"Enter a number : ";
				cin>>temp;
				mHeap.push(temp);
				cout<<"Success!"<<endl;
				break;
			case 2:
				if(mHeap.empty()) {
					cout<<"Empty!"<<endl;
				}
				else {
					cout<<mHeap.extractTop()<<" removed!"<<endl;
				}
				break;
			case 3:
				mHeap.print();
				break;
			case 4:
				cout<<"Goodbye!"<<endl;
				break;
			default:
				cout<<"Invalid option!"<<endl;
		}
	} while(ch != 4);
	return 0;
}
