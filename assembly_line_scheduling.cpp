#include "m_includes.hpp"
#define NUM_STATIONS 4

using namespace std;

int optimal_schedule_time(int noOfStations, int a[][NUM_STATIONS], 
						int t[][NUM_STATIONS], int entry[],
							int exit[]) {
	int dp1[noOfStations], dp2[noOfStations];
	dp1[0] = entry[0] + a[0][0];
	dp2[0] = entry[1] + a[1][0];
	for (int i=1; i < noOfStations; ++i) { 
		dp1[i] = min(dp1[i-1] + a[0][i], dp2[i-1] + t[0][i] + a[0][i]);
		dp2[i] = min(dp2[i-1] + a[1][i], dp1[i-1] + t[1][i] + a[0][i]);
	}
	return min(dp1[noOfStations - 1] + exit[0], 
			   dp2[noOfStations - 1] + exit[1]);
}

int main(int argc, char **argv) {
	int noOfLines = 2, noOfStations = 4;
	int a[][4] = {
		{4, 5, 3, 2},
		{2, 10, 1, 4}
	};
	int t[][4] = {
		{0, 7, 4, 5},
		{0, 9, 2, 8}
	};
	int entry[] = {10, 12}, exit[] = {18, 7};

	cout << optimal_schedule_time(noOfStations, a, t , entry, exit) << endl;
	return 0;
}