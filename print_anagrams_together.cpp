/*input
1
45
8 29 38 27 29 11 29 37 42 35 26 33 16 40 34 49 9 45 24 6 11 36 40 34 45 47 13 6 44 43 1 1 21 40 29 1 50 7 38 43 42 15 25 9 5 
10
*/
#include "m_includes.hpp"

using namespace std;


void brute(const vi &a, int n, int s) {
	for (int i=0; i < n; ++i) {
		for (int j =i + 1; j < n; ++j) {
			for (int k = j + 1; k < n; ++k) {
				for (int l = k + 1; l < n; ++l) {
					if( a[i] + a[j] + a[k] + a[l] == s) {
						cout << a[i] << ", "<< a[j] << ", " << a[k] << ", " << a[l];
						return;
					}
				}
			}
		}
	}
	cout << "NO" << endl;
	return;
}

int has_quad(const vi &a, int n, int s) {
    unordered_map<int, pii> umap;
    for (int i=0; i < n; ++i){ 
        for (int j = i + 1; j < n; ++j) {
            auto it = umap.find(s - a[i] - a[j]);
            if (it == umap.end()) {
                umap[a[i] + a[j]] = {i, j};
            } else {
                pii pos = it->second;
                if (pos.first == i || pos.second == i ||
                    pos.first == j || pos.second == j)
                    continue;
                return 1;
            }
        }
    }
    return 0;
}

int main() {
    int t;
    cin >> t;
    while (t--) {
        int n, x;
        cin >> n;
        vi arr(n);
        READ_ARR(arr);
        cin >> x;
        brute(arr, n, x);
    }
    return 0;
}