#include "m_includes.hpp"

using namespace std;

bool find(const vi &arr, int n, int csum, int tsum) {
    if(n == 0)
        return csum*2 == tsum;

    return find(arr, n-1, csum, tsum) || find(arr, n-1, csum + arr[n-1], tsum);
}

int main() {
    vi arr = {1, 5,3};
    int sum = 0;
    for(auto &el : arr)
        sum += el;
    cout << find(arr, arr.size(), 0, sum) << endl;
    return 0;
}
