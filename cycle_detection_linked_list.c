#include <stdio.h>
#include <malloc.h>

typedef struct __node {
	int data;
	struct __node *link;
} node;


node *findCycleStart(node *header) {
	node *slow_ptr = header, *fast_ptr = header;

	while(fast_ptr) {
		fast_ptr = fast_ptr->link?fast_ptr->link->link:NULL;
		slow_ptr = slow_ptr->link;
		if(slow_ptr == fast_ptr) break;
	}

	if(fast_ptr == NULL) {
		printf("Sorry ! No cycle found!\n");
		return NULL;
	}

	slow_ptr = header;

	while(slow_ptr != fast_ptr) {
		slow_ptr = slow_ptr->link;
		fast_ptr = fast_ptr->link;
	}

	return slow_ptr;
}



void removeCycle(node *header) {
	node *fast_ptr = header, *slow_ptr = header;
	node *last_ptr; /* Special case : when the entire list is a loop! */
	while(fast_ptr) {
		last_ptr = fast_ptr;
		fast_ptr = fast_ptr->link?fast_ptr->link->link:NULL;
		slow_ptr = slow_ptr->link;
		if(fast_ptr == slow_ptr) break;
	}

	if(fast_ptr == NULL) /* no cycle */
		return;

	slow_ptr  = header;
	while(slow_ptr != fast_ptr) {
		slow_ptr = slow_ptr->link;
		last_ptr = fast_ptr;
		fast_ptr = fast_ptr->link;
	}
	last_ptr->link = NULL;
}

int main() {
	node *header= (node *)malloc(sizeof(node));
	header->link = NULL;
	header->data = 0;
	node *ptr = header;
	for(int i=0; i<10; ++i) {
		node *new_node =(node *)malloc(sizeof(node));
		new_node->link = NULL;
		new_node->data = i + 1;
		ptr->link = new_node;
		ptr = new_node;
	}

	node *begin = ptr;
	for(int i = 0; i<7; ++i) {
		node *new_node = (node *)malloc(sizeof(node));
		new_node->link = NULL;
		new_node->data = 11 + i;
		ptr->link = new_node;
		ptr = new_node;
	}


	//make the cycle 
	
	ptr->link = begin;


	node *detected_begin = findCycleStart(header);
	if(detected_begin == begin) {
		printf("Success!\n");
	}
	else{
		printf("Not matching!\n");
	}

	removeCycle(header);

	ptr = header->link;
	while(ptr) {
		printf("%d  ", ptr->data);
		ptr = ptr->link;
	}

	printf("\n");
	return 0;
}