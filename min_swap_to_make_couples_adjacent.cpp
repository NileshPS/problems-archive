/*input
4
8 3 1 2
*/
#include "m_includes.hpp"

using namespace std;



int min_swap_recur(int *arr, int *pairs, int *index, int i, int n) {
    if (i >= n) return 0;
    if (pairs[arr[i]] == arr[i+1]) return 0;
    // Else find index of pair of arr[i]
    int pi = index[pairs[arr[i]]];
    swap (arr[i + 1], arr[pi]);
    index[arr[pi]] = pi;
    int a = min_swap_recur(arr, pairs, index, i + 2, n);
    swap (arr[i + 1], arr[pi]);
    index[arr[pi]] = pi;

    pi = index[arr[i + 1]];
    swap (arr[i], arr[pi]);
    index[arr[pi]] = pi;
    int b = min_swap_recur(arr, pairs, index, i + 2, n);
    //backtrack
    swap (arr[i], arr[pi]);
    index[arr[pi]] = pi;

    return min(a, b) + 1;
}

int minSwaps(int n, int *pairs, int *arr) {
    int index[2 * n  + 1];
    for (int i=1; i <= 2 * n; ++i)
        index[arr[i]] = i;
    return min_swap_recur(arr, pairs, index, 1, 2 * n);    
}


int main() {
    // For simplicity, it is assumed that arr[0] is
    // not used.  The elements from index 1 to n are
    // only valid elements
    int arr[] = {0, 3, 5, 6, 4, 1, 2};
 
    // if (a, b) is pair than we have assigned elements
    // in array such that pairs[a] = b and pairs[b] = a
    int pairs[] = {0, 3, 6, 1, 5, 4, 2};
    int m = sizeof(arr)/sizeof(arr[0]);
 
    int n = m/2;  // Number of pairs n is half of total elements
 
    // If there are n elements in array, then
    // there are n pairs
    cout << "Min swaps required is " << minSwaps(n, pairs, arr);
    return 0;
}