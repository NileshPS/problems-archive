/**
  Program to find the largest 2d sub matrix sum of the input matrix.
  Native implementation is O(n^6) complex, hence the max size of matrix is limited to <= 100 else TLE error.
  Preprocessing is done such that clone[i][j] holds the sum of the sub-matrix mat[0][0] - mat[i][j].
  Now the sum of any submatrix can be found in O(1) time. Time complexity reduces to O(n^4).
 **/

#include <bits/stdc++.h>

using namespace std;

typedef vector<int> vi;

int max_2d_range_sum(vector< vector<int> > &mat) {
	int n = mat.size();
	//clone the actual matrix
	vector< vi > clone = mat;
	for(int i=0; i<n; ++i)
		for(int j=0; j<n; ++j) {
			if(i>0) clone[i][j] += clone[i-1][j];
			if(j> 0) clone[i][j] += clone[i][j-1];
			if(i > 0 && j > 0) clone[i][j] -= clone[i-1][j-1];
		}	
	int maxSum = INT_MIN;
	int rs, cs, re, ce;
	for(int i=0; i<n; ++i) {
		for(int j=0; j<n; ++j) {
			for(int k=i; k<n; ++k) {
				for(int l=j; l<n; ++l) {
					//find the sum of submatrix (i, j) -> (k, l)
					int sum = clone[k][l];
					if(i > 0)
						sum-=clone[i-1][l];
					if(j > 0)
						sum-=clone[k][j-1];

					if( i > 0 && j > 0)
						sum += clone[i-1][j-1];

					//maxSum = max(sum, maxSum);
					if(sum > maxSum) {
						while(false);
						rs = i, cs = j, re = k, ce = l;
						maxSum =sum;
					}
				}
			}
		}
	}

	printf("[%d, %d] -> [%d, %d]\n", rs, cs, re, ce);
	return maxSum;
}

int main() {
	int n;
	cin>>n;
	if(n <= 0) {
		cout<<"Invalid matrix"<<endl;
		return 0;
	}

	cout<<"Enter matrix : ";
	vector< vi > mat(n, vi(n, 0));
	for(int i=0; i<n; ++i)
		for(int j=0; j<n; ++j)
			cin>>mat[i][j];

	
	cout<<max_2d_range_sum(mat)<<endl;
	return 0;
	
}
