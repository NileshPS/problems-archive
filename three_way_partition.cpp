#include "m_includes.hpp"

// Three way  partition.

using namespace std;

void m_sort(vi &arr) {
    int len = arr.size();
    int i = -1, j = -1, k;
    for(k =0; k < len; ++k ){
        if(arr[k] == 2) {
            // Continue;
        }
        else if (arr[k] == 1) {
            ++j;
            swap(arr[j], arr[k]);
        }
        else {
            ++i;
            arr[i] = 0;
            j = max(j, i);
            if(j != i) {
                ++j;
                arr[j] = 1;
            }
            if( j != k) 
                arr[k] = 2;
        }
    }
}

int threeWayPartition(int A[], int end)
{
    int start = 0, mid = 0;
    int pivot = 1;
 
    while (mid <= end)
    {
        if (A[mid] < pivot)            // current element is 0
        {
            swap(A[start], A[mid]);
            ++start, ++mid;
        }
        else if (A[mid] > pivot)    // current element is 2
        {
            swap(A[mid], A[end]);
            --end;
        }
        else                        // current element is 1
            ++mid;
    }
}

int main() {
    vi arr= {0, 0, 0, 0, 0, 0, 2};
    m_sort(arr);
    for(auto &el : arr)
        cout << el << "  ";
    cout << endl;
    return 0;
}