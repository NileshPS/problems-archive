#include <bits/stdc++.h>


using namespace std;

int main() {
	char str[200];
	cin.getline(str, 100);

	int spaceCount = 0;
	size_t len = strlen(str);
	for(int i=0; i<len; ++i)
		if(str[i] == ' ')
			++spaceCount;

	int newLength = len + 2*spaceCount;
	if(newLength >= 200) {
		cout<<"buffer limit exceeded!"<<endl;
		return -1;
	}

	str[newLength] = '\0';
	for(int i=len-1; i>=0; --i) {
		if(str[i] == ' ') {
			str[newLength-1] = '0';
			str[newLength - 2] = '2';
			str[newLength-3] = '%';
			newLength -= 3;
		}	
		else {
			str[newLength - 1] = str[i];
			newLength--;
		}
	}
	cout<<str<<endl;
}