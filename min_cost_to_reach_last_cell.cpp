#include "m_includes.hpp"

using namespace std;

inline bool is_valid(int i, int j, int m, int n) {
    return i >= 0 && i < m && j >= 0 && j < n;
}

int min_cost(const matrix &mat, int i, int j) {
    int m = mat.size(), n = mat[0].size();
    if(! is_valid(i, j, m, n))
        return INT_MAX;
    if(i == m-1 && j == n-1)
        return mat[i][j];
    return min(
        min_cost(mat, i + 1, j),
        min_cost(mat, i, j + 1)
    ) + mat[i][j];
}

int min_cost_dp(const matrix &mat) {
    int m = mat.size(), n = mat[0].size();
    matrix dp(m, vi(n, 0));
    for(int i=0; i<m; ++i) {
        for(int j=0; j < n; ++j) {
            if(i == 0 && j == 0)
                dp[i][j] = mat[i][j];
            else if(i == 0) 
                dp[i][j] = dp[i][j-1] + mat[i][j];
            else if(j == 0)
                dp[i][j] = dp[i-1][j] + mat[i][j];
            else
                dp[i][j]  = min(dp[i-1][j], dp[i][j-1]) + mat[i][j];
        }
    }
    return dp[m-1][n-1];
}
int main() {
    matrix mat = {

        { 4, 7, 8, 6, 4 },
        { 6, 7, 3, 9, 2 },
        { 3, 8, 1, 2, 4 },
        { 7, 1, 7, 3, 7 },
        { 2, 9, 8, 9, 3 }
    };
    cout << min_cost_dp(mat) << endl;
    return 0;
}