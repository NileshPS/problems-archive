#include <bits/stdc++.h>

using namespace std;

typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector< ii > vii;


#define DEBUG 

struct node {
	int data, height;
	node *left, *right;
};


struct node *ROOT;

int height(node *n) {
	if(!n) return 0;
	return n->height;
}

node *new_node(int data = 0) {
	node *__node = new node;
	__node->left = __node->right = NULL;
	__node->data = data, __node->height = 1;
	return __node;
}

node *leftRotate(node *x) {
	if(!x) return NULL;
	/**
	 * Assume the following structure
	 * 				X
	 * 			   / \
	 * 			  a   Y
	 * 			     / \
	 * 			    b   c
	 */
	
	node *Y = x->right;
	x->right = Y->left;
	Y->left = x;

	//The order in which the height is calculated is very important!
 	x->height = 1 + max(height(x->left), height(x->right));
	Y->height = 1 + max(height(Y->left), height(Y->right));
	return Y; // new root
}

node *rightRotate(node *x) {
	if(!x) return NULL;

	/**
	 * Assume the following structure
	 * 				x
	 * 			   /  \
	 * 			  y    c    
	 * 			 / \
	 * 			a   b
	 */
	
	node *y = x->left;
	x->left = y->right;
	y->right = x;
	x->height = 1 + max(height(x->left), height(x->right));
	y->height = 1 + max(height(y->left), height(y->right));
	return y;
}


node *successor(node *root) {
	node *ptr = root->right;
	while(ptr && ptr->left)
		ptr = ptr->left;
	return ptr;
}

node *predecessor(node *root) {
	node *ptr = root->left;
	while(root && root->right)
		ptr = ptr->right;
	return ptr;
}


int getBalance(node *root) {
	if(!root) return 0;
	return height(root->left) - height(root->right);
}

node *avl_insert(node *root, int data) {
	if(!root) 
		return new_node(data);
	else if(data < root->data)
		root->left = avl_insert(root->left, data);
	else if( data > root->data)
		root->right = avl_insert(root->right, data);
	else
		return root; //duplicate found!

	//Now the new node is inserted, time to update the height of the node
	root->height =  1 + max(height(root->left), height(root->right));

	int balanceFactor = height(root->left) - height(root->right);

	if(balanceFactor < -1) {
		if(data < root->right->data) {
			root->right = rightRotate(root->right);
			return leftRotate(root);
		}
		else {
			return leftRotate(root);
		}
	}
	else if(balanceFactor > 1) {
		if(data < root->left->data) {
			return rightRotate(root);
		}
		else {
			root->left = leftRotate(root->left);
		}
	}


	return root;
}




node *avl_delete(node *root, int data) {
	if(!root) return NULL;

	if(data < root->data) {
		root->left = avl_delete(root->left, data);
	}
	else if(data > root->data) {
		root->right = avl_delete(root->right, data);
	}
	else { // key matches
		if(root->left && root->right) {
			//Two child case
			int temp = successor(root)->data;
			avl_delete(ROOT, temp);
			root->data = temp;
		}
		else if(root->left == NULL && root->right == NULL) {
			//no child case
			node *tmp = root;
			root = NULL;
			delete tmp;
		}
		else {
			//one child case
			node *child = root->right?root->right:root->left;
			*root = *child;
			delete child;
		}
	}

	if(!root) return NULL;

	root->height = 1 + max(height(root->left), height(root->right));

	int bf = getBalance(root);

	if(bf  > 1) {
		if(getBalance(root->left) >= 0) {
			//left left case
			root = rightRotate(root);
		}
		else {
			root->left = leftRotate(root->left);
			root = rightRotate(root);
		}
	}
	else if(bf < -1) {
		if(getBalance(root->right) <= 0) {
			//right right
			root = leftRotate(root);
		}
		else {
			root->right = rightRotate(root->right);
			root = leftRotate(root);
		}
	}
	return root;
}


bool empty(node *root) {
	return root == NULL;
}



void inorder(node *root) {
	if(root) {
		inorder(root->left);
		cout<<root->data<<" ";// << root->height<< " ) ";
		inorder(root->right);
	}
}

void preorder(node *root) {
	if(root) {
		cout<<root->data<<" ";
		preorder(root->left);
		preorder(root->right);
	}
}

bool isBalanced(node *root) {
	if(! root) return true;
	if(abs(height(root->left) - height(root->right)) > 1)
		return false;

	return isBalanced(root->left) && isBalanced(root->right);
}


bool avl_search(node *root, int data) {
	if(!root) return false;
	if(data == root->data) return true;
	return data < root->data?avl_search(root->left, data):avl_search(root->right, data);
}




int main() {
	int ch, tmp; 
	do {
		cout<<"1. Insert\n2. Delete\n3. Inorder\n4. isBalanced?\n5. Exit\nEnter an option : ";
		cin>>ch;
		switch(ch) {
			case 1:
				cout<<"Enter an element : ";
				cin>>tmp;
				if(!avl_search(ROOT, tmp) && (ROOT = avl_insert(ROOT, tmp))) {
					cout<<"Done!"<<endl;
				} else {
					cout<<"Duplicate key!"<<endl;
				}

				break;

			case 2:
				cout<<"Enter an element : ";
				cin>>tmp;
				if(avl_search(ROOT, tmp)) {
					ROOT = avl_delete(ROOT, tmp);
					cout<<"Success!"<<endl;
				}
				else {
					cout<<"Not found!"<<endl;
				}
				break;

			case 3:
				if(empty(ROOT)) {
				 cout<<"Empty!"<<endl;
				}
				else {
					cout<<"Inorder : ", inorder(ROOT);
					cout<<"\nPreorder : ",  preorder(ROOT);
					cout<<endl;
				}

				break;

			case 4:
				cout<<( isBalanced(ROOT)?"YES":"NO")<<endl;
				break;

			case 5:
				cout<<"Goodbye"<<endl;
				break;

			default:
				cout<<"Invalid number!"<<endl;
			}
		}while(ch != 5);
	
	return 0;
}
