#include "m_includes.hpp"

using namespace std;

struct node {
    int data;
    node *left, *right;
};

node *get_node(int data = 0, node *left = nullptr, node *right = nullptr){
    node *n = new node;
    n->data = data;
    n->left = left;
    n->right = right;
    return n;
}

void init(node *n, int data , node *left = nullptr, node *right = nullptr) {
    n->data = data;
    n->left = left;
    n->right = right;
}

void print_paths_with_sum(node *root, int sum, vector<int> &path) {
    if(!root) return;
    if(sum == 0) {
        for(auto &el : path)
            cout << el << "  ";
        cout << endl;
    }
    path.push_back(root->data);
    print_paths_with_sum(root->left, sum - root->data, path);
    print_paths_with_sum(root->right, sum - root->data, path);
    path.pop_back();
}

int main() {
/*
          10
         /  \
        5   -3
       / \    \
      3   2   11
         / \   \
         3  -2   1
*/
    node *n[9];
    for(int i=0;i < 9; ++i) {
        n[i] = get_node();
    }
    init(n[0], 1);
    init(n[1], -2);
    init(n[2], 3);
    init(n[3], 11, nullptr, n[0]);
    init(n[4], 2, n[4], n[1]);
    init(n[5], 3);
    init(n[6], -3, nullptr, n[3]);
    init(n[7], 5, n[5], n[4]);
    init(n[8], 10, n[7], n[6]);
    vector<int> path;
    print_paths_with_sum(n[8], 8, path);
    cout << endl;
    return 0;
}