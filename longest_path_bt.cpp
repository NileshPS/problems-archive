/**
Given a Binary Tree find the length of the longest path which comprises of nodes with consecutive values in increasing order. 
Every node is considered as a path of length 1.
**/
#include "m_includes.hpp"
#define is_leaf(n) (!n->left && !n->right)

using namespace std;

struct node {
    int data;
    node *left, *right;
};


void find_longest_path(node *root, int cur_path_len, int &max_path_len, int expected_val) {
    if(! root)
        return;
    if(root->data == expected_val) {
        max_path_len = max(max_path_len, ++cur_path_len);
        find_longest_path(root->left ,cur_path_len, max_path_len, root->data + 1);
        find_longest_path(root->right ,cur_path_len, max_path_len, root->data + 1);
        return;
    } 
    cur_path_len = 1;
    find_longest_path(root->left, 1, max_path_len, root->data + 1);
    find_longest_path(root->right, 1, max_path_len, root->data + 1);
}

void find_max_len(node *root, int cur_len, int &max_len) {
    if(!root) return;
    max_len = max(cur_len, max_len);
    if(root->left && root->data + 1 == root->left->data) 
        find_max_len(root->left, cur_len +1, max_len);
    else 
        find_max_len(root->left, 1, max_len);

    if(root->right && root->data + 1 == root->right->data) 
        find_max_len(root->right, cur_len + 1, max_len);
    else
        find_max_len(root->right, 1, max_len);

}

int find_longest_path(node *root) {
    // int cur_path_len = 1, max_path_len = 1;
    // find_longest_path(root, cur_path_len, max_path_len, 0);
    // return max_path_len;
    /** alternative **/
    int max_len = 1;
    find_max_len(root, 1, max_len);
    return max_len;
}


int main() {
    node *n[10];
    for(int i=0; i< sizeof(n)/sizeof(n[0]); ++i) {
        n[i] = new node;
        n[i]->left = n[i]->right = nullptr;
        n[i]->data = i;
    }  
    n[1]->left = n[2], n[1]->right = n[4];
    n[2]->left = n[3];
    n[4]->left = n[5], n[5]->right = n[6];
    n[6]->left = n[7];
    cout << "Longest path length is : " << find_longest_path(n[1]) << endl;
    return 0;
}

