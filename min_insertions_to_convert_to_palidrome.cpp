/*input
3
abcd
aba
geeks
*/
#include "m_includes.hpp"

using namespace std;

bool ispali(const string &str, int l, int r) {
	while (l < r) {
		if (str[l++] != str[r--]) return false;
	}
	return true;
}
int find_min_insert(const string &str, int l, int r) {
	if (l == r || ispali(str, l, r)) return 0;
	return min(
		find_min_insert(str, l, r-1),
		find_min_insert(str, l + 1, r)
	) + 1;
}

int find_min_insert(const string &str) {
	int n = str.size();
	matrix dp;
	matrix_init(dp, n, n, 0);
	for (int i=0; i < n; ++i) {
		if (i + 1 < n)
			dp[i][i+1] = (str[i] == str[i+1]) ? 0 : 1;
	}
	for (int cl = 3; cl <= n; ++cl) {
		for (int i=0; i + cl - 1 < n; ++i) {
			int j = i + cl - 1;
			if (str[i] == str[j]) 
				dp[i][j] = dp[i+1][j-1];
			else
				dp[i][j] = min(dp[i+1][j], dp[i][j-1]) + 1;
		}
	}
	return dp[0][n-1];
}

int main() {
	int t;
	cin >> t;
	while (t--) {
		string str;
		cin >> str;
		cout << find_min_insert(str) << endl;
	}
	return 0;
}
