// Rearrange positive and negative elements of an array such that their relative order is not altered.
// Do not use any auxiliary data structures.

#include "m_includes.hpp"

using namespace std;

void reverse(vi &arr, int l, int r) {
    if( l < r) {
        swap(arr[l], arr[r]);
        reverse(arr, ++l, --r);
    }
}

void merge(vi &arr, int l, int mid, int r) {
    int i, j;
    i = l;
    while(i <= mid && arr[i] < 0) i++;
    j = mid + 1;
    while(j <= r && arr[j] < 0) j++;
    reverse(arr, i, mid);
    reverse(arr, mid + 1, j-1);
    reverse(arr, i, j-1);
}

void rearrange(vi &arr, int l, int r) {
    if( l >= r) return;
    int mid = (l + r) >> 1;
    rearrange(arr, l, mid);
    rearrange(arr, mid + 1, r);
    merge(arr, l, mid, r);
}

int main() {
    vi arr{-1, 9, 2, -3, 6, 5, -7, 10, -2, 11};
    //merge(arr, 0, 5, arr.size() - 1);
    rearrange(arr, 0, arr.size() - 1);
    for(auto &el : arr)
        cout << el << "  ";
    cout << endl;
    return 0;
}