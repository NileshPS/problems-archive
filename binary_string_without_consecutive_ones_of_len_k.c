#include "__includes.h"



/**
 * Main function.
 */
int main() {
	int k;
	printf("Enter value of k : ");
	scanf("%d", &k);
	int *a = (int *)malloc(sizeof(int) * (k + 1)); /** No of string ending with 0 **/
	int *b = (int *)malloc(sizeof(int) * (k + 1)); // No of sttring ending with 1.
	a[0] = b[0] = a[1] = b[1] = 0;
	a[2] = 2;
	b[2]  = 1;
	for(int i=3; i<=k; ++i) {
		a[i] = a[i-1] + b[i-1];
		b[i] = a[i-1];
	}

	printf("Answer is : %d\n", a[k] + b[k]);
	
	free(a);
	free(b);
	return 0;
}