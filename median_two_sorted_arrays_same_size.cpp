#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

typedef vector<int> vi;

#define IS_ODD(n) ((n) & 1)
#define IS_EVEN(n) (! IS_ODD(n))

static inline double median(const vi &arr, int start, int len) {
    int mid = start + (len >> 1);
    return IS_ODD(len) ? arr[mid] : (arr[mid] + arr[mid - 1]) * 0.5f;
}

static double findMedianBySorting(const vi &arr_one, const vi &arr_two) {
    assert (arr_two.size() == arr_one.size());
    int m = arr_two.size() + arr_one.size();

    vi out(m);

    auto it_1 = arr_one.cbegin(), it_2 = arr_two.cbegin();
    auto it_out = out.begin();

    while (it_1 != arr_one.cend() || it_2 != arr_two.cend()) {
        if (it_1 == arr_one.cend()) {
            *it_out = *it_2++;
        } else if (it_2 == arr_two.cend()) {
            *it_out = *it_1++;
        } else if (*it_1 < *it_2) {
            *it_out = *it_1++;
        } else {
            *it_out = *it_2++;
        }
        ++it_out;
    }
    return median(out, 0, m);
}

double findMedianBinarySearch(const vi &arr1, const vi &arr2) {
    assert (arr1.size() == arr2.size());
    int n = arr1.size(), l1 = 0, l2 = 0;
    while (n > 2) {
        double m1 = median(arr1, l1, n), 
               m2 = median(arr2, l2, n);
        // printf("L1 = %d L2 = %d M1 = %.2f M2 = %.2f\n", l1, l2, m1, m2);
        if (m1 == m2) {
            return m1;
        } else if (m1 < m2) {
            l1 += (n >> 1) - IS_EVEN(n);
        } else {
            l2 += (n >> 1) - IS_EVEN(n);
        }
        n = (n >> 1) + 1;
    }
    return  ( max(arr1[l1], arr2[l2]) + min(arr1[l1 + 1], arr2[l2 + 1])) * 0.5f;
}

template<class T>
void print_vector(const vector<T> &vec) {
    for (auto &el : vec) {
        cout << el << " ";
    }
    cout << endl;
}


int main() {
    int N = 4987;

    srand(1000);

    vi arr1(N), arr2(N);
    for (int t = 0; t < N; ++t) {
        *(arr1.begin() + t) = rand() % (5 * N);
        *(arr2.begin() + t) = rand() % (5 * N);
    }

    sort(arr1.begin(), arr1.end());
    sort(arr2.begin(), arr2.end());

    // print_vector(arr1);
    // print_vector(arr2);

    cout << "Median by sorting = " << findMedianBySorting(arr1, arr2) << endl;
    cout << "Median by binary search = " << findMedianBinarySearch(arr1, arr2) << endl;
    return 0;
}