#include "m_includes.hpp"

using namespace std;

enum color { WHITE, GRAY, BLACK};

class Graph {
	typedef struct {
		int to, weight;
	} edge;
	int n;
	vector< vector<edge> > adjList;
	bool directed;

	void dfs_recur_util(vector<color> &col, int node, vi &ret) {
		col[node] = GRAY;
		ret.push_back(node);
		auto it = adjList[node].begin();
		while (it != adjList[node].end()) {
			if (col[*it] == WHITE)
				dfs_recur_util(col, *it, ret);
		}
		col[node] = BLACK;
	}

public:
	Graph(int n, bool directed = false) {
		this->n = n;
		adjList.resize(n);
		this->directed = directed;
	}

	void addEge(int u, int v, int w = 0) {
		adjList[u].insert(v);
		if (! directed)
			adjList[v].insert(u);
	}

	vi dfs_recur() {
		vector<color> col(n, WHITE);
		vi ret;
		for (int i=0; i < n; ++i) {
			if (col[i] == WHITE)
				dfs_recur_util(col, i, ret);
		}
		return ret;
	}

	vi dfs(int src) {
		stack<int> st;
		vi ret;
		ret.push_back(src);
		st.push(src);
		vb visited(n, false);
		while (!st.empty()) {
			int top = st.top();
			st.pop();
			for (auto &edge : adjList[top]) {
				if (! visited[edge]) {
					visited[edge] = true;
					st.push(edge);
				}
			}
			ret.push_back(top);
		}
		return ret;
	}
}


int main() {
	int n;
	cin >> n;
}