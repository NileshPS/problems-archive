/*input
czcaba
*/
#include <bits/stdc++.h>

using namespace std;


typedef vector<int> vi;

string getNewString(const string &s) {
    string ret = "@#";
    for (int i=0; i < s.size(); ++i) {
        ret += s.substr(i, 1) + "#";
    }
    ret += "#";
    return ret;
}

void printLargestPalidrome(const string &s) {
    string newString = getNewString(s);
    vector<int> mem(newString.size(), 0);
    int c = 0, r = 0, len = newString.size();
    for (int i=1; i < len; ++i) {
        int idash = c - (i - c);
        if (r > i) {
            mem[i] = min(r - i, mem[idash]);
        }
        while (newString[i + mem[i] + 1] == newString[i - mem[i] - 1]) mem[i]++;
        if (mem[i] + i > r) {
            r = mem[i] + i;
            c = i;
        }
    }
    // Find the index with largest value
    int maxIndex = 0;
    for (int i=1; i < len; ++i) {
        if (mem[maxIndex] < mem[i]) {
            maxIndex = i;
        }
    }
    cout << mem[maxIndex] << endl;
    cout << s.substr( (maxIndex - 1 - mem[maxIndex]) / 2, mem[maxIndex]) << endl;
}

int main() {
    string str;
    cin >> str;
    printLargestPalidrome(str);
    return 0;
}
