#include <bits/stdc++.h>

using namespace std;

void printAll(string str, size_t cur_pos, size_t len) {
	if(cur_pos == len) {
		cout << str << endl;
		return;
	}
	if(str[cur_pos - 1] == '1') {
		printAll(str + "0", cur_pos + 1, len );
	}
	else {
		printAll(str + "0", cur_pos + 1, len);
		printAll(str + "1", cur_pos + 1, len);
	}
}

int main() {
	int n;
	cout << "Enter the value of N : ";
	cin >> n;
	printAll("0", 1, n);
	printAll("1", 1, n);
	return 0;
}