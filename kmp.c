#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <ctype.h>


int *find_lps_array(char *pat, int len) {
	int *lps = (int *)malloc(sizeof(int) * len);
	memset(lps, 0, len);
	int j = 0, i = 1;
	lps[0] = 0;
	while( i < len) {
		if(pat[i] == pat[j]) {
			lps[i] = ++j;
			i++;
		} else {
			if(j != 0) {
				j = lps[j-1];
			} else {
				lps[i] = j = 0;
				i++;
			}
		}
	}
	return lps;
}

void kmp_search(char *text, char *pat) {
	int m  = strlen(text), n = strlen(pat);
	int i = 0, j = 0;
	int *lps = find_lps_array(pat, n);
	while(i < m) {
		if(text[i] == pat[j]) {
			i++;
			j++;
		}
		if(j == n) {
			printf("Pattern found at index :  %d\n", i - n);
			j = lps[j-1];
		}
		else if( i < m && text[i] != pat[j]) {
			if( j != 0) {
				j = lps[j-1];
			} else {
				i++;
			}
		}
	}
	free(lps);
	return;
}

int main() {
	//code here
	char pat[100], text[100];
	printf("Enter text : ");
	scanf("%s", text);
	printf("Enter pattern : ");
	scanf("%s", pat);
	kmp_search(text, pat);
	return 0;
}
