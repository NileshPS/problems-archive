#include <bits/stdc++.h>

#define N 6
#define is_valid(i, j) ((i) >= 0 && (i) < N && (j) >= 0 && (j) < N)

typedef std::vector<int> vi;
typedef std::vector<vi> matrix;
enum direction { d_up, d_down, d_left, d_right};

using namespace std;

matrix optimalPath;
int optimalSize = INT_MAX;

// Can we find the optimal path?
bool find(const matrix &maze, matrix &sol, int row, int col, int index, direction dir) { 
    sol[row][col] = index;
    if(row == N-1 && col == N-1) return true;
    if(dir != d_down && is_valid(row - 1, col) && sol[row -1 ][col] == 0 && maze[row-1][col] == 0)
        if(find(maze, sol, row - 1, col, index + 1, d_up))
            return true;

    if(dir != d_up && is_valid(row + 1, col) && sol[row + 1 ][col] == 0 && maze[row +1 ][col] == 0)
    if(find(maze, sol, row + 1, col, index + 1, d_down))
        return true;

    if(dir != d_left && is_valid(row, col + 1) && sol[row ][col + 1] == 0 && maze[row][col + 1] == 0) 
    if(find(maze, sol, row, col + 1, index + 1, d_right))
        return true;

    if(dir != d_right && is_valid(row, col - 1) && sol[row ][col - 1] == 0 && maze[row][col - 1] == 0)
    if(find(maze, sol, row, col - 1, index + 1, d_left))
        return true;

    sol[row][col] = 0;
    return false;
}

void find_optimal(const matrix &maze, matrix &sol, int row, int col, int index, direction dir) { 
    sol[row][col] = index;
    if(row == N-1 && col == N-1) {
        if(index < optimalSize) {
            optimalSize = index;
            optimalPath = sol;
        }
        sol[N-1][N-1] = 0;
        return;
    }
    if(dir != d_down && is_valid(row - 1, col) && sol[row -1 ][col] == 0 && maze[row-1][col] == 0)
        find_optimal(maze, sol, row - 1, col, index + 1, d_up);

    if(dir != d_up && is_valid(row + 1, col) && sol[row + 1 ][col] == 0 && maze[row +1 ][col] == 0)
        find_optimal(maze, sol, row + 1, col, index + 1, d_down);

    if(dir != d_left && is_valid(row, col + 1) && sol[row ][col + 1] == 0 && maze[row][col + 1] == 0) 
        find_optimal(maze, sol, row, col + 1, index + 1, d_right);

    if(dir != d_right && is_valid(row, col - 1) && sol[row ][col - 1] == 0 && maze[row][col - 1] == 0)
        find_optimal(maze, sol, row, col - 1, index + 1, d_left);

    sol[row][col] = 0;
}


int main() {
    matrix maze {
        {0, 0, 0, 1, 0, 0 },
        {1, 0, 0, 0, 1, 1 },
        {0, 0, 0, 0, 0, 1 },
        {0, 1, 1, 1, 1, 0 },
        {0, 0, 0, 1, 0, 0 },
        {1, 1, 0, 0, 0, 0 }
    };

    matrix sol = matrix(N, vi(N, 0));
    find_optimal(maze, sol, 0, 0, 1, d_down);
    if(optimalSize == INT_MAX) {
        cout << "No solution!" << endl;
    } else {
        cout << "Minimum steps required = " << optimalSize << endl;
        for(auto &row : optimalPath) {
            for(auto &el : row)
                cout << setw(3) << el;
            cout << endl;
        }
    }
    return 0;
}