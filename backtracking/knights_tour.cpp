#include "../m_includes.hpp"

using namespace std;

#define N 6


bool is_valid(int r, int c) {
    return r >= 0 && r < N && c >= 0 && c < N;
}


bool find(matrix &sol, int r, int c, int i) {
    //cout << "Visiting " << r << ", " << c << endl;
    if(i == N*N) {
        // SOlution found.
        sol[r][c] = i;
        return true;
    }
    sol[r][c] = i;
    int dx[8] = {  2, 1, -1, -2, -2, -1,  1,  2 };
    int dy[8] = {  1, 2,  2,  1, -1, -2, -2, -1 };
    for(int i=0;i <8; ++i) {
        int nrow = r + dx[i], ncol = c + dy[i];
        if(is_valid(nrow, ncol) && sol[nrow][ncol] == 0 && find(sol, nrow, ncol, i + 1))
            return true;
    }
    sol[r][c] = 0;
    return false;
}

int main() {
    matrix sol = matrix(N, vi(N, 0));
    if(find(sol, 0, 0, 1)) {
        for(auto &row : sol) {
            for(auto &el : row)
                cout << setw(4) << el;
            cout << endl;
        }
    }
    return 0;
}