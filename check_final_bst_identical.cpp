/**
 * Given two input streams. Find whether the BST constructed from the two streams are identical without actually constructing the tree.
 */

#include "m_includes.hpp"

using namespace std;

bool is_bst_identical(const vi &s1, const vi &s2, int len) {
    if(len == 0)
        return true;
    if(s1[0] != s2[0])
        return false;
    // Else partition
    vi s1_left, s2_left, s1_right, s2_right;
    for(auto &i : s1) {
        if(i < s1[0])
            s1_left.push_back(i);
        else if(i > s1[0])
            s1_right.push_back(i);
    }
    for(auto &i : s2) {
        if(i < s2[0])
            s2_left.push_back(i);
        else if(i > s2[0]) 
            s2_right.push_back(i);
    }
    if(s1_left.size() != s2_left.size() || s1_right.size() != s2_right.size())
        return false;
    return is_bst_identical(s1_left, s2_left, s1_left.size()) && is_bst_identical(s1_right, s2_right, s1_right.size());
}


int main() {
    vi s1 = { 20, 21, 23, 25, 22, 11, 15, 19, 9, 10, 7, 6};
    vi s2 = { 20,  9, 11,  7, 6, 10, 15, 19, 21, 23, 22, 25};
    assert(s1.size() == s2.size());
    int n = s1.size();
    if(is_bst_identical(s1, s2, n)) {
        cout << "YES" << endl;
    }
    else {
        cout << "NO" << endl;
    }
    return 0;
}
