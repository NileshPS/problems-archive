/**
Given a N x N matrix of positive integers,
find shortest path from the first cell of the matrix to its last cell that satisfies given constraints.
**/

#include "m_includes.hpp"

using namespace std;

typedef vector<bool> vb;

vector<vb> visited;

inline bool is_valid(int i, int j, int n) {
    return i >= 0 && i < n && j >= 0 && j < n && !visited[i][j];
}


int min_cost(const matrix &mat, int i, int j, int n) {
    queue<pair< pii, int> > q;
    q.push( { {i, j}, 0});
    while(!q.empty()) {
        pair<pii, int> top = q.front();
        q.pop();
        int i = top.first.first, j = top.first.second, c = mat[i][j];
        if(i == n-1 && j == n-1)
            return top.second;
        if(is_valid(i+c, j, n)) {
            q.push( { {i+c, j}, top.second + 1});
        }
        if(is_valid(i-c, j, n)) {
            q.push( { {i-c, j}, top.second + 1});
        }

        if(is_valid(i, j+c, n)) {
            q.push( { {i, j+c}, top.second + 1});
        }

        if(is_valid(i, j, n)) {
            q.push( { {i, j-c}, top.second + 1});
        }
    }
    return INT_MAX;
}   


int main() {
    int n;
    cin >> n;
    matrix mat(n, vi(n, 0));
    for(auto &row : mat) {
        for(auto &el : row)
            cin >> el;
    }
    visited.resize(n, vb(n, false));
    cout << min_cost(mat, 0, 0, n) << endl;
    return 0;
}

