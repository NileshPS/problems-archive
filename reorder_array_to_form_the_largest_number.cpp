/*input
8
1 34 3 8 9 76 45 4
4
54 546 548  60
*/

/**
 * Given an array of numbers, arrange them in a way that yields the largest 
 * value. For example, if the given numbers are {54, 546, 548, 60}, 
 * the arrangement 6054854654 gives the largest value. And if the given 
 * numbers are {1, 34, 3, 98, 9, 76, 45, 4}, then the arrangement 
 * 998764543431 gives the largest value.
 */
#include "m_includes.hpp"

using namespace std;

string max_num(vi &arr) {
	sort ( arr.begin(), arr.end(), [] (const int &a, const int &b) {
		string sa = to_string(a), sb = to_string(b);
		return (sa + sb) > (sb + sa);
	});
	stringstream ss;
	for (int i : arr) ss << i;
	return ss.str();
}

int  main() {
	int n;
	cin >> n;
	vi arr(n);
	READ_ARR(arr);
	cout << max_num(arr) << endl;
	return 0;
}