/**
Given a maze in the form of the binary rectangular matrix, 
find length of the shortest path in a maze from given source to given destination.
**/

#include "m_includes.hpp"

using namespace std;

typedef vector<vb> bool_matrix;

struct q_node_t {
    int x, y, dist;
};

inline bool is_valid(int i, int j, int m, int n, bool_matrix &visited) {
    return i>= 0 && i < m && j >= 0 && j < n && !visited[i][j];
}

int shortest_distance(bool_matrix &mat) {
    int m = mat.size(), n = mat[0].size();
    int dist = INT_MAX;
    for(int i=0; i<m; ++i) {
        if(mat[i][0]) {
            // Start BFS.
            queue<q_node_t> q;
            bool_matrix visited(m, vb(n, false));
            q.push({i, 0, 0});
            while(!q.empty()) {
                q_node_t top = q.front();
                q.pop();
                visited[top.x][top.y] = true;
                if(top.y == n-1)
                    dist = min(dist, top.dist);
                int dx[] = {1, 0, -1, 0},
                    dy[] = {0, 1,  0, -1};
                  for(int i=0; i<4; ++i) {
                if(is_valid(top.x + dx[i], top.y + dy[i], m, n, visited) && mat[top.x + dx[i]][top.y + dy[i]] )
                    q.push({ top.x + dx[i], top.y + dy[i], top.dist + 1});
                }   
            }
        }
    }
    return dist;
}


int shortest_distance(bool_matrix &mat, int src_i, int src_j, int dest_i, int dest_j) {
    int m = mat.size(), n = mat[0].size();
    queue<q_node_t> q;
    q.push({ src_i, src_j, 0});
    bool_matrix visited(m, vb(n, false));
    while(! q.empty()) {
        q_node_t top = q.front();
        q.pop();
        if(top.x == dest_i && top.y == dest_j) {
            return top.dist;
        }
        visited[top.x][top.y] = true;
        int dx[] = {1, 0, -1, 0},
            dy[] = {0, 1,  0, -1};
        for(int i=0; i<4; ++i) {
            if(is_valid(top.x + dx[i], top.y + dy[i], m, n, visited) && mat[top.x + dx[i]][top.y + dy[i]] )
                q.push({ top.x + dx[i], top.y + dy[i], top.dist + 1});
        }
    }
    return INT_MAX;
}

int main() {
    bool_matrix mat = {
        { 1, 1, 1, 1, 1, 0, 0, 1, 1, 1 },
        { 0, 1, 1, 1, 1, 1, 0, 1, 0, 1 },
        { 0, 0, 1, 0, 1, 1, 1, 0, 0, 1 },
        { 1, 0, 1, 1, 1, 0, 1, 1, 0, 1 },
        { 0, 0, 0, 1, 0, 0, 0, 1, 0, 1 },
        { 1, 0, 1, 1, 1, 0, 0, 1, 1, 0 },
        { 0, 0, 0, 0, 1, 0, 0, 1, 0, 1 },
        { 0, 1, 1, 1, 1, 1, 1, 1, 0, 0 },
        { 1, 1, 1, 1, 1, 0, 0, 1, 1, 1 },
        { 0, 0, 1, 0, 0, 1, 1, 0, 0, 1 },
    };
    cout << "Shortest path  : " << shortest_distance(mat, 0, 0, 7, 5) << endl;
    return 0;
}

