/**
 * Sample program to print the left view of a binary tree.
 * Same idea can be used for the right tree.
 * 
 */
#include <bits/stdc++.h>


using namespace std;

struct node {
	node *left, *right;
	int data;
};



node *new_node(int data = 0) {
	node *n = new node;
	n->data = data;
	n->right = n->left = NULL;
	return n;
}

/**
 
         1
        /  \
	   2    3
	  / \   / \
	 4   5  6  7
	       / \
	      8   9
 */


int max_level = -1;

void left_view(node *root, int cur_level) {
	if(!root) return;
	if(max_level < cur_level) {
		max_level = cur_level;
		cout<<root->data<<" "<<endl;
	}

	left_view(root->left, cur_level + 1);
	left_view(root->right, cur_level + 1);
}


int main() {
	node *n[10];
	for(int i=1; i<10; ++i)
		n[i] = new_node(i);

	n[1]->left = n[2]; n[1]->right = n[3];
	n[2]->left= n[4], n[2]->right = n[5];
	n[3]->left = n[6], n[3]->right = n[7];
	n[6]->left = n[8], n[6]->right = n[9];

	left_view(n[1], 0); //root

	return 0;

}