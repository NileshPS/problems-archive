/*input
9
*/

/**
 * Count no of set bits in the binary representation of numbers in the range [1..n].
 */
#include "m_includes.hpp"

using namespace std;

int count_ones(int n) {
    int  c = 0;
    while(n) {
        c++;
        n = n & (n-1);
    }
    return c;
}

int total_set_bits(int n) {
    int c = 0;
    for(int i=1;i <=n ; ++i)
        c += count_ones(i);
    return c;
}


int bit_count_opt(uint n) {
    int res = 0, pc = 0;
    for(uint i = (1 << 31); i > 0; i >>= 1) {
        if(i & n ){
            int zeroesToRight = int(log2(i));
            res += zeroesToRight * (i >> 1) + 1 + pc * (i);
            pc++;
        }
    }
    return res;
}


int bit_count_optimized(uint n) {
    int ans = 0, ones_count = 0;
    for(uint i = (1 << 31); i >0; i >>= 1) {
        if(i & n) {
            int bitsToRight = int(log2(i));
            ans += bitsToRight * ( i >> 1) + ones_count * i + 1;
            ones_count++;
        }
    }
    return ans;
}
int main() {
    int n;
    cin >> n;
    cout << total_set_bits(n) << endl;
    cout << bit_count(n) << endl;
    return 0;
}
