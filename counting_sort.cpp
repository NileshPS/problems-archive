
/**
 * Couting sort implementation in C++.
 * @author Nilesh
 * @date 3/02/2017
 */

#include <bits/stdc++.h>

using namespace std;

typedef vector<int> vi;


void count_sort(vi &arr, int low, int high) {
	assert( high > low);
	int n = arr.size(), range_len = high - low + 1;
	int bias = 0;
	bias = (low < 0)? -low : 0;
	int count[range_len], result[n];
	memset(count, 0 ,sizeof(int) * range_len);
	for(int i=0; i<n; ++i) {
		count[arr[i] + bias]++;
	}
	for(int i=1; i<range_len; ++i) {
		count[i] += count[i-1];
	}
	memset(result, 0, n);
	for(int i=n-1; i>=0; --i) {
		int pos = count[arr[i] + bias]-1;
		result[pos] = arr[i];
		count[arr[i] + bias]--;
	}

	for(int i=0; i<n; ++i)
		arr[i] = result[i];
}

int main() {
	int n, h, l;
	cin >> n >> l >> h;
	vi arr(n, 0);
	for(int i=0; i<n; ++i)
		cin >> arr[i];
	count_sort(arr,l, h);
	for(int i : arr) {
		cout<<i<<" ";
	}

	return 0;
}