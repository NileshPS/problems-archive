#include <bits/stdc++.h>

using namespace std;

struct node {
	int data;
	node *link;
};

void insertFront(node *header, int data) {
	node *n = new node;
	n->data = data;
	n->link = header->link;
	header->link = n;
}

node *findMid(node *start) {
	node *ptr1, *ptr2, *ptr1Parent = NULL;
	ptr1 = ptr2 = start;
	while(ptr2 && ptr2->link) {
		ptr1Parent = ptr1;
		ptr1 = ptr1->link;
		ptr2 = ptr2->link;
		if(ptr2)
			ptr2 = ptr2->link;
	}
	return ptr2?ptr1:ptr1Parent;
}


void sort_list_util(node **);



node *sorted_merge(node *h1,  node *h2) {
	if(! h1)
		return h2;

	if(! h2)
		return h1;
	node *n;
	if(h1->data <= h2->data) {
		n = h1;
		n->link = sorted_merge(h1->link, h2);
	}
	else {
		n = h2;
		n->link = sorted_merge(h1, h2->link);
	}

	return n;
}


void sort_list_util(node **_n) {
	node *n = *_n;
	if(! n || !n->link)
		return;

	node *mid = findMid(n);
	node *headerOne = n, *headerTwo = mid->link;
	mid->link = NULL;
	sort_list_util(&headerOne);
	sort_list_util(&headerTwo);
	*_n = sorted_merge(headerOne, headerTwo);

}

int main() {
	node *header = new node;
	int n ,t;
	cin>>n;
	while(n--) {
		cin>>t;
		insertFront(header, t);
	}	

	if(! header->link || !header->link->link) {
		// list is either empty or single
	}
	else {
		sort_list_util(& header->link);
	}

	node *ptr = header->link;
	while(ptr) {
		cout<<ptr->data<<" ";
		ptr = ptr->link;
	}
	cout<<endl;
	return 0;
}