/*input
4
8 3 1 2
*/
#include "m_includes.hpp"


/**
 * Given an array arr[] of n integers, find the maximum that maximizes sum of 
 * value of i*arr[i] where i varies from 0 to n-1.
 */
using namespace std;

int max_rotation_value(const vi &arr) {
    int n = arr.size();
    int total = accumulate(arr.begin(), arr.end(), 0);
    int ans = INT_MIN, curSum = 0;
    for (int i=0; i < n; ++i) curSum += i * arr[i];
    ans = curSum;
    for (int i=0; i < n - 1; ++i) {
        int remSum = total - arr[i];
        curSum += arr[i] * (n - 1) - remSum;
        ans = max(curSum, ans);
    }
    return ans;
}

int main() {
    int n;
    cin >> n;
    vi arr(n);
    READ_ARR(arr);
    cout << "Max rotation value = " << max_rotation_value(arr) << endl;
    return 0;
}