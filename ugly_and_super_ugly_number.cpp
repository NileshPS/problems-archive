/*input
150
*/

/**
 * Ugly numbers are numbers whose only prime factors are 2, 3 or 5. 
 * The sequence 1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15, … 
 * shows the first 11 ugly numbers. By convention, 1 is included.
 */

/**
 * Super ugly numbers are positive numbers whose all prime factors are in the 
 * given prime list. Given a number n, the task is to find n’th Super Ugly 
 * number. It may be assumed that given set of primes is sorted. Also, 
 * first Super Ugly number is 1 by convention.
 */

#include "m_includes.hpp"

using namespace std;

int nth_ugly(int n) {
	assert (n >= 1);
	vi dp(n, 0);
	dp[0] = 1;
	int next2 = 2, next3 = 3, next5 = 5, i2 = 0, i3 = 0, i5 = 0;
	for (int i=1; i< n; ++i) {
		int cur = min(next2, min(next3, next5));
		dp[i] = cur;
		if (cur == next2) {
			next2 = dp[++i2] * 2;
		}
		if (cur == next3) 
			next3 = dp[++i3] * 3;
		if (cur == next5) 
			next5 = dp[++i5] * 5;
	}
	return dp[n-1];
}

int nth_super_ugly(const vi &primes, int n) {
	int psize = primes.size();
	vi dp(n, 0), next(psize, 0), ptr(psize, 0);
	for (int i=0; i < psize; ++i)
		next[i] = primes[i];
	dp[0] = 1;
	for (int i=1; i < n; ++i) {
		int cur = *min_element(next.begin(), next.end());
		dp[i] = cur;
		for (int j =0; j < psize; ++j) {
			if (next[j] == cur) {
				next[j] = dp[++ptr[j]] * primes[j];
			}
		}
	}
	return dp[n-1];
}

int main() {
	cout << nth_super_ugly({2, 5, 7}, 150) << endl;
	return 0;
}