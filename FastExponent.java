import java.util.*;

public class FastExponent {
	public static double pow(double m, int n) {
		double result = 1;
		while(n > 0) {
			result *= m;
			n--;
		}

		return result;
	}


	public static double pow2(double m, double n) {
		if(n == 0)
			return 1;

		if(n == 1)
			return m;

		if( n==2)
			return m*m;

		if(n % 2 == 0) {
			return pow2(pow2(m, n/2), 2);
		}
		else {
			return m*pow2(pow2(m, (n-1)/2), 2);
		}
	}


	public static void main(String... args) {
		
	}
}