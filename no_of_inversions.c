#include "__includes.h"

int merge(int *arr, int, int, int);

int countInversions(int *arr, int start, int end) {
	if(end <= start)
		return 0;
	int mid = (start + end)/2;
	return countInversions(arr, start, mid)  +
	       countInversions(arr, mid+1, end) + 
	       merge(arr, start, mid, end);
}


int merge(int *arr, int start, int mid, int end) {
	int temp[end - start +1], k = 0, i = start, j = mid + 1, ic = 0;
	while(i <= mid && j<= end) {
		if(arr[i] <= arr[j])
			temp[k++] = arr[i++];
		else {
			ic += mid - i + 1;
			temp[k++] = arr[j++];
		}
	}

	while(i <= mid) {
		temp[k++] = arr[i++];
	}

	while(j <= end) {
		temp[k++] = arr[j++];
	}

	for(int i=start; i <= end; ++i) {
		arr[i] = temp[i-start];
	}

	return ic;
}


int main() {
	int arr[] = {10, 20, 30, 8, 50, 60, 70, 60};
	printf("No of inversions is : %d\n\n", countInversions(arr, 0, sizeof(arr)/sizeof(int) - 1));
	return 0;
}
