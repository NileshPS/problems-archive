/*input
15
11 13 5 6 4 8 9 10 2 3 19 16 12 50 60
*/
#include "m_includes.hpp"

using namespace std;


int ceil_index(const vi &tail, int l, int r, int k) {
	int i = r;
	while (l < r) {
		int mid = (r + l ) >> 1;
		if (k < tail[mid]) {
			i = mid;r = mid - 1;
		}
		else if (k > tail[mid])
			l = mid + 1;
		else
			return mid;
	}
	return i;
}


int lis_opt(const vi &arr) {
	vi tail(arr.size());
	tail[0] = arr[0];
	int length = 1;
	for (int i=0; i < arr.size(); ++i) {
		if (arr[i] < tail[0])
			tail[0]  = arr[i];
		else if (tail[length - 1] < arr[i])
			tail[length++] = arr[i];
		else {
			// Find index j in tail such that tail[j] >= arr[i] &&  tail[j] - arr[i] is minimum
			int index = ceil_index(tail, 0, length - 1, arr[i]);
			tail[index] = arr[i];
		}
	}
	return length;
}

int LIS(const vi &arr) {
	vi lis(arr.size(), 1);
	int ans = INT_MIN;
	for (int i=1; i< arr.size(); ++i) {
		int j = i -1;
		while (j >= 0) {
			if (arr[j] <= arr[i] && lis[j] + 1 > lis[i]) {
				lis[i] = lis[j] + 1;
				ans = max(ans, lis[i]);
			}
			j--;
		}
	}
	return ans;
}

int main() {
	int n;
	cin >> n;
	vi arr(n);
	READ_ARR(arr);
	cout << "Length of LIS = " << lis_opt(arr) << endl;
	cout << "DP solution   = " << LIS(arr)     << endl;
	return 0;
}