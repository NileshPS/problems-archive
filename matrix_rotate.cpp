#include <bits/stdc++.h>

using namespace std;

#define vi vector<int>

template<class T> void rotateLeft(vector< vector <T> > &mat) {
	int m = mat.size(), n = mat[0].size();
	assert( m == n && m > 0);

	for(int i=0; i<n/2; ++i) {
		for(int j = i; j<n-i-1; ++j) {
			T temp = mat[i][j];
			mat[i][j] = mat[j][n-i-1];
			mat[j][n-i-1] = mat[n-i-1][n-j-1];
			mat[n-i-1][n-j-1] = mat[n-j-1][i];
			mat[n-j-1][i] = temp;	
		}
	}
	return;
}

template<class T> void rotateRight(vector< vector<T> >  &mat) {
	int m = mat.size(), n = mat[0].size();
	assert(m == n && m > 0);
	for(int i=0; i<n/2; ++i) {
		for(int j=i; j<n-i-1; ++j) {
			T temp = mat[i][j];
			mat[i][j] =  mat[n-j-1][i] ;
			mat[n-j-1][i] = mat[n-i-1][n-j-1];
			mat[n-i-1][n-j-1] = mat[j][n-i-1];
			mat[j][n-i-1] = temp;
		}
	}
}
void printMat(vector<vi> &mat) {
	for(int i=0; i<mat.size(); ++i) {
		for(int j=0; j<mat[0].size(); ++j) {
			printf("%3d  ", mat[i][j]);
		}	
		cout<<endl;
	}
}

int main() {
	//matrix rotate
	
	int m;
	cout<<"Enter order of square matrix : ";
	cin>>m;
	vector<vi> mat(m, vi(m, 0));
	for(int i=0; i<m; ++i) {
		for(int j=0; j<m;++j)
			cin>>mat[i][j];
	}
	rotateRight(mat);
	cout<<endl;
	printMat(mat);
	return 0;
}
