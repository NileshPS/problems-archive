/*input
2
2
3
*/  
#include <bits/stdc++.h>


/**
 * Given an integer N, how many structurally unique binary search trees are 
 * there that store values 1...N?
 */
/*
For example, for N = 2, there are 2 unique BSTs
     1               2  
      \            /
       2         1
For N = 3, there are 5 possible BSTs
  1              3        3         2      1
    \           /        /        /  \      \
     3        2         1        1    3      2
    /       /           \                      \
   2      1            2                        3
 */
using namespace std;

typedef vector<int> vi;
typedef vector<vi> matrix;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef vector<pii>   vpii;
typedef vector<bool> vb;
typedef vector<vb> bmatrix;

#define matrix_init(mat, m, n, val)   mat.resize(m, vi(n, val))
#define read_arr(arr)                       for (auto &el : arr) cin >> el;
#define matrix_read(mat)       for (auto &row : mat) \
                                       for (auto &el : row) \

#define print_arr(arr) for(auto &el : arr) { cout << el << " "; } cout << endl;
 

 int  count(int n) {
    if (n <= 1) return 1;
    if (n == 2) return 2;
    int ans = 0;
    for (int i=0; i < n; ++i) {
        int left = count(i), right = count(n - i - 1);
        ans += left * right;
    }
    return ans;
 }


int count_dp(int n) {
    vi arr(n + 1, 0);
    arr[0] = arr[1] = 1;
    arr[2] = 2;
    for (int i =3; i <= n; ++i) {
        for (int j = 0; j < i; ++j) {
            arr[i] += arr[j] * arr[i - j - 1];
        }
    }
    return arr[n];
}

int main() {
    int t;
    cin >> t;
    while(t--) {
        int n;
        cin >> n;
        cout << count_dp(n) << endl;
    }
    return 0;
}