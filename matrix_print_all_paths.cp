#include "m_includes.hpp"

using namespace std;

vi path;

void print_all_paths(const matrix &mat, int i, int j, int n) {
    if( i < 0 || i >= n || j < 0 || j >= n)
        return;
    if(i == n-1 && j == n-1) {
        for(auto &el : path)
            cout << el << "  ";
        cout << mat[i][j] << endl;
        return;
    }
    path.push_back(mat[i][j]);
    print_all_paths(mat, i +1, j, n);
    print_all_paths(mat, i, j+1, n);
    path.pop_back();
}

int main() {
    int n;
    cin >> n;
    matrix mat(n, vi(n, 0));
    for(auto &row : mat) {
        for(auto &el : row)
            cin >> el;
    }
    print_all_paths(mat, 0, 0, n);
    return 0;
}