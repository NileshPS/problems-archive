#include <bits/stdc++.h>

using namespace std;

int *arr, *st;
int arr_len, st_len;

void bst(int ss, int se, int index) {
	if (ss == se)
		st[index] = arr[se];
	else {
		int mid = (ss + se)/2;
		bst(ss, mid, 2*index + 1);
		bst(mid+1, se, 2*index + 2);
		st[index] = min(st[2*index + 1], st[2*index+2]);
	}
}

int range_min(int qs, int qe, int ss, int se, int index) {
	if(qe < ss || qs > se)
		return INT_MAX;

	if(ss >= qs && se <= qe)
		return st[index];

	int mid = (ss + se)/2;
	return min(
		range_min(qs, qe, ss, mid, 2*index + 1),
		range_min(qs, qe, mid+1, se, 2*index + 2)
		);
}

void update_arr(int pos, int newval) {

}

int main() {
	cin>>arr_len;
	arr = new int[arr_len];
	for(int i=0; i<arr_len; ++i)
		cin>>arr[i];
	st_len = 2 * pow(2, ceil(log10(arr_len)/log10(2))) - 1;
	st = new int[st_len];
	memset(st, 0, st_len);
	bst(0, arr_len-1, 0);
	int c, i, j;
	while(true) {
		cin>>c>>i>>j;
		switch(c) {
			case 'E':
			case 'e' :
				exit(0);
				break;
			case 'R':
			case 'r':
				cout<<range_min(i, j, 0, arr_len-1, 0);
				break;
			case 'U':
			case 'u':
				update_arr(i, j);
				break;
		}
	}
}