/*input
6
1 3 5 7 9 11
*/  
#include <bits/stdc++.h>

using namespace std;

typedef vector<int> vi;
typedef vector<vi> matrix;
typedef vector<string> vs;
typedef pair<int, int> pii;
typedef vector<pii>   vpii;
typedef vector<bool> vb;
typedef vector<vb> bmatrix;

#define matrix_init(mat, m, n, val)   mat.resize(m, vi(n, val))
#define read_arr(arr)                       for (auto &el : arr) cin >> el;
#define matrix_read(mat)       for (auto &row : mat) \
                                       for (auto &el : row) \

#define print_arr(arr) for(auto &el : arr) { cout << el << " "; } cout << endl;
     



void update_BIT(vi &bit, int i, int diff) {
    int n = bit.size();
    i++;
    while (i < n) {
        bit[i] += diff;
        i += i & -i;
    }
}

int prefix_sum_BIT(const vi &bit, int i) {
    int s = 0;
    i++;
    while (i > 0) {
        s += bit[i];
        i -= i & (-i);
    }
    return s;
}

int range_sum_BIT(const vi &bit, int l, int r) {
    return prefix_sum_BIT(bit,  r ) - prefix_sum_BIT(bit, l - 1);
}

vi construct_BIT(const vi &arr) {
    vi bit(arr.size() + 1, 0);
    for (int i=0; i < arr.size(); ++i ) {
        update_BIT(bit, i, arr[i]);
    }
    return bit;
}

void update_range_BIT(vi &bit, int l, int r, int diff) {
    update_BIT(bit, l, diff);
    update_BIT(bit, r + 1, -diff);
}


int main() {
    int n;
    cin >> n;
    vi arr(n);
    read_arr(arr);
    // vi bit = construct_BIT(arr);
    vi bit(n + 1, 0);
    for (int i=0; i < n; ++i) {
        update_range_BIT(bit, i, i, arr[i]);
    }
    update_range_BIT(bit, 1, 4, 2);
    for (int i=0; i < n; ++i)
        cout << prefix_sum_BIT(bit, i) << " ";
    cout << endl;
    return 0;
}