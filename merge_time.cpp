#include <bits/stdc++.h>

using namespace std;


struct interval {
	int start, end;
	string toString() {
		ostringstream ostr;
		ostr<<"{" << start <<", "<<end<<" }";
		return ostr.str();
	}
};

bool overlap(const interval &one, const interval &two) {
	return two.end > one.start && two.end <= one.end;
}

bool compare(const interval &one, const interval &two) {
	return one.start < two.start;
}

int main() {
	int n;
	cin>>n;
	cout<<"Enter "<<n<<" intervals : ";
	interval arr[n];
	for(int i=0; i<n; ++i) {
		cin>>arr[i].start>>arr[i].end;	
		assert(arr[i].start <= arr[i].end);
	}

	stack<interval> st;
	sort(arr, arr + n, compare);
	// for(int i=0; i<n; ++i) {
	// 	cout<<" { "<<arr[i].start<<", "<<arr[i].end<<" } "<<endl;
	// }
	st.push(arr[0]);

	for(int i=1; i<n; ++i) {
		if(overlap(arr[i], st.top())) {
			interval top = st.top();
			st.pop();
			top.start = min(arr[i].start, top.start);
			top.end = max(arr[i].end, top.end);
			st.push(top);
		}
		else {
			st.push(arr[i]);
		}
	}

	while(!st.empty()) {
		cout<<st.top().toString();
		st.pop();
	}
}