/*input
5 6
4 2 2 6 4
*/
#include "m_includes.hpp"

using namespace std;

int brute(vi &arr, int m) {
	int ans = 0, n =  arr.size();
	for(int i=0; i < n; ++i) {
		int x = 0;
		for(int j=i + 1; j < n; ++j) {
			x ^= arr[j];
			if (x == m) ans++;
		}
	}
}

/**
 * Given a array and intege m, find the number of subarrays with xor  value m.
 */

int main() {
	int n, m;
	cin >> n >> m;
	vi arr(n);
	READ_ARR(arr);
	unordered_map<int, int> umap;
	umap[0] = 1;
	int prexor = 0, ans = 0;
	for (int i=0; i < n; ++i) {
		prexor ^= arr[i];
		auto it = umap.find(prexor ^ m);
		if (it != umap.end()) {
			ans += it->second;
		}
		umap[prexor]++;
	}
	cout << ans << endl;
	return 0;
}