
#include <bits/stdc++.h>

using namespace std;

string to_words(int n) {
	assert( n >= 0 || n <= 999999);

	if(n == 0) return "Zero";

	string ones[] = {"", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven",
					"Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen", "Twenty" };
	string doubles[] = {"", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

	int t = n, c = 0;
	string result = "";
	while(t > 0) {
		++c;
		int rem = t % 10;
		t/=10;
		if(rem == 0) continue;
		if(c == 1) {
			result = ones[rem];
		}
		else if( c == 2) {
			if( rem == 1)
				result = ones[n % 100];
			else
				result.insert(0, doubles[rem] + " ");
		}
		else if(c == 3) {
			result.insert(0, ones[rem] + " Hundred " + ( n % 100 ?" and ":"") );
		}
		else if( c == 4) {
			if( (n % 1000) < 100 || (n % 1000) % 100 == 0) 
				result.insert(0, "and ");
			result.insert(0, ones[rem] + " Thousand " );

		}

	}

	return result;
}

int main() {
	while(true) {
		int n;
		cout<<"Enter a number (-1 to exit ) : ";
		cin>>n;
		if(n == -1)
			break;
		else if(n < 0 || n > 999999)
			cout<<"Out of bounds!"<<endl;
		else
			cout<<to_words(n)<<endl;
	}

	return 0;

}