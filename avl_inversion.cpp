#include <bits/stdc++.h>

struct node {
	int data, size, height;
	node *left, *right;
};

node *ROOT;

node *new_node(int data) {
	node *n = new node;
	n->size = 1, n->data = data;
	n->height = 1;
	n->left = n->right = NULL;
	return n;
}


int height(node *n) {
	return n?n->height:0;
}

int get_balance(node *root) {
	if(!root) return 0;
	return height(root->left) - height(root->right);
}

int get_size(node *root) {
	return root?root->size:0;
}

node *leftRotate(node *root) {
	if(!root) return NULL;

	node *new_root = root->right;
	root->right = new_root->left;
	new_root->left = root;

	root->height = 1 + std::max(height(root->left), height(root->right));
	new_root->height = 1 + std::max(height(new_root->left), height(new_root->right));

	root->size = get_size(root->left) + get_size(root->right) + 1;
	new_root->size = get_size(root->left) + get_size(root->right) + 1;

	return new_root;

}


node *rightRotate(node *root) {
	if(!root) return NULL;

	node *new_root = root->left;
	root->left = new_root->right;
	new_root->right = root;

	root->height = 1 + std::max(height(root->left), height(root->right));
	new_root->height = 1 + std::max(height(new_root->left), height(new_root->right));

	root->size = get_size(root->left) + get_size(root->right) + 1;
	new_root->size = get_size(new_root->left) + get_size(new_root->right) + 1;

	return new_root;
}

node *insert(node *root, int data, int *ic) {
	if(!root) return new_node(data);
	if(data < root->data) { // inversion detected!
		root->left = insert(root->left, data, ic);
		*ic = *ic + get_size(root->right) + 1;
	}
	else if(data > root->data)
		root->right = insert(root->right, data, ic);
	else // duplication node
		return root;

	if(data > root->data) root->size++; // increase the count of right elements
	int bf = get_balance(root);
	root->size = get_size(root->left) + get_size(root->right) + 1;
	if(bf < -1) {
		if(data < root->right->data) {
			//right - left
			root->left = rightRotate(root->left);
			root = leftRotate(root);
		}
		else {
			// right right
			root = leftRotate(root);
		}
	}
	else if(bf > 1) {
		if(data < root->left->data) {
			// left left
			root = rightRotate(root);
		}
		else {
			//left right
			root->right = leftRotate(root->right);
			root = rightRotate(root);
		}
	}

	return root;

}


typedef std::vector<int> vi;

int countInversions(vi &a) {
	int ic = 0;
	for(int i=0; i<a.size(); ++i) {
		ROOT = insert(ROOT, a[i], &ic);
	}
	return ic;
}

using namespace std;


int main() {
	int n;
	cin>>n;
	vi a(n);
	for(int i=0; i<n; ++i)
		cin>>a[i];

	cout<<"No of inversions : "<<countInversions(a)<<endl;
	return 0;

}