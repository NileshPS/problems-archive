#include <iostream>
#include <cstdio>

using namespace std;

int find_occurences(int *arr, int n, int key) {
	if(!arr)
		return -1;

	if( n == 0) return -1;

	//find the first occurence
	int first = 0, last = n-1;
	int first_pos = -1;
	int c = 0;
	while(first <= last) 
	{
		++c;
		int mid  = (first + last)/2;
		if(arr[mid] == key) {
			first_pos = mid;
			last = mid-1;
		}
		else if(arr[mid] < key)
			first = mid+1;
		else 
			last = mid - 1;
	}
	if(first_pos == -1)
		return -1;

	cout<<"First pos : "<<first_pos<<endl;
	//else find the last pos
	int last_pos = first_pos;
	first = 0, last = n -1;
	while(first <= last) {
		++c;
		int mid = (first + last)/2;
		if(arr[mid] == key)
		{
			last_pos = mid;
			first = mid + 1;
		}
		else if(arr[mid] < key) {
			first = mid + 1;
		}
		else
			last = mid - 1;
	}
	cout<<"Loop executed "<<c<<" times!"<<endl;
	cout<<"Last pos : "<<last_pos<<endl;
	return last_pos - first_pos + 1;
}


int main() {
	int n;
	cin>>n;
	int *arr = new int[n];
	for(int i=0; i<n; ++i)
		cin>>arr[i];
	int key;
	cin>>key;
	cout<<key<<endl;
	int c = find_occurences(arr, n, key);
	if(c == -1)
		cout<<"Not found!"<<endl;
	else
		cout<<c<<" time!"<<endl;

	delete[] arr;
	return 0;
}
