#include "__includes.h"

void reverse(int *, size_t, size_t);
void _swap(int *, int *);

void left_rotate(int *arr, size_t len, size_t d) {
    d %= len;
    if(len == 0 || d == 0)
        return;
    reverse(arr, 0, d-1);
    reverse(arr, d, len - 1);
    reverse(arr, 0, len - 1);
}


void reverse(int *arr, size_t left, size_t right) {
    if(right <= left)
        return; // Error
    size_t count = (right - left)  + 1;
    for(int i=0; i < count/2; ++i) {
        _swap(arr + left + i, arr + right - i);
    }
    return;
}

void _swap(int *a, int *b) {
    int t = *a;
    *a    = *b;
    *b    = t;
}

int main() {
    int arr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int      n = sizeof(arr)/sizeof(arr[0]);
    
    int d;
    scanf("%d", &d);
    left_rotate(arr, n, d);
    for(int i=0; i<n; ++i)
        printf("%d  ", arr[i]);
    printf("\n");
    return 0;
}