import java.util.*;

public class TrendingWordTracker {

    private int nWords;
    private Heap<String> heap;
    private Map<String, Integer> wordCount;

    public TrendingWordTracker(int N) {
        this.nWords = N;
        heap = new Heap<>(nWords);
        wordCount = new HashMap<>();
    }

    public void addWord(String word) {
        int count = wordCount.getOrDefault(word, 0);
        count += 1;
        if (heap.isFull()) {
            if (heap.contains(word)) {
                heap.update(word, count);
            }
            else if (heap.minPriority() <= count) {
                heap.replaceMin(word, count);
            }
        }
        else {
            if (heap.contains(word))
                heap.update(word, count);
            else
                heap.add(word, count);
        }
        wordCount.put(word, count);
    }

    public Map<String, Integer> getTrending() {
        return heap.toMap();
    }
}

/**
 * An updatable implementation of a MinHeap.
 * @param <T> A Comparable type.
 */
class Heap<T> {

    private int maxSize, curSize;
    private List<HeapNode<T>> arr;
    private HashMap<T, Integer> indexMap;

    public Heap(int size) {
        this.maxSize = size;
        curSize = 0;
        arr = new ArrayList<>();
        for (int i=0; i < maxSize; ++i)
            arr.add(null);
        indexMap = new HashMap<>();
    }

    private int getIndex(T node) {
        return indexMap.getOrDefault(node, -1);
    }

    private void setIndex(T node, int index) {
        indexMap.put(node, index);
    }

    public void add(T node, int priority) {
        int index = getIndex(node);
        if (index == -1) {
            if (curSize >= maxSize) {
                replaceMin(node, priority);
            }
            else {
                arr.set(curSize, new HeapNode<>(node, priority));
                setIndex(node, curSize);
                propagateUp(curSize++);
            }
        }
        else {
            throw new IllegalArgumentException("element already in heap. use update.");
        }
    }

    public boolean contains(T node) {
        return indexMap.containsKey(node);
    }

    public boolean isFull() {
        return curSize == maxSize;
    }

    public int minPriority() {
        if (curSize == 0)
            throw new IllegalStateException("heap empty!");
        return arr.get(0).priority;
    }

    public void replaceMin(T node, int priority) {
        T curMin = arr.get(0).element;
        indexMap.remove(curMin);
        arr.set(0, new HeapNode<>(node, priority));
        indexMap.put(node, 0);
        propagateDown(0);
    }

    public boolean update(T node, int newPriority) {
        int index = getIndex(node);
        if (index == -1)
            throw new IllegalArgumentException("element not in heap.");
        // get current priority
        int oldPriority = arr.get(index).priority;
        if (oldPriority == newPriority) return false;
        arr.get(index).priority = newPriority;
        if (newPriority > oldPriority)
            propagateDown(index);
        else
            propagateUp(index);
        return true;
    }

    public Map<T, Integer> toMap() {
        Map<T, Integer> map = new HashMap<>();
        for (HeapNode<T> h: arr)
            map.put(h.element, h.priority);
        return map;
    }

    private void propagateDown(int index) {
        int left = 2 * index + 1, right = left + 1;
        int minIndex = index, curP = arr.get(index).priority;
        if (left < curSize && arr.get(left).priority < curP) {
            minIndex = left;
            curP = arr.get(left).priority;
        }
        if (right < curSize && arr.get(right).priority < curP)
            minIndex = right;
        if (minIndex != index) {
            Collections.swap(arr, minIndex, index);
            setIndex(arr.get(minIndex).element, minIndex);
            setIndex(arr.get(index).element, index);
            propagateDown(minIndex);
        }
    }

    private void propagateUp(int index) {
        while (index > 0) {
            int parent = (index - 1) / 2;
            if (arr.get(parent).priority > arr.get(index).priority) {
                Collections.swap(arr, index, parent);
                setIndex(arr.get(index).element, index);
                setIndex(arr.get(parent).element, parent);
                index = parent;
            }
            else break;
        }
    }


    private static class HeapNode<T> {
        T element;
        int priority;

        public HeapNode(T element, int priority) {
            this.element = element;
            this.priority = priority;
        }
    }
}