/*input
5
1 2 3 4 5
 */

#include <bits/stdc++.h>

using namespace std;

void insertAtBottom(stack<int> &st, int t) {
	if(st.empty()) st.push(t);
	else {
		int t = st.top();
		st.pop();
		insertAtBottom(st, t);
		st.push(t);
	}
}

void reverse(stack<int> &st) {
	if(st.size() <= 1) return;
	int temp = st.top();
	st.pop();
	reverse(st);
	insertAtBottom(st, temp);
}

int main() {
	int n;
	cin>>n;
	stack<int> st;
	while(n--) {
		int t;
		cin>>t;
		st.push(t);
	}

	reverse(st);
	while(!st.empty()) {
		cout<<st.top();
		st.pop();
	}
	return 0;
}