#include <bits/stdc++.h>

#define contains(container, element) ((container).find((element)) != (container).end())

using namespace std;

typedef vector<string> vs;
typedef pair<int, int> pi;
typedef vector<int>    vi;

struct node {
	int data, lcount;
	node *left, *right;
};

node *get_node(int data = 0) {
	node *n = new node;
	n->left = n->right = NULL;
	n->data = data;
	n->lcount = 0;
	return n;
}

bool insert(node **header, int data) {
	if(! *header) {
		*header = get_node(data);
		return true;
	}
	node *parent = NULL, *ptr = *header;
	while(ptr) {
		parent = ptr;
		if(data < ptr->data) {
			ptr->lcount++;
			ptr = ptr->left;
		}
		else if(data > ptr->data)
			ptr = ptr->right;
		else
			return false;
	}

	if(parent->data > data) {
		parent->left = get_node(data);
	}
	else
		parent->right = get_node(data);
	return true;
}

void inorder(node *root) {
	if(root) {
		inorder(root->left);
		cout << root->data << "  ";
		inorder(root->right);
	}
}

int kth_smallest(node *root, int k) {
	if(! root)
		return INT_MIN;

	if(root->lcount == k-1)
		return root->data;
	else if(root->lcount >= k) {
		return kth_smallest(root->left, k);
	}
	else {
		return kth_smallest(root->right, k - root->lcount - 1);
	}
}

int main() {
	node *root = NULL;
	cout << "Enter no of elements : ";
	int n;
	cin >> n;
	cout << "Enter them : ";
	for(int i=0; i<n; ++i) {
		int t;
		cin >> t;
		insert(&root, t);
	}	
	inorder(root);
	cout << endl << endl;
	cout << "Enter K : ";
	int k;
	cin >> k;
	if( 1 > k || k > n) {
		cout << "Invalid  K !" << endl;
	}
	else {
		cout << "Kth smallest element is " << kth_smallest(root, k) << endl;
	}
	return 0;
}