/**
 * Program to clone a linked list of nodes with an arbitrary pointer.
 * @created on Nov 16 2016
 */

#include <bits/stdc++.h>

using namespace std;

struct node {
	node *next, *rand;
	int data;
};


node *getNode(int data = 0) {
	node *t  = new node;
	t->next = t->rand = NULL;
	t->data = data;
	return t;
}


void print(node *header) {
	if(! header || !header->next) {
		cout<<"[EMPTY]";
	}
	else {
		header = header->next;
		while(header) {
			cout<<header->data<<" "<< ( header->rand? header->rand->data:0)<<endl;
			header = header->next;
		}
	}

	cout<<endl;
}


//This method has space complexity of O(1)
node *clone1(node *header) {
	if(!header)
		return NULL;
	node *ptr = header->next;
	node *t, *newHeader = getNode();
	while(ptr) {
		t = getNode();
		t->data = ptr->data;
		node *next = ptr->next;
		ptr->next = t;
		t->next = next;
		ptr = next;
	}

	ptr = header->next;
	while(ptr) {
		ptr->next->rand = ptr->rand ? ptr->rand->next : NULL;
		ptr = ptr->next->next;
	}

	ptr = header->next;
	node *newPtr = newHeader;
	while(ptr) {
		newPtr->next = ptr->next;
		newPtr = ptr->next;
		ptr->next = newPtr->next;
		ptr = newPtr->next;
	}

	return newHeader;
}


// Space complexity O(n)
node *clone2(node *header) {
	if(! header || !header->next)
		return NULL;
	int c = 0;
	list<node *> links;
	node *ptr = header->next, *nHeader = getNode(), *nPtr = nHeader;
	while(ptr) {
		++c;
		node *newNode = getNode(ptr->data);
		nPtr->next = newNode;
		nPtr = newNode;
		newNode->rand = ptr;
		node *ptr_next = ptr->next;
		ptr->next = newNode;
		//add it to the linked list
		links.push_back(ptr_next);
		//move to the next node
		ptr = ptr_next;
	}

	nPtr = nHeader->next;
	while(nPtr) {
		nPtr->rand = nPtr->rand->rand ? nPtr->rand->rand->next : NULL;
		nPtr = nPtr->next;
	}

	//restore original list
	ptr = header->next;
	list<node *>::iterator it = links.begin();
	while(ptr && it != links.end()) {
		ptr->next = *it;
		ptr = ptr->next;
		it++;
	}

	return nHeader;
}


int main() {
	node *header = new node;
	header->next = header->rand = NULL;
	//  1    2    3    4    5   [ index ]
	//  1 -> 2 -> 5 -> 11 -> 9  [ data ]
	//  0    1    2     0    2  [ rand_index ]
	
	
	node *n1 = getNode(13), *n2 = getNode(20), *n3 = getNode(5),
				*n4 = getNode(11), *n5 = getNode(9);
	header->next = n1;
	n1->next = n2;
	n2->next = n3;
	n3->next = n4;
	n4->next = n5;
	n2->rand = n1;
	n3->rand = n2;
	n5->rand = n2;

	node *copy = clone2(header);
	print(header);
	print(copy);

	return 0;
}
