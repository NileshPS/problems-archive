#include <bits/stdc++.h>

using namespace std;


struct page_t {
	int no;
};


struct QNode {
	QNode *left, *right;
	page_t page;
};

struct Queue {
	int count, size;
	QNode *header;
};



QNode **ref;
Queue q;
int n;

void init_pages(int n, int qs) {
	ref = new QNode*[n];
	q.count = 0;
	q.size = qs;
	q.header = new QNode;
	q.header->left = q.header->right = q.header;
}


void clean() {
	delete[] ref;
}

bool request_page(int p) {
	assert(p >= 0 && p < n);
	if(ref[p]) {
		// Page Hit.
		if(ref[p] == q.header->right) return true;
		// Bring page to the front of the LRU Cache.
		QNode *p_node = ref[p];
		QNode *front = q.header->right;
		//Remove p_node from queue first
		QNode *temp = p_node->left;
		temp->right = p_node->right;
		p_node->right->left = temp;

		//now insert it in the front
		temp = front->left;
		p_node->right = front;
		front->left = p_node;
		p_node->left = temp;
		temp->right = p_node;
		return true;
	}
	else {
		//Cache miss :-(
		if(q.count >= q.size) {
			//delete the last node
			QNode *last  = q.header->left;
			ref[last->page.no] = NULL;
			QNode *tmp = last->left;
			tmp->right = last->right;
			last->right->left = tmp;
			delete last;
			//insert new node in front. Hence, no change in size.
			QNode *new_node = new QNode;
			new_node->page.no = p;
			new_node->left = q.header, new_node->right = q.header->right;
			q.header->right->left = new_node;
			q.header->right = new_node;
		}
		else {
			q.count++;
			QNode *m_node = new QNode;
			ref[p] = m_node;
			m_node->page.no = p;
			m_node->left = q.header, m_node->right = q.header->right;
			q.header->right->left = m_node;
			q.header->right = m_node;
		}
		

	}
	return false;
}

void print_cache() {
	cout<<endl;
	cout<<"Count of queue : "<<q.count<<endl;
	QNode *ptr = q.header->right;
	if(ptr == q.header) {
		cout<<"Queue is empty!"<<endl;
	}
	else {
		while(ptr != q.header) {
			cout<<ptr->page.no<<" ";
			ptr = ptr->right;
		}
	}
	cout<<endl;
	cout<<"LRU Cache dump complete!"<<endl;
}

int main(int argc, char **argv) {
	cout<<"Enter no of pages : ";
	cin>>n;
	cout<<"Enter size of LRU Cache : ";
	int q;
	cin>>q;
	init_pages(n, q);
	int ch, temp;
	do {
		cout<<"1. Request page\n2. Print LRU Cache\n3. Exit\nEnter an option : ";
		cin>>ch;
		switch(ch) {
			case 1:
				cout<<"Enter page no : ";
				cin>>temp;
				if(temp <0  || temp >= n) {
					cout<<"Invalid page no!";
					continue;
				}
				if(request_page(temp))
					cout<<"Cache hit!"<<endl;
				else 
					cout<<"Cache miss!"<<endl;
				break;
			case 2:
				print_cache();
				cout<<endl;
				break;
			case 3:
				cout<<"Goodbye!"<<endl;
				break;
			default:
				cout<<"Invalid option!"<<endl;
		}
	}while(ch != 3);
	clean();
	return 0;
}
				
