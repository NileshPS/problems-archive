#include "m_includes.hpp"

using namespace std;

struct node {
	int data;
	node *left, *right;

	node(int data = 0) {
		this->data = data;
		left = right = NULL;
	}

};

void printKBelow(node *root, int k) {
	if(!root || k < 0) return;
	if (k == 0) {
		cout << root->data <<  " ";
		return;
	}
	printKBelow(root->left, k- 1);
	printKBelow(root->right, k - 1);
}

int printK(node *root, int target, int k) {
	if (!root) return -1;
	if (root->data == target) {
		printKBelow(root, k);
		return 1;
	}
	int left = printK(root->left, target, k);
	if (left != -1) {
		if (left == k) 
			cout << root->data << " ";
		else
			printKBelow(root->right, k - left - 1);;
		return left + 1;
	}
	int right = printK(root->right, target, k);
	if (right != -1) {
		if (right == k) 
			cout << root->data << " ";
		else
			printKBelow(root->left, k - right - 1);
		return right + 1;
	}
	return -1;
}


int main() {
	node *n[10];
	for (int i=0; i < 10; ++i) {
		n[i] = new node(i);
	}
/*
		0
	   / \
	  1   2
	 / \   \
	3   4   8
	    \    \
	     5    9
	    /
	    6
	     \
	      7   
 */
	n[0]->left = n[1], n[0]->right = n[2];
	n[1]->left = n[3], n[1]->right = n[4];
	n[2]->right = n[8];
	n[4]->right = n[5];
	n[5]->left = n[6];
	n[6]->right = n[7];
	n[8]->right = n[9];
	printK(n[0], 5, 4);
	return 0;
}