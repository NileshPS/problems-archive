#include <bits/stdc++.h>

using namespace std;

void combinations(const string &str, size_t len, size_t cur_pos, size_t output_max_len, string  output) {
	if(output_max_len == output.length()) {
		cout << output << endl;
		return;
	}
	if(cur_pos >= len) 
		return; 
	// We can either take str[cur_pos] or not. There are two cases.
	// If we take it ..
	combinations(str, len, cur_pos + 1, output_max_len, output + str[cur_pos]);
	combinations(str, len, cur_pos + 1, output_max_len, output);
}

int main() {
	string str;
	int k;
	cout << "Enter string : ";
	cin >> str;
	int len = str.length();
	cout << "Enter k : ";
	cin >> k;
	combinations(str, len, 0, k, "");
	return 0;
}