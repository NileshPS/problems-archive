/*input
2
*/
/**
    Given a mobile keypad having digits from [0-9] associated with each key, 
    count total possible combinations of digits having length n. 
    We can start with any digit and press only four adjacent keys of any digit. 
    Keypad also contains * and # key which we are not allowed to press.
**/

#include "m_includes.hpp"

using namespace std;


#define MAXN 10


int dp[10][MAXN];

char keymap[][3] = {
    { '1', '2', '3'},
    { '4', '5' ,'6'},
    { '7', '8' ,'9'},
    { '*', '0', '#'}
};



inline int row(int digit) {
    if(digit == 0)
        return 3;

    return digit/4;
}

inline int col(int digit) {
    if(digit == 0)
        return 1;
    return digit % 3?digit % 3 - 1 : 2;    
}

inline bool isvalid(int i, int j) {
    return i>=0 && i < 3 && j >=0 && j < 3 && isdigit(keymap[i][j]);
}

inline int num(int r, int c) {
    if(c == 1 && r == 3)
        return 0;
    return 1 + 3 * r + c;
}

void print(string str, int k, int n ) {
    if(k == n - 1) {
        cout << str << endl;
        return;
    }

    int r = row(str[k] - '0'), c = col(str[k] - '0');
    print(str + str[k], k  + 1, n);
    if(isvalid(r +1, c)) 
        print(str + to_string(num(r + 1, c)), k + 1, n);

    if(isvalid(r, c + 1)) 
        print(str + to_string(num(r, c + 1)), k + 1, n);

    if(isvalid(r, c - 1))
        print(str + to_string(num(r, c - 1)), k + 1, n);

    if(isvalid(r - 1, c))
        print(str + to_string(num(r - 1, c)), k + 1, n);


}
int noOfCalls = 0;

int count(int digit, int k) {
    ++noOfCalls;
    if ( k == 0)
        return 1;

    int r = row(digit), c = col(digit), aux = 0;
    aux += count(digit, k - 1);
    if(isvalid(r + 1, c)) {
         aux += count(num(r+1, c), k - 1);
    }

    if(isvalid(r, c + 1))
        aux += count(num(r, c + 1), k - 1);

    if(isvalid(r - 1, c))
        aux += count(num(r - 1, c), k - 1);

    if(isvalid(r, c - 1))
        aux += count(num(r, c - 1), k - 1);

    return aux;
}

int dp_getCount(int digit, int k) {
    ++noOfCalls;
    // Use the cached result.
    if(dp[digit][k] > 0)
        return dp[digit][k];

    // Find The answer.
    int r = row(digit), c = col(digit), aux = 0;
    aux += count(digit, k - 1);
    if(isvalid(r + 1, c)) {
         aux += dp_getCount(num(r+1, c), k - 1);
    }

    if(isvalid(r, c + 1))
        aux += dp_getCount(num(r, c + 1), k - 1);

    if(isvalid(r - 1, c))
        aux += dp_getCount(num(r - 1, c), k - 1);

    if(isvalid(r, c - 1))
        aux += dp_getCount(num(r, c - 1), k - 1);

    return dp[digit][k] = aux;

}

int main() {
    memset(dp, -1, sizeof(int) * MAXN * 10);
    int k, c = 0;
    cin >> k;
    assert(k > 0 && k <= MAXN);
    for(int i=0; i<10; ++i)
        dp[i][0] = 1;
    for(int i=0; i<10; ++i) 
        c += count(i, k  - 1);

    cout << c << endl << noOfCalls;
}
// Using dynamic programming.
    

    