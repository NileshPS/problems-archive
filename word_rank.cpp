/**
Given a stream of words, write a program that'll give us the
trending 4 words at any instant. For the sake of the problem,
more occurences means it's more trending.
**/

#include <bits/stdc++.h>

#define HSIZE    4

using namespace std;

typedef pair<int, int> pii;

class word_rank {
	private:
		unordered_map<string, pii> hmap;
		typedef unordered_map<string, pii>::iterator miterator_t;
		int hsize;
		vector<miterator_t> heap;

		inline int parent(int i) { return (i - 1) >> 1;}
		inline int left(int i)   { return ( i << 1) + 1; }
		inline int right(int i)  { return (i << 1) + 2; }
	public:
		word_rank() {
			hsize = 0;
			heap.resize(HSIZE);
		}

		void addWord(const string &str) {
			auto it = hmap.find(str);
			if (it == hmap.end()) 
				it = hmap.emplace(str, make_pair(-1, 1)).first;
			else 
				it->second.second++;
			it->second.first == -1 ? addToHeap(it) : reheapify(it->second.first);
		}

		void addToHeap(miterator_t it) {
			if (hsize < HSIZE) {
				heap[hsize] = it;
				it->second.first = hsize;
				heap_update(hsize++);
			} else if (heap[0]->second.second < it->second.second) {
				// heap_swap(0, hsize - 1);
				// reheapify(0);
				// heap_remove(hsize);
				// addToHeap(it);
				heap_remove(0);
				heap[0] = it;
				it->second.first = 0;
				hsize++;
				reheapify(0);
			}
		}

		void heap_swap(int index, int parent) {
			heap[index]->second.first = parent;
			heap[parent]->second.first = index;
			swap(heap[index], heap[parent]);
		}

		void heap_update(int index) {
			while (index > 0 && heap[index]->second.second < heap[parent(index)]->second.second) {
					heap_swap(index, parent(index));
					index = parent(index);
			}
		}

		void heap_remove(int index) {
			heap[index]->second.first = -1; // Not in heap anymore
			hsize--;
		}

		void reheapify(int index) {
			int l = left(index), r = right(index), smallest = index;
			if ( l < hsize && heap[l]->second.second < heap[smallest]->second.second) 
				smallest = l;
			if (r < hsize && heap[r]->second.second < heap[smallest]->second.second) 
				smallest = r;
			if (smallest != index) {
				heap_swap(smallest, index);
				reheapify(smallest);
			}
		}

		vector< pair<string, int> > getTrending() {
			vector<pair<string, int> > ret;
			for(int i=0; i < hsize; ++i ){
				ret.push_back({ heap[i]->first, heap[i]->second.second});
			}
			sort(ret.begin(),ret.end(), [] (const pair<string, int> &one, const pair<string, int> &two) {
				return one.second > two.second;
			});
			return ret;
		}
};


int main() {
	word_rank ranker;
	string str;
	while (cin >> str) {
		if ( str == "PRINT_TOP") {
			
		} else {
			ranker.addWord(str);
		}
	}
	cout << "x----------x-------------x" << endl;
			for (auto &el : ranker.getTrending())
				printf("%-20s = %d\n", el.first.c_str(), el.second);
}

/**
Python script to generate random list of words

#/bin/python3
import os
import sys
import numpy as np


listOfWords = [
"hello", "world", "name", "age", "old", "orcs", "frodo", "modi", "messi", "federer", "amazon", "prime", "redmi3", 
"djokovic", "marvel", "comics", "justice league", "cristianoronaldo", "#GoT", "sherlock_holmes", "iniesta",
"veratti", "harvard", "microsoft", "google", "kindle", "android", "marshmallow", "linux", "kernel", "java", "interview"
];

noOfWords = 1000

fp = open("data", "w");

for _ in range(noOfWords):
	fp.write(listOfWords[np.random.randint(len(listOfWords))] + "\n");
	if _ % 100 == 0:
		fp.write("PRINT_TOP");

fp.close()
print("Done!")

 */
