#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>


typedef enum {zero = 0, one = 1} bit;

struct __node {
	bit b;
	struct __node *link;

};

typedef  struct __node node;

node *getNode(bit b) {
	node *n = (node *)malloc(sizeof(node));
	n->link = NULL;
	n->b = b;
	return n;
}

int main() {
	node *header = getNode(zero);
	int n, res = 0;
	scanf("%d", &n);
	node *ptr = header, *t;
	while(n > 0 ) {
		t = n&1?getNode(one):getNode(zero);
		t->link = header->link;
		header->link = t;
		n >>= 1;
	}
	ptr = header->link;
	while(ptr) {
		printf("%d ", ptr->b);
		ptr = ptr->link;
	}
	printf("\n");
	res = 0;
	ptr = header->link;
	while(ptr) {
		res = (res << 1 ) | ptr->b;
		ptr = ptr->link;
	}
	printf("\n%d\n", res);
	free(header);
	return 0;
}