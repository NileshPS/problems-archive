	#include <bits/stdc++.h>

	using namespace std;
	#define MAX_CHARS 26
	struct Node {
		Node *children[MAX_CHARS];
		bool terminate;
		int childCount;
	};

	class Trie {
	public:
		Trie() {
			root = new Node;
			memset(root->children, 0, sizeof(Node *)*MAX_CHARS);
			root->terminate = false;
		}

		void addString(const string& str) {
			if(str.length() == 0)
				return;

			int len = str.length();
			Node *ptr = root;
			for(int i=0; i<len; ++i) {
				if(! ptr->children[str[i] - 'a']) {
					Node *_new_node = ptr->children[str[i] - 'a'] = new Node;
					_new_node->childCount = 0;
					_new_node->terminate = false;
					memset(_new_node->children, 0, sizeof(Node *)*MAX_CHARS);
				}
				ptr->childCount++;
				ptr = ptr->children[str[i] - 'a'];
			}
			ptr->terminate = true;
		}

		vector<string> getAll() {
			vector<string> vec;
			get_all_util(root,"", vec );
			return vec;
		}

		void get_all_util(Node *node, string str, vector<string> &arr)  {
			if(node) {
				if(node->terminate) {
					arr.push_back(str);
				}	
				for(int i=0; i<MAX_CHARS; ++i) {
					get_all_util(node->children[i], str + char('a' + i), arr);
				}
			}
		}

		int word_count(Node *node) {
			if(!node)
				return 0;
			int c = 0;
			for(int i=0; i<MAX_CHARS; ++i) {
				c  += word_count(node->children[i]);
			}
			return (node->terminate?1:0) + c;
		}

		int total() {
			return word_count(root);
		}

		int prefix_count(const string& prefix) {
			Node *ptr =root;
			for(int i=0; i<prefix.length(); ++i) {
				ptr = ptr->children[prefix[i]  - 'a'];
				if(!ptr)
					return 0;
			}
			return ptr->childCount;
		}

	private:
		Node *root;

	};



	int main() {
		int n;
		cin>>n;
		Trie t;
		string action , arg;
		while(n--) {
			cin>>action>>arg;
			if(action == "add") {
				t.addString(arg);
			}
			else if(action == "find") {
				cout<<t.prefix_count(arg)<<endl;
			}
		}

		return 0;

	}