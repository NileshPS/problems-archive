#include <bits/stdc++.h>

using namespace std;

#define INF INT_MAX

class Graph {
private:
	int n;
	vector< vector< pair<int, int> > > adjList;

public:
	Graph(int n) { this->n = n; adjList.resize(n, vector< pair<int, int> >());}
	void addEdge(int src, int weight, int to) {
		adjList[src].push_back(make_pair(to, weight));
		adjList[to].push_back(make_pair(src, weight));
	}
	int mstWeightPrim() {
		vector<int> aux(n, INF);
		vector<bool> selected(n, false);
		aux[0] = 0; selected[0] = true;
		int minCost = 0;
		for(pair<int, int> p: adjList[0]) {
			aux[p.first] = p.second;
		}
		int i;
		for(int j=0; j<n-1; ++j) {
			int pos = 0;
			for(i=1; i<n; ++i) {
				if(! selected[i] && aux[i] < aux[pos]) { pos = i;}
			}

			// add node i to mst
			selected[i] = true;
			minCost += aux[i];
			for(pair<int, int> p : adjList[i]) {
				aux[p.first] = p.second;
			}
		}
		return minCost;
	}
};

int main() {

}