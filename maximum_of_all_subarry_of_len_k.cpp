/*input
7 3
12 1 78 90 57 89 56
*/
#include "m_includes.hpp"


// Refer http://www.geeksforgeeks.org/sliding-window-maximum-of-allsubarrays-of-size-k/
// http://www.ideserve.co.in/learn/maximum-element-from-each-subarray-of-size-k-set-2


using namespace std;

int main() {
	int n, k;
	cin >> n >> k;
	assert (k <= n);
	vi arr(n);
	READ_ARR(arr);

	// Algorithm starts here
	deque<int> dq;
	for (int i=0; i < k; ++i) {
		while (! dq.empty() && arr[i] >= arr[dq.back()])
			dq.pop_back();
		dq.push_back(i);
	}

	for (int i = k; i < n; ++i) {
		cout << arr[dq.front()] << "  ";
		while (! dq.empty() && dq.front() <= i - k) 
			dq.pop_front();
		while (!dq.empty() && arr[i] >= arr[dq.back()])
			dq.pop_back();
		dq.push_back(i);
	}
	cout << arr[dq.front()] << "  " << endl;
	return 0;
}