#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int cmp_char(const void *c1 ,const void *c2) {
	return *((char *)c1) - *((char *)c2);
}

int main() {
	char str[100], mask[100];
	scanf("%s", str);
	scanf("%s", mask);
	int strl = strlen(str), maskl = strlen(mask);
	qsort(str, strl, sizeof(char), &cmp_char);
	qsort(mask, maskl, sizeof(char), &cmp_char);
	int i, j;
	i = j = 0;
	while(i<strl && j < maskl) {
		if(str[i] == mask[j]) {
			++i, ++j; //skip
		}
		else if(str[i] < mask[j]) {
			printf("%c", str[i]);
			++i;
		}
		else {
			++j;
		}
	}
	while(i<strl) {
		printf("%c", str[i]);
		++i;
	}
	printf("\n");
}