/**
 * Program to efficiently calculate the median of a running stream of integers.
 */

#include <bits/stdc++.h>

 using namespace std;


typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector< ii > vii;


/**
 * Max heap implementation.
 */

class Heap {
private:
	vi arr;
	int size;
public:
	Heap() { size = 0; }
	void push(int n) {
		if(arr.size() > size) {
			arr[size] = n;
		}
		else {
			arr.push_back(n);
		}

		++size;
		int j = size - 1;
		while(j > 0) {
			int p = parent(j);
			if(arr[j] > arr[p]) {
				swap(arr[j], arr[p]);
				j = p;
			}
			else
				break;
		}
	}


	int extractTop() {
		if(size == 0) return INT_MIN;
		int temp = arr[0];
		arr[0] = arr[size - 1];
		--size;
		bool flag = true;
		int i  = 0;
		while(flag) {
			int leftChild = left(i), rightChild = right(i);
			if(rightChild < size) {
				if(arr[i] < arr[rightChild] && arr[rightChild] >= arr[leftChild]) {
					swap(arr[i], arr[rightChild]);
					i = rightChild;
				}
				else if(arr[i] < arr[leftChild] && arr[leftChild] > arr[rightChild]) {
					swap(arr[i], arr[leftChild]);
					i = leftChild;
				}
				else
					flag = false;
			}
			else if(leftChild < size) {
				if(arr[i] < arr[leftChild]) {
					swap(arr[i], arr[leftChild]);
					i = leftChild;
				}
				else {
					flag = false;
				}
			}
			else {
				flag = false;
			}
		}

		return temp;
	}

	inline int parent(int i) {
		return  i == 0?-1:(i - 1)/2;
	}

	inline int left(int i) {
		return 2*i + 1;
	}

	inline int  right(int i) {
		return 2*i + 2;
	}

	bool empty() { return size <= 0;}
	void print() {
		if(size <= 0) {
			cout<<"EMpty!"<<endl;
		}
		else {
			for(int i=0; i<size; ++i) {
				cout<<arr[i]<<" ";
			}
		}
		cout<<endl;
	}

	int get_size() { return size; }

	int top() { return arr[0]; }
};

/**
 * Class to find the median of running stream of integers.
 */

class MedianFinder {
private:
	Heap maxHeap, minHeap;
	int size;

public:
	MedianFinder() {
		// nothing
	}

	void push(int num) {
		if(size % 2 == 0) {
			maxHeap.push(num);
			++size;
			if(minHeap.get_size() == 0) return;
			if(maxHeap.top() > -minHeap.top()) {
				int m_min = -minHeap.extractTop(), m_max = maxHeap.extractTop();
				maxHeap.push(m_min);
				minHeap.push(-m_max);
			}
		}
		else {
			maxHeap.push(num);
			int m_max = maxHeap.extractTop();
			minHeap.push(-m_max);
			++size;
		}
	}

	double get_median() {
		if(size % 2 == 0)
			return (maxHeap.top() + -minHeap.top()) / 2.0;

		return maxHeap.top();
	}
} ;


int main() {
	int ch, temp;
	do {
		cout<<"1. Insert\n2. Median\n3. Exit\nEnter your choice : ";
		cin>>ch;
		switch(ch){
			case 1:
				cout<<"Enter a number : ";
				cin>>temp;

		}
	} while(ch != 3);
	
	// vi a;
	// for(int i=1; i<20; ++i) {
	// 	a.push_back(i);
	// }

	// random_shuffle(a.begin(), a.end());
	// for(int i=0; i<a.size(); ++i)
	// 	cout<<a[i]<<endl;

	// MedianFinder mf;
	// for(int i=0; i<a.size(); ++i)
	// 	mf.push(a[i]);

	// cout<<mf.get_median()<<endl;


	return 0;
}