#include <bits/stdc++.h>

using namespace std;

template <class T>
struct __node {
    T data;
    __node<T> *left, *right;
};

typedef __node<int> node;

node *get_node(int data = 0) {
    node *n = new node;
    n->data = data;
    n->left = n->right = NULL;
    return n;
}


node *lca(node *root, int a, int b) {
    if(! root)
        return nullptr;
    if(root->data == a || root->data == b)
        return root;
    node *left_lca = lca(root->left, a, b), *right_lca = lca(root->right, a, b);
    if(left_lca && right_lca)
        return root;
    return left_lca ? left_lca : right_lca;
}

int distance_util(node *root, int data, int cur_d) {
    if(!root)
        return INT_MAX;
    if(root->data == data)
        return cur_d;
    else if(root->left) {
        int tmp = distance_util(root->left ,data, cur_d + 1);
        if(tmp != INT_MAX)
            return tmp;
    }
    return distance_util(root->right, data, cur_d + 1);
}

int distance(node *root, int a, int b) {
    node *node_lca = lca(root, a, b);
    /**if(node_lca->data == a) {
        return distance_util(root, b, 0)  - distance_util(root, a, 0);
    }
    else if(node_lca->data == b)
        return distance_util(root, a, 0) - distance_util(root, b, 0);
    */ return distance_util(root, a, 0) + distance_util(root, b, 0) - 2*distance_util(root, node_lca->data, 0);
}

int main() {
    node *n[10];
    for(int i=0; i<10; ++i) {
        n[i] = get_node(i);
    }
    /**
     *          0
     *        /   \
     *       1     2
     *      /      / \
     *     8      3   4
     *    /      /     \
     *   9      5       7
     *          \
     *           6
     */

    n[0]->left = n[1], n[0]->right = n[2];
    n[1]->left = n[8];
    n[2]->left = n[3], n[2]->right = n[4];
    n[3]->left = n[5];
    n[4]->right = n[7];
    n[5]->right = n[6];
    n[8]->left = n[9];

    vector< pair<int, int> > pairs = { {1, 2}, {9 ,3}, {8, 3}, {2 ,3}, {5, 4}, {6, 7}};
    for(int i=0; i<pairs.size(); ++i) {
        cout << "LCA(" << pairs[i].first <<", " << pairs[i].second << ") = " << lca(n[0], pairs[i].first, pairs[i].second)->data << "\t\t";
        cout << "DIST(" << pairs[i].first << ", " << pairs[i].second << ") = " << distance(n[0], pairs[i].first, pairs[i].second) << endl;
    }
    // Free tree.
    return 0;
}


/*

// Find LCA with parent pointer in O(n) time and O(1) space

node_t *search_node(node_t *root, int k, int &level, int curLevel) {
    if (! root) return nullptr;
    if (root->data == k) {
        level = curLevel;
        return root;
    }
    node_t *left = search_node(root->left, k, level, curLevel + 1);
    if (left) return left;
    return  search_node(root->right, k , level, curLevel + 1);
}

node_t *find_lca(node_t *root, int k1, int k2) {
    if (! root) return nullptr;
    int l1 = -1, l2 = -1;
    node_t *first  = search_node(root, k1, l1, 0);
    node_t *second = search_node(root, k2, l2, 0);
    if (l1 == -1 || l2 == -1) return nullptr;
    cout << l1 << "\t" << l2 << endl;
    while (first != second) {
        if (l1 > l2) {
            first = first->parent;
            l1--;
        }
        else if(l1 < l2) {
            second = second->parent;
            l2--;
        } else {
            first = first->parent;
            second = second->parent;
            l1--;
            l2--;
        }
    }
    return first;
}

 */