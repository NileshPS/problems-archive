/*input
7
1 2 3 4 5 6 7

*/
#include <bits/stdc++.h>

using namespace std;

struct node {
	int data;
	node *lc, *rc;
};


node *get_node(int data = 0) {
	node *n = new node;
	n->lc =n->rc = NULL;
	n->data = data;
	return n;
}

node *sorted_list_to_bst(node *);
void inorder(node *);
void preorder(node *);
void postorder(node *);

int main() {
	int n;
	cin>>n;
	node *list_header = new node, *ptr = list_header;
	while(n--) {
		int data;
		cin>>data;
		node *t = get_node(data);
		ptr->rc = t;
		ptr = t;
	}
	cout<<"Entered list is : ";
	ptr = list_header->rc;
	while(ptr) {
		cout<<ptr->data<< " ";
		ptr = ptr->rc;
	}
	node *root = sorted_list_to_bst(list_header);

	//print inorder traversal of bst
	cout<<"\nInorder : ";
	inorder(root);
	cout<<endl<<"Postorder : ";
	postorder(root);
	cout<<endl<<"Preorder : ";
	preorder(root);
	cout<<endl;
	return 0;
}


void preorder(node *n) {
	if(n) {
		cout<<n->data<<" ";
		preorder(n->lc);
		preorder(n->rc);
	}
}

void postorder(node *n) {
	if(n) {
		postorder(n->lc);
		postorder(n->rc);
		cout<<n->data<<" ";
	}
}

void inorder(node *n) {
	if(n) {
		inorder(n->lc);
		cout<<n->data<<" ";
		inorder(n->rc);
	}
}

node *mid(node *start) {
	node *ptr1 , *ptr2, *parent;
	ptr2 = ptr1 = start;
	parent = NULL;
	while(ptr2 && ptr2->rc) {
		parent = ptr1;
		ptr1 = ptr1->rc;
		ptr2 = ptr2->rc;
		if(ptr2)
			ptr2 = ptr2->rc;
	}

	return ptr2?ptr1:parent;
}

void sorted_list_to_bst_util(node *, node **);

node *sorted_list_to_bst(node *header) {
	node *bst_root = get_node();
	sorted_list_to_bst_util(header->rc, &bst_root);
	return bst_root;
}

void sorted_list_to_bst_util(node *list_header, node **root) {
	if(! list_header)
		return;

	node *node_mid = mid(list_header);
	if(! *root) {
		*root = get_node();
	}
	(*root)->data = node_mid->data;
	node *list2_start = node_mid->rc;
	if(node_mid == list_header) return; //do nothing
	node_mid->rc = NULL;
	sorted_list_to_bst_util(list_header, &(*root)->lc);
	sorted_list_to_bst_util(list2_start, &(*root)->rc);
	node_mid->rc = list2_start;

}
