#include "m_includes.hpp"

using namespace std;

/**
 * http://www.geeksforgeeks.org/connect-nodes-at-same-level-with-o1-extra-space/
 */
struct node {
	int data;
	node *left, *right, *next;

	node(int data = 0) {
		this->data = data;
		left = right = next = nullptr;
	}
};

node *getNextRight(node *ptr) {
	if (! ptr) return nullptr;
	ptr = ptr->next;
	while (ptr) {
		if (ptr->left) return ptr->left;
		if (ptr->right) return ptr->right;
		ptr = ptr->next;
	}
	return NULL;
}


void connect(node *p) {
	if (!p) return;
	if (p->next) connect(p->next);
	if (p->left) {
		if (p->right) {
			p->left->next = p->right;
			p->right->next = getNextRight(p);
		} else {
			p->left->next = getNextRight(p);
		}
		connect(p->left);
	}
	else if (p->right) {
		p->right->next = getNextRight(p);
		connect(p->right);
	}
	else
		connect(getNextRight(p));
}

void print(const node *root) {
	while (root ) {
		cout << root->data << " ";
		root = root->next;
	}
	cout << endl;
}

int main() {
	node *n[10];
	for (int i=0; i < 10; ++i) {
		n[i] = new node(i);
	}
/*
		0
	   / \
	  1   2
	 / \   \
	3   4   8
   /       /  
  5	      6 
  \       \
   7       9   
	      
 */
	n[0]->left = n[1], n[0]->right = n[2];
	n[1]->left = n[3], n[1]->right = n[4];
	n[2]->right = n[8];
	n[3]->left = n[5];
	n[5]->right = n[7];
	n[6]->right = n[9];
	n[8]->left = n[6];

	connect(n[0]);
	print(n[1]);
	print(n[3]);
	print(n[5]);
	print(n[7]);

	return 0;
}