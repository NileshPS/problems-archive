#include <bits/stdc++.h>

using namespace std;

struct page_t {
	int no;
};

struct QNode {
	page_t page;
	QNode *left, *right;
};

struct Queue {
	int size, count;
	QNode *header;
};

Queue q;
int n;
map<int, QNode *> m_map;

void init(int no_of_pages, int qsize) {
	q.size = qsize, q.count = 0;
	q.header = new QNode;
	q.header->left = q.header->right = q.header;
}

void insert_front(QNode *new_front) {
	QNode *curFront = q.header->right;
	curFront->left = new_front;
	new_front->right = curFront;
	q.header->right = new_front;
	new_front->left = q.header;
}


void pop_last() {
	QNode *last = q.header->left;
	if(last == q.header)//empty
		return;

	last->left->right = last->right;
	last->right->left = last->left;
	map<int, QNode *>::iterator it = m_map.find(last->page.no);
	if(it != m_map.end()) {
		m_map.erase(it);
	}
	delete last;
}


bool request_page(int pageno) {
	assert( pageno >= 0 && pageno < n);
	map<int, QNode *>::iterator it = m_map.find(pageno);
	if(it == m_map.end()) {
		//cache miss
		if(q.count >= q.size) {
			pop_last();
			QNode *node = new QNode;
			node->page.no = pageno;
			m_map[pageno] = node;
			insert_front(node);
			return false;
		}
		else {
			// Cache not full yet!
			q.count++;
			QNode *node = new QNode;
			node->page.no = pageno;
			insert_front(node);
			m_map[pageno] = node;
			return false;
		}
	}
	else {
		//cache hit
		//Bring the node to the front!
		QNode *cur = it->second;
		if(cur != q.header->right) {
			QNode *tmp = cur->left;
			tmp->right = cur->right;
			cur->right->left = tmp;
			insert_front(cur);
		}
	}

	return true;
}

void print_cache() {
	cout<<endl;
	cout<<"Count of queue : "<<q.count<<endl;
	QNode *ptr = q.header->right;
	if(ptr == q.header) {
		cout<<"Queue is empty!"<<endl;
	}
	else {
		while(ptr != q.header) {
			cout<<ptr->page.no<<" ";
			ptr = ptr->right;
		}
	}
	cout<<endl;
	cout<<"LRU Cache dump complete!"<<endl;
}




int main(int argc, char **argv) {
	cout<<"Enter no of pages : ";
	cin>>n;
	cout<<"Enter size of LRU Cache : ";
	int q;
	cin>>q;
	init(n, q);
	int ch, temp;
	do {
		cout<<"1. Request page\n2. Print LRU Cache\n3. Exit\nEnter an option : ";
		cin>>ch;
		switch(ch) {
			case 1:
				cout<<"Enter page no : ";
				cin>>temp;
				if(temp <0  || temp >= n) {
					cout<<"Invalid page no!";
					continue;
				}
				if(request_page(temp))
					cout<<"Cache hit!"<<endl;
				else 
					cout<<"Cache miss!"<<endl;
				break;
			case 2:
				print_cache();
				cout<<endl;
				break;
			case 3:
				cout<<"Goodbye!"<<endl;
				break;
			default:
				cout<<"Invalid option!"<<endl;
		}
	}while(ch != 3);
	return 0;
}
				


