#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/**
  Like the Naive Algorithm, Rabin-Karp algorithm also slides the pattern one by one. But unlike the Naive algorithm, Rabin Karp algorithm matches the hash value of the pattern with the hash value of current substring of text, and if the hash values match then only it starts matching individual characters. So Rabin Karp algorithm needs to calculate hash values for following strings.

1) Pattern itself.
2) All the substrings of text of length m.

Since we need to efficiently calculate hash values for all the substrings of size m of text, we must have a hash function which has following property.
Hash at the next shift must be efficiently computable from the current hash value and next character in text or we can say hash(txt[s+1 .. s+m]) must be efficiently computable from hash(txt[s .. s+m-1]) and txt[s+m] i.e., hash(txt[s+1 .. s+m])= rehash(txt[s+m], hash(txt[s .. s+m-1]) and rehash must be O(1) operation.

The hash function suggested by Rabin and Karp calculates an integer value. The integer value for a string is numeric value of a string. For example, if all possible characters are from 1 to 10, the numeric value of “122” will be 122. The number of possible characters is higher than 10 (256 in general) and pattern length can be large. So the numeric values cannot be practically stored as an integer. Therefore, the numeric value is calculated using modular arithmetic to make sure that the hash values can be stored in an integer variable (can fit in memory words). To do rehashing, we need to take off the most significant digit and add the new least significant digit for in hash value. Rehashing is done using the following formula.

hash( txt[s+1 .. s+m] ) = d ( hash( txt[s .. s+m-1]) – txt[s]*h ) + txt[s + m] ) mod q

hash( txt[s .. s+m-1] ) : Hash value at shift s.
hash( txt[s+1 .. s+m] ) : Hash value at next shift (or shift s+1)
d: Number of characters in the alphabet
q: A prime number
h: d^(m-1)
 */


int search(const char *pat, const char* text, int prime);


int main() {
	char str[100], pat[100],__c__;
	printf("Enter text : ");
	scanf("%[^\n]%c", str, &__c__);
	printf("Enter pattern : ");
	scanf("%[^\n]", pat);
	int pos = search(pat, str, 101);
	if(pos != -1)
		printf("Found at %d\n", pos);
	else
		printf("Substring not found!\n");
	return 0;
}



int search(const char *pat, const char *text, int M) {
	printf("*%s* *%s*", pat, text);
	#define B 256
	int h = 1, t = 0, p = 0, m = strlen(pat), n = strlen(text);
	if(m > n) return -1;
	//calculate  pow(B, m-1) % M
	for(int i=1; i<m; ++i) 
		h = ( (h%M) * (B % M) ) % M;

	//calculate initial hash values for both pattern and first text shift
	
	for(int i=0; i<m; ++i) {
		t = ( ( ( t % M) * ( B % M ) ) % M + text[i] % M) %  M;
		p = ( ( ( p % M) * ( B % M ) ) % M + pat[i]  % M) %  M;
	}

	for(int i=0; i<=n-m; ++i) {
		printf("%d : %d\t%d\n", i, t, p);
		if(p == t) {
			//check the strings
			int j = 0;
			while(text[i + j] == pat[j] && j<m) j++;
			if( j == m)
				return i;
		}
		else {
			//caculate the next hash value
			if(i < n-m)
				t = ( ( ( ( t % M)  - ( h % M) ) % M * ( B % M) ) % M  + (text[i + m] % M) ) % M;
			if(t < 0) t = t + M;
		}
	}
	return -1;
}