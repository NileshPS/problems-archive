#include "m_includes.hpp"

using namespace std;

struct node {
	int data;
	node *right, *down;
	node(int data = 0, node *right = nullptr, node *down = nullptr) {
		this->data  = data;
		this->right = right;
		this->down  = down;
	}
};

node *construct_2d_list(const matrix &mat) {
	int m = mat.size();
	node *root = nullptr, *prev_root = nullptr;
	for (int i=0; i < m; ++i) {
		node *cur_root = new node;
		cur_root->data = mat[i][0];
		node *ptr = cur_root;
		for (int j=1; j < mat[i].size(); ++j) {
			node *new_node = new node(mat[i][j]);
			ptr->down      = new_node;
			ptr            = new_node;
		}
		if (! prev_root) {
			root = prev_root = cur_root;
		} else {
			prev_root->right = cur_root;
			prev_root = cur_root;
		}
	}
	return root;
}


void print_list(const node *root) {
	while(root) {
		const node *ptr = root;
		while (ptr) {
			cout << ptr->data << "  ";
			ptr = ptr->down;
		}
		cout << endl;
		root = root->right;
	}
	cout << endl;
}

node *merge(node *a, node *b) {
	if(!a ) return b;
	if(!b) return a;

	if (a->data < b->data) {
		a->down = merge(a->down, b);
		return a;
	}

	b->down = merge(a, b->down);
	return b;
}

node *flatten(node *root) {
	if (!root || root->right == nullptr) return root;
	return merge(root,  flatten(root->right));
}


int main() {
	matrix mat = {
		{5, 7 ,8, 30},
		{10, 20},
		{19, 22, 50},
		{28, 35, 40, 45}
	};

	node *root = construct_2d_list(mat);
	print_list(root);
	node *flat = flatten(root);
	while (flat) {
		cout << flat->data <<" ";
		flat = flat->down;
	}
	cout << endl;
	return 0;
}