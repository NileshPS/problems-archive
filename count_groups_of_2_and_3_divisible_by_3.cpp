/*input
6
1 5 7 2 9 14
*/
#include "m_includes.hpp"

using namespace std;



/**
 * You are given N distinct numbers. You are tasked with finding the number of 
 * groups of 2 or 3 that can be formed whose sum is divisible by three.
 */

int count_groups(const vi &arr) {
    int c[3] = {0, 0, 0};
    for (int i : arr) c[i % 3]++;
    return 
        (c[0] - 1) * c[0] / 2  +
        c[1] * c[2]            +
        ( c[0] * (c[0] - 1) * (c[0] - 2)/6) +
        ( c[1] * (c[1] - 1) * (c[1] - 2)/6) +
        ( c[2] * (c[2] - 1) * (c[2] - 2)/6) +
        c[0] * c[1] * c[2];
}

int main() {
    int n;
    cin >> n;
    vi arr(n);
    READ_ARR(arr);
    cout << count_groups(arr) << endl;
    return 0;
}