#include <bits/stdc++.h>

using namespace std;

/**
 * Helpers
 */
typedef vector<int> vi;
typedef pair<int, int> ii;
typedef vector< ii > vii;


/**
 * Function to print the permutations of the string in lexicographical order.
 * @param str The string to be permuted.
 */
void permute(string str) {
	int len = str.length();
	if(len == 0 ) return;
	else if (len == 1) {
		cout<<str<<endl;
		return;
	}

	bool done = false;
	std::sort(str.begin(), str.end());
	while(!done){
		cout<<str<<endl;
		int i; 
		for(i= len -2; i>=0; --i) {
			if(str[i] < str[i + 1]) break;
		}

		if(i <= -1) done = true;
		else {
			int min_pos = i + 1;
			for(int j = i + 2; j<len; ++j) {
				if(str[j] < str[min_pos] && str[j] > str[i]) min_pos = j;
			}

			swap(str[i], str[min_pos]);

			sort(str.begin() + i + 1, str.end());
		}
	}
}

/**
 * Driver function.
 * @return int 
 */
int main() {
	string str;
	cout<<"Enter string to permute : ";
	cin>>str;
	permute(str);
	return 0;
}
